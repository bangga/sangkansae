<?
	# Versi Aplikasi
	$appl_ver 			= "1.0" ;
	# Nama Aplikasi
	$appl_nama 			= "Prototype";
	# Owner Aplikasi
	$appl_owner 		= "Apotek Sangkan Sae";
	# Owner Referensi
	$reff_owner 		= "";
	# Alamat Owner
	$add0_owner 		= "Jalan Raya Sindang Agung";
	$add1_owner 		= "";
	# Telepon Owner
	$phon_owner 		= "0232";
	# Fax Owner
	$pfax_owner 		= "0232";
	# Pengembang Aplikasi
	$appl_developer 	= "Bangga Ramadho";
	# Nama Lengkap Aplikasi
	$application_nama 	= $appl_owner." &bull; ".$appl_nama." (Release: V".$appl_ver.")";
	# Logo Aplikasi
	$appl_logo 			= "favicon.ico";

	$phpSelf		= $_SERVER['PHP_SELF'];
	$ipClient		= $_SERVER['REMOTE_ADDR'];
	# Mangrupaning Tatanggalan
	$hari 			= array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Every Day");
	$bulan 			= array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	$tanggal		= date('d-m-Y');
	$jam			= date('H:i:s');
	$today 			= getdate(); 
	$month 			= date("m");
	$mday 			= date("d"); 
	$year 			= $today['year']; 
	$hday 			= date("w");
	$tgl_sekarang	= "$mday/$month/$year";
	$tgl_entry 		= "$year-$month-$mday";
?>

