<?php
	switch($proses){
		case "editReffBiaya":
			try {
				$pesan 	= "<strong>Succes!</strong> Perubahan referensi biaya ".$keterangan_biaya." telah dilakukan";
				$kelas	= "alert-succes";
				$link->beginTransaction();
				$que	= "UPDATE ref_biaya SET kode_biaya='".$kode_biaya."',keterangan_biaya='".$keterangan_biaya."' WHERE kode_biaya='".$kode_biaya."'";
				$res 	= $link->exec($que);
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan proses perubahan referensi biaya";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
<input type="hidden" class="showMess" name="pesan" 	value="<?php echo $pesan; ?>"/>
<input type="hidden" class="showMess" name="kelas" 	value="<?php echo $kelas; ?>"/>
<script>buka('refresh');</script>
<script>buka('showMess');</script>
<script>tutup('<?php echo $tutupId; ?>');</script>
<?php
			break;
		case "hapusReffBiaya":
			try {
				$pesan 	= "<strong>Succes!</strong> Penghapusan referensi biaya ".$keterangan_biaya." telah dilakukan";
				$kelas	= "alert-succes";
				$link->beginTransaction();
				$que	= "DELETE FROM ref_biaya WHERE kode_biaya='".$kode_biaya."'";
				$res 	= $link->exec($que);
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan proses penghapusan referensi biaya ".$keterangan_biaya;
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
<input type="hidden" class="showMess" name="pesan" 	value="<?php echo $pesan; ?>"/>
<input type="hidden" class="showMess" name="kelas" 	value="<?php echo $kelas; ?>"/>
<script>buka('refresh');</script>
<script>buka('showMess');</script>
<script>tutup('<?php echo $tutupId; ?>');</script>
<?php
			break;
		case "tambahReffBiaya":
			try {
				$pesan 	= "<strong>Succes!</strong> Penambahan referensi biaya ".$ketarangan_biaya." telah dilakukan";
				$kelas	= "alert-succes";
				$link->beginTransaction();
				$que	= "INSERT INTO ref_biaya(kode_biaya,keterangan_biaya) VALUES('".$kode_biaya."','".$keterangan_biaya."')";
				$res 	= $link->exec($que);
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan proses penambahan referensi biaya";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
<input type="hidden" class="showMess" name="pesan" 	value="<?php echo $pesan; ?>"/>
<input type="hidden" class="showMess" name="kelas" 	value="<?php echo $kelas; ?>"/>
<script>buka('refresh');</script>
<script>buka('showMess');</script>
<script>tutup('<?php echo $tutupId; ?>');</script>
<?php
			break;
		default :
			$mess	= "Tidak ada proses yang terdefinisi";
			$kelas	= "info";
			$log->logMess();
?>
<script>buka('refresh');</script>
<div class="span11">
	<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
</div>
<?php
	}
?>