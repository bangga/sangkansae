<?php
	switch($proses){
		case "showMess":
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $pesan; ?>
		</div>
	</div>
<?php
			break;
		case "editReffBiaya":
?>
<script>
	$('#inputKodeBiaya').select();
</script>
<div class="modal">
	<!-- Tambah barang -->
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="targetId"			value="targetFormId"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="targetUrl" 		value="<?php echo _PROC; 			?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="tutupId"			value="<?php echo $targetId;		?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="kode_biaya"		value="<?php echo $kode_biaya;		?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="nama_biaya"		value="<?php echo $nama_biaya;		?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="kode_transaksi"	value="<?php echo $kode_transaksi;	?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?>" 									name="proses"			value="editReffBiaya"/>
	<input 						type="hidden" class="hapus<?php echo $targetId; ?>" 								name="proses"			value="hapusReffBiaya"/>
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4 class="muted">Input Biaya : <?php echo $nama_biaya; ?></h4>
	</div>
	<div class="modal-body">
		<div id="targetFormId" class="row-fluid">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label">Jumlah</label>
					<div class="controls">
						<input id="inputKodeBiaya" type="text" class="input-medium text-left edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" name="jumlah_biaya" onmouseover="$(this).focus();" value="<?php echo $jumlah_biaya; ?>" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Keterangan</label>
					<div class="controls">
						<input type="text" class="input-medium text-left edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" name="keterangan_rinci" value="<?php echo $nama_biaya; ?>" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputSubmit"></label>
					<div class="controls">
						<div class="btn-group">
							<button class="btn" onclick="buka('edit<?php echo $targetId; ?>')">Simpan</button>
							<button class="btn" onclick="buka('hapus<?php echo $targetId; ?>')">Hapus</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		default:
			try{
				$que 			= "SELECT * FROM 050101_view WHERE hari=".($pg-1);
				$sth 			= $link->prepare($que);
				$sth->execute();
				$data			= $sth->fetchAll(PDO::FETCH_ASSOC);
				$tgl_transaksi	= $data[0]['tgl_transaksi'];
				$link			= null;

				/*	menentukan keberadaan operasi next page	*/
				if($pg>1){
					$next_mess	= "<button class=\"btn\" onclick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data referensi biaya");
				$log->logDB($que);
			}
			
			if(strlen($tgl_transaksi)<10){
				if($pg==1){
					$tgl_transaksi = "Hari ini tidak ada transaksi";
				}
				else{
					$tgl_transaksi = ($pg-1)." hari yang lalu tidak ada transaksi";
				}
			}
?>
<!-- Show message -->
<input type="hidden" class="showMess" 				name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="showMess" 				name="targetId"		value="targetMessId"/>
<input type="hidden" class="showMess"				name="proses"		value="showMess"/>
<!-- Tambah barang -->
<input type="hidden" class="tambah" 				name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="tambah"					name="proses"		value="tambahReffBiaya"/>
<!-- Pagging -->
<input type="hidden" class="next_page pref_page refresh" 	name="targetId"		value="content" />
<input type="hidden" class="next_page pref_page refresh" 	name="targetUrl" 	value="<?php echo _FILE;		?>"	/>
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $pref_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $next_page; 	?>" />
<input type="hidden" class="refresh" 						name="pg" 			value="<?php echo $pg; 			?>" />

<h4 class="muted cetak"><?php echo _NAME; ?></h4>
<small class="cetak"><?php echo _DESC; ?></small>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder"></div>
<div class="row-fluid" id="listBarang">
	<div class="span12">
		<div class="skrin">Daftar Pembalian Barang Per <?php echo $tgl_transaksi; ?></div>
		<table class="table table-bordered table-hover">
			<thead class="cetak">
				<tr>
					<th colspan="4">Tanggal : <?php echo $tgl_transaksi; ?></th>
				</tr>
				<tr>
					<th class="prn_head">Nama Barang</th>
					<th class="prn_head">Nama Satuan</th>
					<th class="prn_head">Harga Perolehan</th>
					<th class="prn_head">Jumlah Perolehan</th>
					<th class="prn_head">Total Harga</th>
					<th class="prn_head">Jumlah Penjualan</th>
					<th class="prn_head">Jumlah Persediaan</th>
					<th class="prn_head">Vendor</th>
				</tr>
			</thead>
			<tbody>
<?php			
			for($i=0;$i<count($data);$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
				
				$total_harga			= $jumlah_pembelian*$harga_perolehan;
				$total_h_perolehan[]	= $harga_perolehan;
				$total_j_perolehan[]	= $jumlah_pembelian;
				$total_h_total[]		= $total_harga;
				$total_j_penjualan[]	= $jumlah_penjualan;
				$total_j_persediaan[]	= $jumlah_persediaan;
?>
				<tr>
					<td class="prn_left prn_cell">	<?php echo $nama_barang;		?></td>
					<td class="prn_left prn_cell">	<?php echo $nama_satuan;		?></td>
					<td class="prn_cell">
						<div class="text-right"><?php echo number_format($harga_perolehan);		?></div>
					</td>
					<td class="prn_cell">
						<div class="text-right"><?php echo number_format($jumlah_pembelian);	?></div>
					</td>
					<td class="prn_cell">
						<div class="text-right"><?php echo number_format($total_harga);			?></div>
					</td>
					<td class="prn_cell">
						<div class="text-right"><?php echo number_format($jumlah_penjualan);	?></div>
					</td>
					<td class="prn_cell">
						<div class="text-right"><?php echo number_format($jumlah_persediaan);	?></div>
					</td>
					<td class="prn_left prn_cell">
						<div class="text-left"><?php echo $nama_vendor;	?></div>
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="targetUrl"		value="<?php echo _FILE; 			?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="proses"			value="editReffBiaya"					/>
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="tutupId" 		value="<?php echo $targetId; 		?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="kode_transaksi"	value="<?php echo $kode_transaksi; 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="kode_biaya"		value="<?php echo $kode_biaya;	 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="jumlah_biaya"	value="<?php echo $jumlah_biaya; 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="nama_biaya"		value="<?php echo $keterangan;	 	?>" />
					</td>
				</tr>
<?php
			}
?>
			</tbody>
			<thead>
				<?php if($i>0){	?>
				<tr>
					<th class="prn_head" colspan="3">Total</th>
					<th class="prn_head"><div class="text-right"><?php echo number_format(array_sum($total_j_perolehan));	?></div></th>
					<th class="prn_head"><div class="text-right"><?php echo number_format(array_sum($total_h_total));		?></div></th>
					<th class="prn_head"><div class="text-right"><?php echo number_format(array_sum($total_j_penjualan));	?></div></th>
					<th class="prn_head"><div class="text-right"><?php echo number_format(array_sum($total_j_persediaan));	?></div></th>
					<th class="prn_head"></th>
				</tr>
				<?php }			?>
				<tr class="cetak">
					<th colspan="7">
						<div class="btn-group">
							<button class="btn" onclick="buka('pref_page')">Pref</button>
							<?php echo $next_mess; ?>
						</div>
					</th>
					<th>Halaman <?php echo $pg; ?></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<?php
	}
?>