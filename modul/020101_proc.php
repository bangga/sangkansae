<?php
	switch($proses){
		case "validasi":
			try {
				$pesan 	= "<strong>Succes!</strong> Validasi stok opname telah dilakukan";
				$kelas	= "alert-succes";
				$link->beginTransaction();
				$que	= "INSERT INTO frm_opname(kode_status) VALUES(1)";
				$res 	= $link->exec($que);
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan validasi stok opname";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
	</div>
<?php
			break;
		case "cetakin":
			try{
				$wsdl_url	= "http://"._PRIN."/printClient/printServer.wsdl";
				$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
				$stringFile	= $stringFile.".txt";
				$client->cetak($stringCetak,$stringFile);
				$pesan		= "Daftar persediaan telah berhasil dicetak";
				$kelas		= "alert-success";
			}
			catch (Exception $e){
				$pesan 		= $e->getMessage();
				$kelas		= "alert-error";
			}
?>
<div class="span12">
	<div class="alert <?php echo $kelas; ?>">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $pesan; ?>
	</div>
</div>
<?php
			break;
		default :
			$log->logMess("Tidak ada proses yang terdefinisi");
	}
?>