<?php
	switch($proses){
		case "setBayar":
			try {
				$link->beginTransaction();
				if($do_dibayar>=$sisa_tagihan){
					$byr_total 		= $sisa_tagihan;
					$byr_kembali	= $do_dibayar - $sisa_tagihan;
					$kode_bayar		= 2;
					$pesan 			= "<strong>Success!</strong> Pembayaran utang telah dilakukan";
				}
				else{
					$byr_total		= $do_dibayar;
					$byr_kembali	= 0;
					$kode_bayar		= 1;
					$pesan 			= "<strong>Success!</strong> Pembayaran angsuran utang telah dilakukan";
				}
				$que	= "INSERT INTO tabel_pembayaran(byr_no,byr_tgl,kode_do,kar_id,lok_ip,byr_loket,byr_total,byr_kembali,byr_cetak,byr_upd_sts,byr_sts) VALUES("._TOKN.",NOW(),$kode_po,'"._USER."','"._HOST."','B',$byr_total,$byr_kembali,$byr_cetak,NOW(),1)";
				$res 	= $link->exec($que);
				$log->logDB($que);
				$que	= "UPDATE tabel_po SET tanggal_bayar=NOW(),kode_bayar=$kode_bayar,kar_id='"._USER."' WHERE kode_po=$kode_po AND kode_bayar<2";
				$res 	= $link->exec($que);
				$log->logDB($que);
				if($res>0){
					$link->commit();
					$kelas	= "alert-success";
				}
				else{
					$link->rollBack();
					$pesan 	= "<strong>Info!</strong> Pembayaran tidak dapat dilakukan";
					$kelas	= "alert-info";
				}
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan pembayaran tagihan";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
			}
			$log->logMess($pesan);
?>
<div class="span5">
	<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
	<p class="text-center"><button class="btn" onclick="tutup('<?php echo $tutupId; ?>')">Kembali</button></p>
</div>
<?php
			break;
		default :
			$log->logMess("Tidak ada proses yang terdefinisi");
	}
?>