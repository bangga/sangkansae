<?php
	if(!$erno) die();
	switch($proses){
		case "cetakResi":
			try{
				$wsdl_url	= "http://"._PRIN."/printClient/printServer.wsdl";
				$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
				$stringFile	= $stringFile.".txt";
				$client->cetak($stringCetak,$stringFile);
				$pesan		= "Resi telah berhasil dicetak";
				$kelas		= "alert-success";
			}
			catch (Exception $e){
				$pesan 		= $e->getMessage();
				$kelas		= "alert-error";
			}
?>
<div class="span12">
	<div class="alert <?php echo $kelas; ?>">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $pesan; ?>
	</div>
</div>
<?php
			break;
		case "validasi":
			try {
				$kode_po	= $_SESSION['kode_po'];
				$link->beginTransaction();
				$que		= "UPDATE tabel_po SET tanggal_drop=NOW(),tanggal_bayar=NOW(),kode_status=1,kar_id='"._USER."' WHERE kode_po='$kode_po' AND kode_status=3";
				$res 		= $link->exec($que);
				$log->logDB($que);
				$link->commit();
				if($res>0){
					$pesan = "Purchase order telah berhasil divalidasi";
					$kelas = "alert-success";
				}
				else{
					$pesan = "Purchase order tidak dapat divalidasi";
					$kelas = "alert-info";
				}
			}
			catch (Exception $e){
				$pesan = "Gagal melakukan proses validasi purchase order";
				$kelas = "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
			}
			$log->logMess($pesan);
			?><td colspan="7"><div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div></td><?php
			break;
		case "deleteDraft":
			$erno	= false;
			try {
				$link->beginTransaction();
				$que = "DELETE FROM tabel_pembelian WHERE kode_pembelian=$kode_pembelian AND kode_status=3";
				$res = $link->exec($que);
				$log->logDB($que);
				$link->commit();
				if($res>0){
					$pesan 	= "Rincian $jumlah_pembelian $nama_satuan $nama_barang telah dihapus";
					$kelas	= "alert-success";
				}
				else{
					$pesan 	= "Rincian purchase order tidak bisa dihapus";
					$kelas	= "alert-notice";
					$erno	= true;
				}
			}
			catch (Exception $e){
				$pesan 	= "Gagal melakukan proses hapus rincian purchase order";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$erno	= true;
			}
			$log->logMess($pesan);
			
			if($erno){
				?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; ?>" /><?php
				?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; ?>" /><?php
				?><script>buka('showMess')</script><?php
			}
			else{
				?><button class="btn btn-danger">Hapus</button><?php
				?><script>buka('refBarang');</script><?php
			}
			break;
		case "tambahPO":
			$kode_po	= $_SESSION['kode_po'];
			try {
				$link->beginTransaction();
				$que 		= "SELECT kode_po FROM tabel_po WHERE kode_po='$kode_po' AND kode_status=1";
				$res 		= $link->query($que);
				if($res->fetchColumn()>0){
					$pesan 	= "<strong>Notice!</strong> Order telah divalidasi";
					$kelas	= "alert-notice";
				}
				else{
					$pesan 	= "<strong>Success!</strong> Purchase order $jumlah_penjualan $nama_barang telah ditambahkan";
					$kelas	= "alert-success";
					// input barang
					$que 	= "INSERT INTO tabel_pembelian(kode_po,kode_barang,jumlah_pembelian,kode_satuan,harga,kar_id,keterangan) VALUES('$kode_po','$kode_barang',$jumlah_pembelian,$kode_satuan,$harga,'"._USER."','AUTO')";
					$link->exec($que);
					$log->logDB($que);
					// update harga jual
					if($harga_dasar>0){
						$que	= "INSERT INTO ref_harga(kode_barang,kode_satuan,harga_dasar) VALUES($kode_barang,$kode_satuan,$harga_dasar) ON DUPLICATE KEY UPDATE harga_dasar=$harga_dasar";
						$link->exec($que);
						$log->logDB($que);
					}
					$log->logMess($pesan);
					$link->commit();
				}
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan penambahan purchase order";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
		<p class="text-center"><button class="btn" onclick="buka('<?php echo $backId; ?>')">Kembali</button></p>
		<script>buka('refBarang');</script>
	</div>
<?php
			break;
		case "deletePO":
			$kode_po	= $_SESSION['kode_po'];
			$nama		= $_SESSION['nama'];
			$erno		= false;
			try {
				$link->beginTransaction();
				$que		= "DELETE FROM tabel_po WHERE kode_po='$kode_po' AND kode_status=3 AND kode_bayar=0";
				$res 		= $link->exec($que);
				if($res>0){
					$pesan 	= "Drop order $nama telah di hapus";
					$kelas	= "alert-success";
					unset($_SESSION['kode_po']);
					unset($_SESSION['nomer_po']);
					unset($_SESSION['appl_tokn']);
					unset($_SESSION['nama']);
					unset($_SESSION['kode_pelanggan']);
					unset($_SESSION['alamat']);
					unset($_SESSION['kota']);
					unset($_SESSION['badge']);
					?><script>buka('<?php echo _KODE; ?>')</script><?php
				}
				else{
					$pesan 	= "<strong>Notice!</strong> purchase order $nama tidak bisa dihapus";
					$kelas	= "alert-notice";
					$erno	= true;
				}
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan	= "<strong>Error!</strong> Proses delete purchase order gagal dilakukan";
				$kelas	= "alert-error";
				$erno	= true;
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
			if($erno){
				?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; ?>" /><?php
				?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; ?>" /><?php
				?><script>buka('showMess')</script><?php
			}
			break;
		case "setNopel":
			if(!isset($nama)){
				try {
					if(isset($kode_pelanggan)){
						$kunci = $kode_pelanggan;
					}
					$que 	= "SELECT a.kode_vendor AS kode_pelanggan,a.nama,a.alamat,a.kota,'0.5' AS badge,IFNULL(b.kode_po,0) AS kode_po,IFNULL(b.nomer_po,'-') AS nomer_po FROM tabel_vendor a LEFT JOIN tabel_po b ON(b.kode_vendor=a.kode_vendor AND b.kode_status=3) WHERE a.kode_vendor='".$kunci."' ORDER BY IFNULL(b.kode_po,0) LIMIT 1";
					$res 	= $link->prepare($que);
					$res->execute();
					$data	= $res->fetch(PDO::FETCH_ASSOC);
					if($res->rowCount()>0){
						/** getParam 
							memindahkan semua nilai dalam array POST ke dalam
							variabel yang bersesuaian dengan masih kunci array
						*/
						$konci	= array_keys($data);
						for($i=0;$i<count($konci);$i++){
							$$konci[$i]	= $data[$konci[$i]];
						}
						/* getParam **/
						$erno	= true;
					}
					else{
						$pesan 	= "<strong>Notice!</strong> Data Pelanggan tidak ditemukan";
						$kelas	= "alert-notice";
						$erno	= false;
					}
				}
				catch(Exception $e){
					$pesan 	= "<strong>Error!</strong> Gagal melakukan inquiri data pelanggan";
					$kelas	= "alert-error";
					$log->errorDB($e->getMessage());
					$log->logMess($pesan);
					$log->logDB($que);
				}
			}
			
			// Set parameter pelanggan ke session
			if($erno){
				$_SESSION['kode_pelanggan']	= $kode_pelanggan;
				$_SESSION['nama']			= $nama;
				$_SESSION['alamat']			= $alamat;
				$_SESSION['kota']			= $kota;
				$_SESSION['badge']			= $badge;
			}
			
			if($erno and $kode_po>1){
				$_SESSION['kode_po'] 	= $kode_po;
				$_SESSION['nomer_po']	= $nomer_po;
				?><script>buka('refOrder');</script><?php
				?><script>buka('refBarang');</script><?php
			}
			else if($erno){
				$kode_po 	= $_SESSION['kode_po'];
				$nomer_po 	= $_SESSION['nomer_po'];
				try {
					$link->beginTransaction();
					$que 	= "INSERT INTO tabel_po(kode_po,tanggal_po,nomer_po,nomer_kontrak,kode_vendor,kar_id,keterangan) VALUES('$kode_po',CURDATE(),'$nomer_po','-','$kode_pelanggan','"._USER."','AUTO')";
					$pesan	= "Purchase order $nama telah di simpan";
					$link->exec($que);
					$log->logDB($que);
					$log->logMess($pesan);
					$link->commit();
					?><script>buka('refOrder');</script><?php
				}
				catch (Exception $e){
					$pesan 	= "<strong>Error!</strong> Gagal melakukan input purchase order";
					$kelas	= "alert-error";
					$link->rollBack();
					$log->errorDB($e->getMessage());
					$log->logMess($pesan);
					$log->logDB($que);
					?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; 	?>"/><?php
					?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; 	?>"/><?php
					?><script>buka('showMess');</script><?php
				}
			}
			else{
				?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; 	?>"/><?php
				?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; 	?>"/><?php
				?><script>buka('showMess');</script><?php
			}
			if(isset($tutupId)){
				?><script>tutup('<?php echo $tutupId; ?>');</script><?php
			}
			break;
		default :
			$log->logMess("Tidak ada proses yang terdefinisi");
	}
?>