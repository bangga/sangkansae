<?php
	switch($proses){
		case "showMess":
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $pesan; ?>
		</div>
	</div>
<?php
			break;
		case "tambahBarang":
			// inquiri parameter jenis
			try{
				$que	= "SELECT jenis_barang FROM ref_barang GROUP BY jenis_barang";
				$data1	= $link->query($que)->fetchAll();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri referensi jenis barang");
				$log->logDB($que);
			}
?>
<script>
	function setJenis(kode,nama){
		$('#namaJenis').html(nama);
		$('#outputJenis').attr('value',kode);
	}
</script>
<div class="modal">
	<!-- Tambah barang -->
	<input 						type="hidden" class="<?php echo $targetId; ?>" name="targetUrl" value="<?php echo _PROC; ?>"/>
	<input 						type="hidden" class="<?php echo $targetId; ?>" name="proses"	value="tambahBarang"/>
	<input 						type="hidden" class="<?php echo $targetId; ?>" name="targetId"	value="targetFormId"/>
	<input id="outputJenis"		type="hidden" class="<?php echo $targetId; ?>" name="jenis_barang" />
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4 class="muted">Form Tambah Barang</h4>
	</div>
	<div class="modal-body">
		<div id="targetFormId" class="row-fluid">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label">Nama Barang</label>
					<div class="controls">
						<input type="text" class="input-medium text-left <?php echo $targetId; ?>" name="nama_barang" value="<?php echo $nama_barang; ?>" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Jenis Barang</label>
					<div class="controls">
						<div class="btn-group">
							<button class="btn dropdown-toggle" data-toggle="dropdown">
								<span id="namaJenis">Pilih Jenis<span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<?php for($i=0;$i<count($data1);$i++){ ?>
								<li><a onclick="setJenis('<?php echo $data1[$i]['jenis_barang']; ?>','<?php echo $data1[$i]['jenis_barang']; ?>')"><?php echo $data1[$i]['jenis_barang']; ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
				<br/><br/><br/><hr/>
				<div class="control-group">
					<label class="control-label" for="inputSubmit"></label>
					<div class="controls">
						<div class="btn-group">
							<button class="btn" onclick="tutup('<?php echo $targetId; ?>')">Kembali</button>
							<button class="btn" onclick="buka('<?php echo $targetId; ?>')">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		case "editBarang":
			if($nama_satuan == '-') $nama_satuan = "Pilih Satuan";
			// inquiri parameter satuan
			try{
				$que	= "SELECT kode_satuan,nama_satuan FROM ref_satuan ORDER BY nama_satuan";
				$data0	= $link->query($que)->fetchAll();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri referensi satuan");
				$log->logDB($que);
			}
			
			// inquiri parameter jenis
			try{
				$que	= "SELECT jenis_barang FROM ref_barang GROUP BY jenis_barang";
				$data1	= $link->query($que)->fetchAll();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri referensi jenis barang");
				$log->logDB($que);
			}
?>
<script>
	function setSatuan(kode,nama){
		$('#namaSatuan').html(nama);
		$('#outputSatuan').attr('value',kode);
	}
	function setJenis(kode,nama){
		$('#namaJenis').html(nama);
		$('#outputJenis').attr('value',kode);
	}
</script>
<div class="modal">
	<!-- Tambah barang -->
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="targetUrl" 	value="<?php echo _PROC; 			?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?>" 									name="proses"		value="editBarang"/>
	<input 						type="hidden" class="hapus<?php echo $targetId; ?>" 								name="proses"		value="hapusBarang"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="targetId"		value="targetFormId"/>
	<input						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="kode_barang" 	value="<?php echo $kode_barang; 	?>" />
	<input id="outputSatuan"	type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="kode_satuan" 	value="<?php echo $kode_satuan; 	?>" />
	<input id="outputJenis"		type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="jenis_barang" value="<?php echo $jenis_barang;	?>" />
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4 class="muted">Form Edit Barang</h4>
		<hr/>
		<div id="targetFormId" class="span5"></div>
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label">Nama Barang</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" class="input-medium text-left edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" name="nama_barang" value="<?php echo $nama_barang; ?>" onmouseover="$(this).select()" />
						<div class="btn-group">
							<button class="btn dropdown-toggle" data-toggle="dropdown">
								<span id="namaSatuan"><?php echo $nama_satuan; ?><span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<?php for($i=0;$i<count($data0);$i++){ ?>
								<li><a onclick="setSatuan('<?php echo $data0[$i]['kode_satuan']; ?>','<?php echo $data0[$i]['nama_satuan']; ?>')"><?php echo $data0[$i]['nama_satuan']; ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Jenis Barang</label>
				<div class="controls">
					<div class="btn-group">
						<button class="btn dropdown-toggle" data-toggle="dropdown">
							<span id="namaJenis"><?php echo $jenis_barang; ?><span>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<?php for($i=0;$i<count($data1);$i++){ ?>
							<li><a onclick="setJenis('<?php echo $data1[$i]['jenis_barang']; ?>','<?php echo $data1[$i]['jenis_barang']; ?>')"><?php echo $data1[$i]['jenis_barang']; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Harga Dasar</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" class="input-medium text-right edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" name="harga_dasar" value="<?php echo number_format($harga_dasar); ?>" onmouseover="$(this).select()" />
						<span class="add-on">Rupiah</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-body">
		<div class="row-fluid">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="inputSubmit"></label>
					<div class="controls">
						<div class="btn-group">
							<button class="btn" onclick="buka('hapus<?php echo $targetId; ?>')">Hapus</button>
							<button class="btn" onclick="buka('edit<?php echo $targetId; ?>')">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		default:
			try{
				if($pg == 1) $jml_perpage = 10;
				$que 	= "select concat(repeat(_utf8'0',(6 - length(b.kode_barang))),b.kode_barang) AS kode_barang,b.nama_barang,b.jenis_barang,IFNULL(c.kode_satuan,'-') AS kode_satuan,IFNULL(c.nama_satuan,'-') AS nama_satuan,IFNULL(a.harga_dasar,0) AS harga_dasar from ref_barang b LEFT JOIN ref_harga a ON(a.kode_barang=b.kode_barang AND a.kode_satuan=a.kode_satuan) LEFT JOIN ref_satuan c ON(c.kode_satuan=a.kode_satuan) ORDER BY IF(IFNULL(a.harga_dasar,0)>0,1,0),b.nama_barang LIMIT $limit_awal,$jml_perpage";
				$sth 	= $link->prepare($que);
				$sth->execute();
				$data	= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 	= null;

				/*	menentukan keberadaan operasi next page	*/
				if(count($data)>=$jml_perpage){
					$next_mess	= "<button class=\"btn\" onclick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data barang");
				$log->logDB($que);
			}
?>
<!-- Show message -->
<input type="hidden" class="showMess" 				name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="showMess" 				name="targetId"		value="targetMessId"/>
<input type="hidden" class="showMess"				name="proses"		value="showMess"/>
<!-- Tambah barang -->
<input type="hidden" class="tambah" 				name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="tambah"					name="proses"		value="tambahBarang"/>
<!-- Pagging -->
<input type="hidden" class="next_page pref_page refresh" 	name="targetId"		value="content" />
<input type="hidden" class="next_page pref_page refresh" 	name="jml_perpage"	value="<?php echo $jml_perpage;	?>" />
<input type="hidden" class="next_page pref_page refresh" 	name="targetUrl" 	value="<?php echo _FILE;		?>"	/>
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $next_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $pref_page; 	?>" />
<input type="hidden" class="refresh" 						name="pg" 			value="<?php echo $pg; 	?>" />

<h4 class="muted cetak"><?php echo _NAME; ?></h4>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder"></div>
<div class="row-fluid" id="listBarang">
	<div class="span12">
		<div class="skrin">Daftar Harga per <?php echo $tgl_sekarang; ?></div>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="center prn_head">Kode Barang</th>
					<th class="center prn_head">Nama Barang</th>
					<th class="center prn_head">Satuan</th>
					<th class="center prn_head">Jenis</th>
					<th class="center prn_head">Harga Jual</th>
				</tr>
			</thead>
			<tbody>
<?php			
			for($i=0;$i<count($data);$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
?>
				<tr onclick="nonghol('editBarang<?php echo $i; ?>')">
					<td class="right prn_cell">			<?php echo $kode_barang; 	?></td>
					<td class="right prn_left prn_cell"><?php echo $nama_barang;	?></td>
					<td class="right prn_left prn_cell"><?php echo $nama_satuan;	?></td>
					<td class="right prn_left prn_cell">
						<div class="cetak"><?php echo $jenis_barang;				?></div>
					</td>
					<td class="right prn_cell">
						<div class="text-right"><?php echo number_format($harga_dasar); 	?></div>
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="targetUrl"		value="<?php echo _FILE; 			?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="proses"			value="editBarang" 						/>
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="tutupId" 		value="<?php echo $targetId; 		?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="nama_barang"		value="<?php echo $nama_barang;	 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="kode_barang"		value="<?php echo $kode_barang;	 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="nama_satuan"		value="<?php echo $nama_satuan;	 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="kode_satuan"		value="<?php echo $kode_satuan;	 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="jenis_barang"	value="<?php echo $jenis_barang;	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="harga_dasar"		value="<?php echo $harga_dasar;	 	?>" />
					</td>
				</tr>
<?php
			}
?>
			</tbody>
			<thead class="cetak">
				<tr>
					<th colspan="4">
						<div class="btn-group">
							<?php echo $pref_mess; 	?>
							<?php echo $tambah; 	?>
							<?php echo $next_mess; 	?>
						</div>
					</th>
					<th>Halaman <?php echo $pg; ?></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<?php
	}
?>