<?php
	if(!$erno) die();
	switch($proses){
		case "showMess":
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $pesan; ?>
		</div>
	</div>
<?php
			break;
		case "listBarang":
			try {
				$kode_po	= $_SESSION['kode_po'];
				$que		= "SELECT a.kode_pembelian,b.kode_barang,b.nama_barang,c.kode_satuan,c.nama_satuan,a.jumlah_pembelian,a.harga,d.harga_dasar,(a.jumlah_pembelian*a.harga) AS harga_total,a.kode_status FROM tabel_pembelian a JOIN ref_harga d ON(d.kode_barang=a.kode_barang AND d.kode_satuan=a.kode_satuan) JOIN ref_barang b ON(b.kode_barang=a.kode_barang) JOIN ref_satuan c ON(c.kode_satuan=a.kode_satuan) WHERE a.kode_po='".$kode_po."' ORDER BY b.nama_barang";
				$sth 		= $link->prepare($que);
				$sth->execute();
				$data		= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 		= null;
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess('Gagal melakukan inquiri data barang');
				$log->logDB($que);
				$erno = true;
			}

			$resi_no	  = base_convert($kode_po, 10, 36);
			// line untuk ff continous paper
			$stringCetak  = chr(27).chr(67).chr(1);
			// enable paper out sensor
			$stringCetak .= chr(27).chr(57);
			// draft mode
			$stringCetak .= chr(27).chr(120).chr(48);
			// line spacing x/72
			$stringCetak .= chr(27).chr(65).chr(12);
			$stringCetak .= str_repeat(" ",59)."Daftar Pembelian".chr(10);
			$stringCetak .= printLeft($appl_owner,59).printLeft("No",11).": ".strtoupper($resi_no).chr(10);
			$stringCetak .= printLeft($reff_owner,59).printLeft("Tanggal",11).": ".$tgl_sekarang.chr(10);
			$stringCetak .= printLeft($add1_owner,59).printLeft("Vendor",11).": ".$_SESSION['nama'].chr(10);
			$stringCetak .= printLeft($phon_owner.", ".$pfax_owner,59).printLeft("Alamat",11).": ".$_SESSION['alamat'].chr(10);
			$stringCetak .= str_repeat("_",119).chr(10);
			$stringCetak .= printLeft("No.",4).printLeft("Kode Item",10).printLeft("Nama Item",30).printLeft("Jumlah",7).printLeft("Satuan",10).printRight("Harga Beli ",15).printRight("Disk (%)",9).printRight("Harga Disk",15).printRight("Total",19).chr(10);
			$stringCetak .= str_repeat("-",119).chr(10);
?>
<div class="span12">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Nama Barang</th>
				<th>Satuan</th>
				<th>Harga Perolehan</th>
				<th>Harga Penjualan</th>
				<th>Jumlah Item</th>
				<th>Total Harga</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
<?
				$grand_total = array(0);
				for($i=0;$i<count($data);$i++){
					/** getParam 
						memindahkan semua nilai dalam array POST ke dalam
						variabel yang bersesuaian dengan masih kunci array
					*/
					$nilai	= $data[$i];
					if(count($nilai)>0){
						$konci	= array_keys($nilai);
					}
					for($b=0;$b<count($konci);$b++){
						$$konci[$b]	= $nilai[$konci[$b]];
					}
					/* getParam **/
					$grand_total[] = $harga_total;
					$stringCetak  .= printLeft(($i+1),4).printLeft($kode_barang,10).printLeft($nama_barang,30).printLeft($jumlah_pembelian,7).printLeft($nama_satuan,10)." ".str_repeat("_",14)." ".str_repeat("_",8)." ".str_repeat("_",14)." ".str_repeat("_",18).chr(10);
?>
			<tr>
				<td><?php echo $nama_barang; 		?></td>
				<td><?php echo $nama_satuan; 		?></td>
				<td><div class="text-right"><?php echo number_format($harga); 			?></div></td>
				<td><div class="text-right"><?php echo number_format($harga_dasar); 	?></div></td>
				<td><div class="text-right"><?php echo number_format($jumlah_pembelian);?></div></td>
				<td><div class="text-right"><?php echo number_format($harga_total); 	?></div></td>
				<td id="<?php echo $kode_pembelian; ?>">
					<?php if($kode_status==3){ ?>
					<button class="btn btn-danger" onclick="buka('<?php echo $kode_pembelian; ?>')">Hapus</button>
					<!-- Parameter proses hapus draft barang -->
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="targetUrl" 		value="<?php echo _PROC; 			?>" />
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="kode_pembelian" 	value="<?php echo $kode_pembelian; 	?>" />
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="nama_barang"	 	value="<?php echo $nama_barang; 	?>" />
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="nama_satuan" 		value="<?php echo $nama_satuan; 	?>" />
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="jumlah_pembelian"	value="<?php echo $jumlah_pembelian;?>" />
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="targetId" 		value="<?php echo $kode_pembelian; 	?>" />
					<input type="hidden" class="<?php echo $kode_pembelian; ?>" name="proses" 			value="deleteDraft" 					/>
					<?php } ?>
				</td>
			</tr>
<?php
				}
				$stringCetak .= str_repeat("-",119).chr(12);
				
?>
			<tr id="targetValidasi" class="cetak">
				<td colspan="5">
					<button class="btn" onclick="buka('cetak')">Cetak</button>
					<input type="hidden" class="cetak" 		name="targetUrl" 	value="<?php echo _PROC; ?>"/>
					<input type="hidden" class="cetak" 		name="targetId"		value="targetMessId"/>
					<input type="hidden" class="cetak"		name="proses"		value="cetakResi"/>
					<input type="hidden" class="cetak" name="stringCetak" 	value="<?php echo base64_encode($stringCetak); 	?>" />
					<input type="hidden" class="cetak" name="stringFile" 	value="<?php echo $resi_no; 					?>" />
				</td>
				<td><div class="text-right"><?php echo number_format(array_sum($grand_total)); ?></div></td>
				<td>
					<?php if($kode_status==3){ ?>
					<button class="btn" onclick="buka('validasi')">Validasi</button>
					<?php } ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?php
			break;
		case "setBarang":
			// random id untuk proses set barang
			$randId	= getToken(mt_rand(1,9999));
			// random id untuk proses kembali
			$backId	= getToken(mt_rand(1,9999));
			// inquiri parameter satuan
			try{
				$que	= "SELECT a.kode_satuan,a.nama_satuan FROM ref_satuan a JOIN ref_harga b ON(b.kode_satuan=a.kode_satuan) JOIN ref_barang c ON(c.kode_barang=b.kode_barang) WHERE b.kode_barang=".$kode_barang." ORDER BY a.nama_satuan";
				$data	= $link->query($que)->fetchAll();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri referensi satuan");
				$log->logDB($que);
			}
?>
<script>
	function setSatuan(kode,nama){
		$('#outputSatuan').html(nama);
		$('#inputSatuan').attr('value',kode);
		$('.setForm').prop('disabled',false);
	}
	function setHarga(opt){
		var inputJumlah = $('#inputJumlah').attr('value');
		var outputTotal = opt*inputJumlah;
		$('#inputHarga').attr('value',opt);
		$('#outputHarga').attr('value',$.formatNumber(opt, {format:'#,###', locale:'us'}));
		$('#outputTotal').attr('value',$.formatNumber(outputTotal, {format:'#,###', locale:'us'}));
		if(outputTotal>0){
			$('#submitDraft').prop('disabled',false);
		}
		else{
			$('#submitDraft').prop('disabled',true);
		}
	}
	function setJumlah(opt){
		var inputHarga = $('#inputHarga').attr('value');
		var outputTotal = opt*inputHarga;
		$('#inputJumlah').attr('value',opt);
		$('#outputJumlah').attr('value',$.formatNumber(opt, {format:'#,###', locale:'us'}));
		$('#outputTotal').attr('value',$.formatNumber(outputTotal, {format:'#,###', locale:'us'}));
		if(outputTotal>0){
			$('#submitDraft').prop('disabled',false);
			$('#submitDraft').focus();
		}
		else{
			$('#submitDraft').prop('disabled',true);
		}
	}
</script>
<input id="inputHarga" 		type="hidden" class="<?php echo $randId; ?>" name="harga" 				value="0" />
<input id="inputJumlah" 	type="hidden" class="<?php echo $randId; ?>" name="jumlah_pembelian"	value="0" />
<input id="inputHargaJual" 	type="hidden" class="<?php echo $randId; ?>" name="harga_dasar" 		value="0" />
<input id="inputSatuan" 	type="hidden" class="<?php echo $randId; ?>" name="kode_satuan"			value="0" />
<input 						type="hidden" class="<?php echo $randId; ?>" name="targetId"			value="tambahPOresult" 				/>
<input 						type="hidden" class="<?php echo $randId; ?>" name="targetUrl"			value="<?php echo _PROC; ?>" 		/>
<input 						type="hidden" class="<?php echo $randId; ?>" name="nama_barang"			value="<?php echo $nama_barang; ?>" />
<input 						type="hidden" class="<?php echo $randId; ?>" name="kode_barang"			value="<?php echo $kode_barang; ?>" />
<input 						type="hidden" class="<?php echo $randId; ?>" name="backId"				value="<?php echo $backId; 		?>" />
<input 						type="hidden" class="<?php echo $randId; ?>" name="proses"				value="tambahPO" 					/>
<!-- Parameter proses kembali ke halaman perncarian barang -->
<input type="hidden" class="<?php echo $backId; ?>" name="targetUrl" 	value="<?php echo _FILE; ?>" 		/>
<input type="hidden" class="<?php echo $backId; ?>" name="targetId"		value="<?php echo $targetId; 	?>"	/>
<input type="hidden" class="<?php echo $backId; ?>" name="proses"		value="cariBarang"					/>
<input type="hidden" class="<?php echo $backId; ?>" name="pg"			value="<?php echo $back;		?>"	/>
<?php if(strlen($kunci)>0){ ?>
<input type="hidden" class="<?php echo $backId; ?>" name="kunci"		value="<?php echo $kunci; 		?>"	/>
<?php } ?>
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<div class="input-prepend input-append">
			<span class="add-on"><?php echo $kode_barang; ?></span>
			<input class="span2" type="text" value="<?php echo $nama_barang; ?>" disabled />
			<div class="btn-group">
				<button class="btn dropdown-toggle" data-toggle="dropdown">
					<span id="outputSatuan">Pilih Satuan<span>
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<?php for($i=0;$i<count($data);$i++){ ?>
					<li><a onclick="setSatuan('<?php echo $data[$i]['kode_satuan']; ?>','<?php echo $data[$i]['nama_satuan']; ?>')"><?php echo $data[$i]['nama_satuan']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="modal-body">
		<div id="tambahPOresult" class="row-fluid">
			<div class="span6">
				<div class="form-horizontal">
					<div class="control-group">
						<label class="control-label" for="inputHarga">Harga Perolehan</label>
						<div class="controls">
							<input id="outputHarga" type="text" class="input-medium text-right setForm" value="0" onblur="setHarga(this.value)" onmouseover="$(this).select()" disabled />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputJumlah">Harga Jual</label>
						<div class="controls">
							<input id="outputHargaJual" type="text" class="input-medium text-right setForm" value="0" onblur="$('#inputHargaJual').attr('value',this.value)" onmouseover="$(this).select()" disabled />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputJumlah">Jumlah Item</label>
						<div class="controls">
							<input id="outputJumlah" type="text" class="input-medium text-right setForm" value="0" onblur="setJumlah(this.value)" onmouseover="$(this).select()" disabled />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="outputTotal">Total</label>
						<div class="controls">
							<input type="text" id="outputTotal" class="input-medium text-right" value="0" disabled />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputSubmit"></label>
						<div class="controls">
							<div class="btn-group">
								<button id="submitDraft" class="btn" onclick="buka('<?php echo $randId; ?>')" disabled>Simpan</button>
								<button class="btn" onclick="buka('<?php echo $backId; ?>')">Kembali</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		case "cariBarang":
			// Form pencarian barang
			if(isset($rinci)){
				$kunci	= $rinci;
			}
			try {
				$que		= "SELECT a.nama_barang,a.kode_barang,a.jenis_barang FROM ref_barang a WHERE a.nama_barang LIKE '%$kunci%' ORDER BY a.nama_barang LIMIT $limit_awal,$jml_perpage";
				$sth 		= $link->prepare($que);
				$sth->execute();
				$data		= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 		= null;

				/*	menentukan keberadaan operasi next page	*/
				if(count($data)>=$jml_perpage){
					$next_mess	= "<button class=\"btn\" onClick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess('Gagal melakukan inquiri data barang');
				$log->logDB($que);
				$erno = true;
			}
			
			if(strlen($kunci)==0){
				$placeholder = "placeholder=\"Kode atau Nama Barang\"";
			}
			else{
				$placeholder = "value=\"".$kunci."\"";
			}
?>
<?php if(strlen($kunci)>0){ ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="kunci" 		value="<?php echo $kunci; 		?>" />
<?php } ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="targetId"		value="<?php echo $targetId;	?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="targetUrl" 	value="<?php echo _FILE;		?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="proses" 		value="<?php echo "cariBarang";	?>" />
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $next_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $pref_page; 	?>" />
<input type="hidden" class="next_cari" 						name="pg" 			value="<?php echo "1";			?>" />
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<div class="form-search">
			<input type="text" class="input-medium search-query span5 next_cari" name="rinci" <?php echo $placeholder; ?> onchange="buka('next_cari')" onmouseover="$(this).select()" />
		</div>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
		  <thead>
			<tr>
				<th>Nama Obat</th>
				<th>Jenis Obat</th>
				<th></th>
			</tr>
		  </thead>
		  <tbody>
<?php
			if(count($data)<=$jml_perpage){
				$jml_perpage=count($data);
			}
			for($i=0;$i<$jml_perpage;$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
				$randId	= getToken(mt_rand(1,9999));
?>
			<tr>
				<td><?php echo $nama_barang;	?></td>
				<td><?php echo $jenis_barang;	?></td>
				<td>
					<button class="btn btn-mini" onclick="buka('<?php echo $randId; ?>')">Pilih</button>
					<input type="hidden" class="<?php echo $randId; ?>" name="targetUrl"	value="<?php echo _FILE;			?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="targetId"		value="<?php echo $targetId;		?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="proses"		value="setBarang" 						/>
					<input type="hidden" class="<?php echo $randId; ?>" name="kode_barang" 	value="<?php echo $kode_barang; 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="nama_barang"	value="<?php echo $nama_barang;	 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="back"			value="<?php echo $pg;	 			?>" />
					<?php if(strlen($kunci)>0){ ?>
					<input type="hidden" class="<?php echo $randId; ?>" name="kunci"		value="<?php echo $kunci;	 		?>" />
					<?php } ?>
				</td>
			</tr>
<?php
			}
?>
			<tr>
				<td colspan="3">
					<div class="btn-group">
					  <?php echo $pref_mess; ?>
					  <?php echo $next_mess; ?>
					</div>
				</td>
			</tr>
		  </tbody>
		</table>
	</div>
</div>
<?php
			break;
		case "refOrder":
			if(count($_SESSION)>0){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$data	= $_SESSION;
				$konci	= array_keys($data);
				for($i=0;$i<count($konci);$i++){
					$$konci[$i]	= $data[$konci[$i]];
				}
				/* getParam **/
			}
?>
	<div class="span6">
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputPelanggan">Pelanggan</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" class="span7 cariNopel setNopel" name="kunci" placeholder="<?php echo $kode_pelanggan; ?>" disabled />
						<button class="btn" onclick="buka('resetPO')">Reset</button>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputBarang">Barang</label>
				<div class="controls">
					<div class="input-append">
						<input id="inputBarang" type="text" class="span7 cariBarang" name="kunci" placeholder="Nama atau Kode" onchange="nonghol('cariBarang')" onmouseover="$(this).select()" />
						<button class="btn" onclick="nonghol('cariBarang')">Cari</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span6">
		<table class="table table-bordered">
			<tr>
				<td>Nama</td>
				<td>
					<?php echo $nama; 	?>
					<span class="badge badge-success"><?php echo $badge; ?></span>
				</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><?php echo $alamat; ?></td>
			</tr>
		</table>
	</div>
<?php
			break;
		case "cariNopel":
			$kode_do	= $_SESSION['kode_do'];
			if(isset($rinci)){
				$kunci	= $rinci;
			}
			try {
				$que	= "SELECT kode_vendor AS kode_pelanggan,nama,alamat,kota,'0.5' AS badge FROM tabel_vendor WHERE kode_vendor LIKE '%".$kunci."%' OR nama LIKE '%".$kunci."%' ORDER BY nama LIMIT $limit_awal,$jml_perpage";
				$sth 	= $link->prepare($que);
				$sth->execute();
				$data	= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 	= null;

				/*	menentukan keberadaan operasi next page	*/
				if(count($data)>=$jml_perpage){
					$next_mess	= "<button class=\"btn\" onclick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess('Gagal melakukan inquiri data pelanggan');
				$log->logDB($que);
				$erno = true;
			}
			
			if(strlen($kunci)==0){
				$placeholder = "placeholder=\"Nomer atau Nama Pelanggan\"";
			}
			else{
				$placeholder = "value=\"".$kunci."\"";
			}
?>
<div class="modal">
<?php if(strlen($kunci)>0){ ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="kunci" 		value="<?php echo $kunci; 		?>" />
<?php } ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="proses" 		value="<?php echo $proses; 		?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="targetId"		value="<?php echo $targetId;	?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="targetUrl" 	value="<?php echo _FILE;		?>" />
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $next_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $pref_page; 	?>" />
<input type="hidden" class="next_cari" 						name="pg" 			value="1" />
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<div class="form-search">
			<input type="text" class="input-medium search-query span5 next_cari" name="rinci" <?php echo $placeholder; ?> onchange="buka('next_cari')" onmouseover="$(this).select()" />
		</div>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
		  <thead>
			<tr>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Badge</th>
			</tr>
		  </thead>
		  <tbody>
<?php
			if(count($data)<=$jml_perpage){
				$jml_perpage=count($data);
			}
			for($i=0;$i<$jml_perpage;$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
?>
			<tr>
				<td><?php echo $nama; 	?></td>
				<td><?php echo $alamat; ?></td>
				<td>
					<a class="badge badge-success" onclick="buka('setNopel<?php echo $targetId; ?>')"><?php echo $badge; ?></a>
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="targetUrl"		value="<?php echo _PROC; 			?>" />
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="targetId"		value="targetMessId" 					/>
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="proses"			value="setNopel" 						/>
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="tutupId" 		value="<?php echo $targetId; 		?>" />
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="kode_pelanggan" value="<?php echo $kode_pelanggan; 	?>" />
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="nama"			value="<?php echo $nama;			?>" />
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="badge"		 	value="<?php echo $badge;		 	?>" />
					<input type="hidden" class="setNopel<?php echo $targetId; ?>" name="alamat"		 	value="<?php echo $alamat;		 	?>" />
				</td>
			</tr>
<?php
			}
?>
			<tr>
				<td colspan="3">
					<div class="btn-group">
					  <?php echo $pref_mess; ?>
					  <?php echo $next_mess; ?>
					</div>
				</td>
			</tr>
		  </tbody>
		</table>
	</div>
</div>
<?php
			break;
		default:
?>
<!-- Show message -->
<input type="hidden" class="showMess" 	name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="showMess" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="showMess"	name="proses"		value="showMess"/>
<!-- Form pencarian pelanggan -->
<input type="hidden" class="cariNopel" 	name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="cariNopel" 	name="proses" 		value="cariNopel"/>
<!-- Set pelanggan berdasarkan nomer pelanggan -->
<input type="hidden" class="setNopel" 	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="setNopel" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="setNopel" 	name="proses" 		value="setNopel"/>
<!-- Reset order -->
<input type="hidden" class="resetPO" 	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="resetPO" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="resetPO"	name="proses"		value="deletePO"/>
<!-- Refresh pelanggan -->
<input type="hidden" class="refOrder" 	name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="refOrder" 	name="targetId"		value="refOrder"/>
<input type="hidden" class="refOrder"	name="proses"		value="refOrder"/>
<!-- Form pencarian barang -->
<input type="hidden" class="cariBarang"	name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="cariBarang"	name="proses"		value="cariBarang"/>
<!-- Refresh list barang -->
<input type="hidden" class="refBarang" 	name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="refBarang" 	name="targetId"		value="listBarang"/>
<input type="hidden" class="refBarang"	name="proses"		value="listBarang"/>
<!-- Validasi order -->
<input type="hidden" class="validasi" 	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="validasi" 	name="targetId"		value="targetValidasi"/>
<input type="hidden" class="validasi"	name="proses"		value="validasi"/>

<h4 class="muted"><?php echo _NAME; ?></h4>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder">
	<div class="span6">
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputPelanggan">Vendor</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" class="span6 cariNopel setNopel" name="kunci" placeholder="Nama atau Kode" onmouseover="$(this).select()" />
						<button class="btn" onclick="buka('setNopel')">Set</button>
						<button class="btn" onclick="nonghol('cariNopel')">Cari</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row-fluid" id="listBarang"></div>
<?php
	}
?>