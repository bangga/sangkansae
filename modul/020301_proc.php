<?php
	switch($proses){
		case "editBarang":
			try {
				$pesan 	= "<strong>Succes!</strong> Perubahan referensi ".$nama_barang." telah dilakukan";
				$kelas	= "alert-succes";
				$link->beginTransaction();
				
				$que	= "UPDATE ref_barang SET nama_barang='".$nama_barang."',jenis_barang='".$jenis_barang."' WHERE kode_barang=ABS(".$kode_barang.")";
				$res 	= $link->exec($que);
				$log->logDB($que);
				
				$que	= "INSERT ref_harga(kode_barang,kode_satuan,harga_dasar) VALUES(ABS(".$kode_barang."),".$kode_satuan.",".$harga_dasar.") ON DUPLICATE KEY UPDATE harga_dasar=".$harga_dasar;
				$res 	= $link->exec($que);
				$log->logDB($que);
				
				$log->logMess($pesan);				
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan proses perubahan referensi barang";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
	<script>buka('refresh');</script>
	<div class="span4">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
	</div>
<?php
			break;
		case "hapusBarang":
			try {
				$pesan 	= "<strong>Succes!</strong> Penghapusan ".$nama_barang." telah dilakukan";
				$kelas	= "alert-succes";

				$link->beginTransaction();
				$que	= "DELETE FROM ref_harga WHERE kode_barang=ABS(".$kode_barang.") AND kode_satuan=".$kode_satuan;
				$res 	= $link->exec($que);
				$log->logDB($que);
					
				$que	= "DELETE FROM ref_barang WHERE kode_barang=ABS(".$kode_barang.") AND kode_barang NOT IN(SELECT kode_barang FROM ref_harga)";
				$res 	= $link->exec($que);
				$log->logDB($que);
				
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan proses penghapusan barang";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
	<script>buka('refresh');</script>
	<div class="span4">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
	</div>
<?php
			break;
		case "tambahBarang":
			try {
				$pesan 	= "<strong>Succes!</strong> Penambahan ".$nama_barang." telah dilakukan";
				$kelas	= "alert-succes";
				$link->beginTransaction();
				$que	= "INSERT INTO ref_barang(nama_barang,jenis_barang) VALUES('".$nama_barang."','".$jenis_barang."')";
				$res 	= $link->exec($que);
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan proses penambahan barang";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
	<script>buka('refresh');</script>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
	</div>
<?php
			break;
		default :
			$log->logMess("Tidak ada proses yang terdefinisi");
	}
?>