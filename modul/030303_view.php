<?php
	switch($proses){
		case "showMess":
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $pesan; ?>
		</div>
	</div>
<?php
			break;
		case "editReffBiaya":
?>
<script>
	$('#inputKodeBiaya').select();
</script>
<div class="modal">
	<!-- Tambah barang -->
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="targetId"			value="targetFormId"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="targetUrl" 		value="<?php echo _PROC; 			?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="tutupId"			value="<?php echo $targetId;		?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="kode_biaya"		value="<?php echo $kode_biaya;		?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="nama_biaya"		value="<?php echo $nama_biaya;		?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" 	name="kode_transaksi"	value="<?php echo $kode_transaksi;	?>"/>
	<input 						type="hidden" class="edit<?php echo $targetId; ?>" 									name="proses"			value="editReffBiaya"/>
	<input 						type="hidden" class="hapus<?php echo $targetId; ?>" 								name="proses"			value="hapusReffBiaya"/>
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4 class="muted">Input Biaya : <?php echo $nama_biaya; ?></h4>
	</div>
	<div class="modal-body">
		<div id="targetFormId" class="row-fluid">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label">Jumlah</label>
					<div class="controls">
						<input id="inputKodeBiaya" type="text" class="input-medium text-left edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" name="jumlah_biaya" onmouseover="$(this).focus();" value="<?php echo $jumlah_biaya; ?>" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Keterangan</label>
					<div class="controls">
						<input type="text" class="input-medium text-left edit<?php echo $targetId; ?> hapus<?php echo $targetId; ?>" name="keterangan_rinci" value="<?php echo $nama_biaya; ?>" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputSubmit"></label>
					<div class="controls">
						<div class="btn-group">
							<button class="btn" onclick="buka('edit<?php echo $targetId; ?>')">Simpan</button>
							<button class="btn" onclick="buka('hapus<?php echo $targetId; ?>')">Hapus</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		default:
			try{
				$que 			= "SELECT DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL ".($pg-1)." DAY),'%d-%m-%Y') AS tgl_transaksi,IFNULL(b.kode_transaksi,0) AS kode_transaksi,a.kode_biaya,a.keterangan_biaya,a.keterangan_biaya AS nama_biaya,a.keterangan_biaya AS keterangan,IFNULL(b.jumlah_biaya,0) AS jumlah_biaya,IFNULL(b.keterangan,'-') AS keterangan_rinci FROM ref_biaya a LEFT JOIN tabel_biaya b ON(b.kode_biaya=a.kode_biaya AND DATE(getTanggal(b.kode_transaksi))=DATE_SUB(CURDATE(), INTERVAL ".($pg-1)." DAY))";
				$sth 			= $link->prepare($que);
				$sth->execute();
				$data			= $sth->fetchAll(PDO::FETCH_ASSOC);
				$tgl_transaksi	= $data[0]['tgl_transaksi'];
				$link			= null;

				/*	menentukan keberadaan operasi next page	*/
				if($pg>1){
					$next_mess	= "<button class=\"btn\" onclick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data referensi biaya");
				$log->logDB($que);
			}
?>
<!-- Show message -->
<input type="hidden" class="showMess" 				name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="showMess" 				name="targetId"		value="targetMessId"/>
<input type="hidden" class="showMess"				name="proses"		value="showMess"/>
<!-- Tambah barang -->
<input type="hidden" class="tambah" 				name="targetUrl" 	value="<?php echo _FILE; ?>"/>
<input type="hidden" class="tambah"					name="proses"		value="tambahReffBiaya"/>
<!-- Pagging -->
<input type="hidden" class="next_page pref_page refresh" 	name="targetId"		value="content" />
<input type="hidden" class="next_page pref_page refresh" 	name="targetUrl" 	value="<?php echo _FILE;		?>"	/>
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $pref_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $next_page; 	?>" />
<input type="hidden" class="refresh" 						name="pg" 			value="<?php echo $pg; 			?>" />

<h4 class="muted cetak"><?php echo _NAME; ?></h4>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder"></div>
<div class="row-fluid" id="listBarang">
	<div class="span12">
		<div class="skrin">Daftar Referensi Biaya Per <?php echo $tgl_transaksi; ?></div>
		<table class="table table-bordered table-hover">
			<thead class="cetak">
				<tr>
					<th colspan="4">Tanggal : <?php echo $tgl_transaksi; ?></th>
				</tr>
			</thead>
			<thead>
				<tr>
					<th class="center prn_head">Kode Biaya</th>
					<th class="center prn_head">Nama Biaya</th>
					<th class="center prn_head">Biaya</th>
					<th class="center prn_head">Keterangan</th>
				</tr>
			</thead>
			<tbody>
<?php			
			for($i=0;$i<count($data);$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
?>
				<tr onclick="nonghol('editBarang<?php echo $i; ?>')">
					<td class="prn_cell">	<?php echo $kode_biaya;		?></td>
					<td class="prn_cell">	<?php echo $nama_biaya;		?></td>
					<td class="prn_cell">
						<div class="text-right"><?php echo number_format($jumlah_biaya);	?></div>
					<td class="prn_cell">
						<div class="text-left"><?php echo $keterangan_rinci;	?></div>
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="targetUrl"		value="<?php echo _FILE; 			?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="proses"			value="editReffBiaya"					/>
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="tutupId" 		value="<?php echo $targetId; 		?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="kode_transaksi"	value="<?php echo $kode_transaksi; 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="kode_biaya"		value="<?php echo $kode_biaya;	 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="jumlah_biaya"	value="<?php echo $jumlah_biaya; 	?>" />
						<input type="hidden" class="editBarang<?php echo $i; ?>" name="nama_biaya"		value="<?php echo $keterangan;	 	?>" />
					</td>
				</tr>
<?php
			}
?>
			</tbody>
			<thead class="cetak">
				<tr>
					<th colspan="3">
						<div class="btn-group">
							<button class="btn" onclick="buka('pref_page')">Pref</button>
							<?php echo $next_mess; ?>
						</div>
					</th>
					<th>Halaman <?php echo $pg; ?></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<?php
	}
?>