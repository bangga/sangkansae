<?php
	switch($proses){
		case "showMess":
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $pesan; ?>
		</div>
	</div>
<?php
			break;
		case "refOrder":
			if(count($_SESSION)>0){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$data	= $_SESSION;
				$konci	= array_keys($data);
				for($i=0;$i<count($konci);$i++){
					$$konci[$i]	= $data[$konci[$i]];
				}
				/* getParam **/
			}
?>
	<div class="span6">
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputPelanggan">Pelanggan</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" class="span7 cariNopel setNopel" name="kunci" value="<?php echo $kode_pelanggan; ?>" readonly />
						<button class="btn" onclick="buka('resetDO')">Reset</button>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputBarang">Barang</label>
				<div class="controls">
					<div class="input-append">
						<input id="inputBarang" type="text" class="span7 cariBarang" name="kunci" placeholder="Nama atau Kode" onchange="nonghol('cariBarang')" onmouseover="$(this).select()" />
						<button class="btn" onclick="nonghol('cariBarang')">Cari</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span6">
		<table class="table table-bordered">
			<tr>
				<td>Nama</td>
				<td>
					<?php echo $nama; 	?>
					<span class="badge badge-success"><?php echo $badge; ?></span>
				</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><?php echo $alamat; ?></td>
			</tr>
		</table>
	</div>
<?php
			break;
		case "cariNopel":
			$kode_do	= $_SESSION['kode_do'];
			if(isset($rinci)){
				$kunci	= $rinci;
			}
			try {
				$que	= "SELECT kode_pelanggan,nama,alamat,kota,'0.5' AS badge FROM tabel_pelanggan WHERE kode_pelanggan LIKE '%".$kunci."%' OR nama LIKE '%".$kunci."%' ORDER BY nama LIMIT $limit_awal,$jml_perpage";
				$sth 	= $link->prepare($que);
				$sth->execute();
				$data	= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 	= null;

				/*	menentukan keberadaan operasi next page	*/
				if(count($data)>=$jml_perpage){
					$next_mess	= "<button class=\"btn\" onclick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess('Gagal melakukan inquiri data pelanggan');
				$log->logDB($que);
				$erno = true;
			}
			
			if(strlen($kunci)==0){
				$placeholder = "placeholder=\"Nomer atau Nama Pelanggan\"";
			}
			else{
				$placeholder = "value=\"".$kunci."\"";
			}
?>
<div class="modal">
<?php if(strlen($kunci)>0){ ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="kunci" 		value="<?php echo $kunci; 		?>" />
<?php } ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="proses" 		value="<?php echo $proses; 		?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="targetId"		value="<?php echo $targetId;	?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="targetUrl" 	value="modul/030201_form.php"		/>
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $next_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $pref_page; 	?>" />
<input type="hidden" class="next_cari" 						name="pg" 			value="1" />
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<div class="form-search">
			<input type="text" class="input-medium search-query span5 next_cari" name="rinci" <?php echo $placeholder; ?> onchange="buka('next_cari')" onmouseover="$(this).select()" />
		</div>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
		  <thead>
			<tr>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Badge</th>
			</tr>
		  </thead>
		  <tbody>
<?php
			if(count($data)<=$jml_perpage){
				$jml_perpage=count($data);
			}
			for($i=0;$i<$jml_perpage;$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
?>
			<tr>
				<td><?php echo $nama; 	?></td>
				<td><?php echo $alamat; ?></td>
				<td>
					<a class="badge badge-success" onclick="buka('setNopel<?php echo $kode_pelanggan; ?>')"><?php echo $badge; ?></a>
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="targetUrl"		value="<?php echo _PROC; 			?>" />
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="targetId"			value="targetMessId" 					/>
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="proses"			value="setNopel" 						/>
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="tutupId" 			value="<?php echo $targetId; 		?>" />
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="kode_pelanggan" 	value="<?php echo $kode_pelanggan; 	?>" />
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="nama"			 	value="<?php echo $nama;		 	?>" />
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="badge"		 	value="<?php echo $badge;		 	?>" />
					<input type="hidden" class="setNopel<?php echo $kode_pelanggan; ?>" name="alamat"		 	value="<?php echo $alamat;		 	?>" />
				</td>
			</tr>
<?php
			}
?>
			<tr>
				<td colspan="3">
					<div class="btn-group">
					  <?php echo $pref_mess; ?>
					  <?php echo $next_mess; ?>
					</div>
				</td>
			</tr>
		  </tbody>
		</table>
	</div>
</div>
<?php
			break;
		case "formBayar":
?>
<script>
	function setKembali(opt){
		var inputTotal 		= $('#inputTotal').attr('value');
		var inputKembali 	= opt - parseInt(inputTotal);
		if(inputKembali>=0){
			$('#inputKembali').attr('value',$.formatNumber(inputKembali, {format:'#,###', locale:'us'}));
			$('#jatuhTempo').hide();
			$('#submitDraft').prop('disabled', false);
			$('#submitDraft').focus();
		}
		else{
			$('#submitDraft').prop('disabled', true);
			$('#inputKembali').attr('value',0);
			$('#jatuhTempo').show();
			$('#setJatuhTempo').focus();
		}
	}
	function setJatuhTempo(opt){
		if(opt>0){
			$('#submitDraft').prop('disabled', false);
			$('#submitDraft').focus();
		}
		else{
			$('#submitDraft').prop('disabled', true);
		}
	}
</script>
<input type="hidden" class="setBayar" name="tutupId" value="<?php echo $targetId; ?>" />
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4 class="muted">Form Pembayaran</h4>
	</div>
	<div class="modal-body">
		<div id="targetBayarId" class="row-fluid">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="inputTagihan">Total Tagihan</label>
					<div class="controls">
						<input type="text" class="input-medium text-right" value="<?php echo number_format($grand_total); ?>" disabled />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputDibayar">Dibayar</label>
					<div class="controls">
						<input type="text" id="setKembali" class="input-medium text-right setBayar" name="do_dibayar" placeholder="<?php echo number_format($grand_total); ?>" onchange="setKembali(this.value)" onmouseover="$(this).select()" <?php if($grand_total==0) echo "disabled"; ?> />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Kembali">Kembali</label>
					<div class="controls">
						<input type="text" id="inputKembali" class="input-medium text-right" placeholder="0" disabled />
					</div>
				</div>
				<div class="control-group hide" id="jatuhTempo">
					<label class="control-label" for="Kembali">Jatuh Tempo</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" id="setJatuhTempo" class="input-small text-right setBayar" name="jatuh_tempo" placeholder="0" onchange="setJatuhTempo(this.value)" onmouseover="$(this).select()" />
							<span class="add-on">Hari</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputSubmit"></label>
					<div class="controls">
						<div class="btn-group">
							<button id="submitDraft" class="btn" onclick="buka('setBayar')" disabled >Simpan</button>
							<button class="btn" onclick="tutup('<?php echo $targetId; ?>')">Kembali</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		case "listBarang":
			try {
				if(!isset($kode_do)){
					$kode_do	= $_SESSION['kode_do'];
				}
				$resi_no	= base_convert($kode_do, 10, 36);
				$que		= "SELECT a.kode_penjualan,CONCAT(REPEAT('0',6-LENGTH(b.kode_barang)),b.kode_barang) AS kode_barang,b.nama_barang,c.kode_satuan,c.nama_satuan,a.jumlah_penjualan,a.harga_penjualan,a.diskon,a.harga_diskon,(a.jumlah_penjualan*a.harga_diskon) AS harga_total,a.kode_status FROM tabel_penjualan a JOIN ref_barang b ON(b.kode_barang=a.kode_barang) JOIN ref_satuan c ON(c.kode_satuan=a.kode_satuan) WHERE a.kode_do='".$kode_do."' ORDER BY b.nama_barang";
				$sth 		= $link->prepare($que);
				$sth->execute();
				$data		= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 		= null;
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess('Gagal melakukan inquiri data barang');
				$log->logDB($que);
				$erno = true;
			}
			// line untuk ff continous paper
			$stringCetak  = chr(27).chr(67).chr(1);
			// enable paper out sensor
			$stringCetak .= chr(27).chr(57);
			// draft mode
			$stringCetak .= chr(27).chr(120).chr(48);
			// line spacing x/72
			$stringCetak .= chr(27).chr(65).chr(12);
			$stringCetak .= str_repeat(" ",59)."FAKTUR PENJUALAN".chr(10);
			$stringCetak .= printLeft($appl_owner,59).printLeft("No",11).": ".strtoupper($resi_no).chr(10);
			$stringCetak .= printLeft($reff_owner,59).printLeft("Tanggal",11).": ".$tgl_sekarang.chr(10);
			$stringCetak .= printLeft($add0_owner,59).printLeft("Sales",11).": "._NAMA.chr(10);
			$stringCetak .= printLeft($add1_owner,59).printLeft("Pelanggan",11).": ".$_SESSION['nama'].chr(10);
			$stringCetak .= printLeft($phon_owner.", ".$pfax_owner,59).printLeft("Alamat",11).": ".$_SESSION['alamat'].chr(10);
			$stringCetak .= str_repeat("_",119).chr(10);
			$stringCetak .= printLeft("No.",4).printLeft("Kode Item",10).printLeft("Nama Item",30).printLeft("Jumlah",7).printLeft("Satuan",10).printRight("Harga Normal ",15).printRight("Disk (%)",9).printRight("Harga Disk",15).printRight("Total",19).chr(10);
			$stringCetak .= str_repeat("-",119).chr(10);
?>
<div class="span12">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Nama Barang</th>
				<th>Satuan</th>
				<th>Harga Jual</th>
				<th>Diskon (Persen)</th>
				<th>Harga Diskon</th>
				<th>Banyak</th>
				<th>Total Harga</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
<?php
				$grand_total = array(0);
				$diskn_total = array(0);
				$dasar_total = array(0);
				for($i=0;$i<count($data);$i++){
					/** getParam 
						memindahkan semua nilai dalam array POST ke dalam
						variabel yang bersesuaian dengan masih kunci array
					*/
					$nilai	= $data[$i];
					if(count($nilai)>0){
						$konci	= array_keys($nilai);
					}
					for($b=0;$b<count($konci);$b++){
						$$konci[$b]	= $nilai[$konci[$b]];
					}
					/* getParam **/
					$grand_total[] = $harga_total;
					$diskn_total[] = ($harga_penjualan-$harga_diskon)*$jumlah_penjualan;
					$dasar_total[] = $harga_penjualan*$jumlah_penjualan;
					$stringCetak .= printLeft(($i+1),4).printLeft($kode_barang,10).printLeft($nama_barang,30).printLeft($jumlah_penjualan,7).printLeft($nama_satuan,10).printRight(number_format($harga_penjualan)." ",15).printRight(number_format($diskon)." ",9).printRight(number_format($harga_diskon)." ",15).printRight(number_format($harga_total),19).chr(10);
?>
			<tr>
				<td><?php echo $nama_barang; 		?></td>
				<td><?php echo $nama_satuan; 		?></td>
				<td><div class="text-right"><?php echo number_format($harga_penjualan); ?></div></td>
				<td><div class="text-right"><?php echo number_format($diskon); 			?></div></td>
				<td><div class="text-right"><?php echo number_format($harga_diskon); 	?></div></td>
				<td><div class="text-right"><?php echo number_format($jumlah_penjualan);?></div></td>
				<td><div class="text-right"><?php echo number_format($harga_total); 	?></div></td>
				<td id="<?php echo $kode_penjualan; ?>">
					<?php if($kode_status==3){ ?>
					<button class="btn btn-danger" onclick="buka('<?php echo $kode_penjualan; ?>')">Hapus</button>
					<!-- Parameter proses hapus draft barang -->
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="targetUrl" 		value="<?php echo _PROC; 			?>" />
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="kode_penjualan" 	value="<?php echo $kode_penjualan; 	?>" />
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="nama_barang"	 	value="<?php echo $nama_barang; 	?>" />
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="nama_satuan" 		value="<?php echo $nama_satuan; 	?>" />
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="jumlah_penjualan"	value="<?php echo $jumlah_penjualan;?>" />
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="targetId" 		value="<?php echo $kode_penjualan; 	?>" />
					<input type="hidden" class="<?php echo $kode_penjualan; ?>" name="proses" 			value="deleteDraft" 					/>
					<?php } ?>
				</td>
			</tr>
<?php
				}
				if(!isset($jatuh_tempo)){
					$jatuh_tempo	= 0;
					$cara_bayar 	= "TUNAI";
				}
				if($jatuh_tempo>0){
					$cara_bayar		= "KREDIT";
					$tanggal_tempo  = mktime(0, 0, 0, date("m"), date("d") + $jatuh_tempo, date("Y"));
					$tanggal_tempo 	= date('d/m/Y', $tanggal_tempo);
				}
				else{
					$cara_bayar 	= "TUNAI";
					$tanggal_tempo 	= $tgl_sekarang;
				}
				$sisa_tagihan		= array_sum($grand_total)-$uang_muka;
				$total_tagihan		= array_sum($grand_total);
				$terbilang			= strtoupper(n2c($total_tagihan," RUPIAH"));
				$stringCetak .= str_repeat("-",119).chr(10);
				$stringCetak .= printLeft(SUBSTR($terbilang, 0,89),90).str_repeat(" ",2).printLeft("Jumlah  :",10).printRight(number_format(array_sum($dasar_total)),17).chr(10);
				$stringCetak .= printLeft(SUBSTR($terbilang,90,89),90).str_repeat(" ",2).printLeft("Diskon  :",10).printRight(number_format(array_sum($diskn_total)),17).chr(10);
				$stringCetak .= printLeft("Cara Bayar",12).printLeft(": ".$cara_bayar,80).printLeft("Dibayar :",10).printRight(number_format($uang_muka),17).chr(10);
				$stringCetak .= printLeft("Syarat",12).printLeft(": ".$jatuh_tempo." Hari",80).printLeft("Sisa    :",10).printRight(number_format($sisa_tagihan),17).chr(10);
				$stringCetak .= printLeft("Jatuh Tempo",12).": ".$tanggal_tempo.chr(10);
				$stringCetak .= printCenter("Keterangan :",40).chr(10);
				$stringCetak .= printCenter("Mengetahui",20).printCenter("Penerima",20).chr(10).chr(10).chr(10);
				$stringCetak .= printCenter("_______________",20).printCenter("_______________",20).chr(10);
				$stringCetak .= "Retur paling lambat 1 hari setelah barang diterima. Lewat waktu tersebut klaim tidak dapat diterima.".chr(12);
?>
			<tr>
				<td colspan="6">
					<?php echo $pesan; ?>
					<?php if($kode_status==1){ ?>
					<input type="hidden" class="cetak" name="stringCetak" 	value="<?php echo base64_encode($stringCetak); 	?>" />
					<input type="hidden" class="cetak" name="stringFile" 	value="<?php echo $resi_no; 					?>" />
					<button class="btn" onclick="buka('cetak')">Cetak</button>
					<button class="btn" onclick="buka('<?php echo _KODE; ?>')">Selesai</button>
					<?php } ?>
				</td>
				<td><div class="text-right"><?php echo number_format(array_sum($grand_total)); ?></div></td>
				<td>
					<?php if($kode_status==3){ ?>
					<input type="hidden" id="inputTotal" class="bayar setBayar" name="grand_total" value="<?php echo array_sum($grand_total); ?>" />
					<button class="btn" onclick="nonghol('bayar')">Bayar</button>
					<?php } ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?php
			break;
		case "setBarang":
			$randId	= getToken(mt_rand(1,9999));
			$backId	= getToken(mt_rand(1,9999));
?>
<script>
	function setHarga(opt){
		if(opt.length==0){
			opt = $('#inputHarga').attr('value');
		}
		$('#hargaJual').attr('value',opt);
		hitungTotal();
	}
	function setJumlah(opt){
		if(opt>0){
			var inputStok	= $('#inputStok').attr('value');
			if(parseInt(inputStok)>=parseInt(opt)){
				$('#limitStok').hide();
				$('#inputJumlah').attr('value',opt);
				hitungTotal();
				$('#submitDraft').prop('disabled', false);
			}
			else{
				$('#limitStok').show();
				$('#submitDraft').prop('disabled', true);
			}
		}
		else{
			$('#submitDraft').prop('disabled', true);
		}
	}
	function setDiskon(opt){
		if(opt.length==0){
			opt = 0;
		}
		$('#inputDiskon').attr('value',opt);
		hitungTotal();
	}
	function hitungTotal(){
		var inputJumlah = $('#inputJumlah').attr('value');
		var hargaJual 	= $('#hargaJual').attr('value');
		var inputDiskon = $('#inputDiskon').attr('value');
		if(inputDiskon>0){
			var hargaDiskon	= hargaJual - (inputDiskon*hargaJual/100);
		}
		else{
			var hargaDiskon = hargaJual;
		}
		$('#hargaDiskon').attr('value',hargaDiskon);
		$('#outputDiskon').attr('value',$.formatNumber(hargaDiskon, {format:'#,###', locale:'us'}));
		if(inputJumlah>0){
			var outputTotal = parseInt(inputJumlah*hargaDiskon);
			$('#outputTotal').attr('value',$.formatNumber(outputTotal, {format:'#,###', locale:'us'}));
		}
	}
</script>
<input type="hidden" id="inputHarga" 	value="<?php echo $harga_dasar; ?>" />
<input type="hidden" id="inputStok" 	value="<?php echo $stok; 		?>" />
<!-- Parameter simpan draft barang -->
<input type="hidden" id="hargaJual" 	class="<?php echo $randId; ?>" 	name="harga_jual"		value="<?php echo $harga_dasar; ?>" />
<input type="hidden" id="hargaDiskon" 	class="<?php echo $randId; ?>" 	name="harga_diskon"		value="<?php echo $harga_dasar; ?>" />
<input type="hidden" id="inputDiskon" 	class="<?php echo $randId; ?>" 	name="diskon" 			value="0" 							/>
<input type="hidden" id="inputJumlah" 	class="<?php echo $randId; ?>" 	name="jumlah_penjualan" value="0" 							/>
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="targetId"			value="tambahDOresult" 				/>
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="targetUrl"		value="<?php echo _PROC; ?>" />
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="nama_barang"		value="<?php echo $nama_barang; ?>" />
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="nama_satuan"		value="<?php echo $nama_satuan; ?>" />
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="kode_barang"		value="<?php echo $kode_barang; ?>" />
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="kode_satuan"		value="<?php echo $kode_satuan; ?>" />
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="backId"			value="<?php echo $backId; 		?>" />
<input type="hidden" 					class="<?php echo $randId; ?>" 	name="proses"			value="tambahDO" 					/>
<!-- Parameter proses kembali ke halaman perncarian barang -->
<input type="hidden" 					class="<?php echo $backId; ?>" 	name="targetUrl" 		value="modul/030201_form.php"		/>
<input type="hidden" 					class="<?php echo $backId; ?>" 	name="targetId"			value="<?php echo $targetId; 	?>"	/>
<input type="hidden" 					class="<?php echo $backId; ?>" 	name="pg"				value="<?php echo $back;		?>"	/>
<?php if(strlen($kunci)>0){ ?>
<input type="hidden" 					class="<?php echo $backId; ?>" 	name="kunci"			value="<?php echo $kunci; 		?>"	/>
<?php } ?>
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<div class="input-prepend input-append">
			<span class="add-on"><?php echo $kode_barang; ?></span>
			<input class="span2" type="text" value="<?php echo $nama_barang; ?>" disabled />
			<span class="add-on"><?php echo $nama_satuan; ?></span>
		</div>
	</div>
	<div class="modal-body">
		<div id="tambahDOresult" class="row-fluid">
			<div id="limitStok" class="alert alert-notice hide">
				<button type="button" class="close" onclick="$('#limitStok').hide()">&times;</button>
				<strong>Notice!</strong> Stok tidak mencukupi
			</div>
			<div class="span6">
				<div class="form-horizontal">
					<div class="control-group">
						<label class="control-label" for="inputHarga">Harga Jual</label>
						<div class="controls">
							<?php if(_KODE=='030202'){ ?>
							<input type="text" id="outputHarga" class="input-medium text-right" placeholder="<?php echo number_format($harga_dasar); ?>" onblur="setHarga(this.value)" onmouseover="$(this).select()" />
							<?php } else{ ?>
							<input type="text" id="outputHarga" class="input-medium text-right" value="<?php echo number_format($harga_dasar); ?>" disabled />
							<?php } ?>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputJumlah">Banyak</label>
						<div class="controls">
								<input type="text" class="input-medium" placeholder="Stok <?php echo $stok; ?>" onblur="setJumlah(this.value)" onmouseover="$(this).select()" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputDiskon">Diskon</label>
						<div class="controls">
							<div class="input-append">
								<input type="text" class="input-small" placeholder="0" onblur="setDiskon(this.value)" onmouseover="$(this).select()" />
								<span class="add-on">%</span>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="outputDiskon">Harga Diskon</label>
						<div class="controls">
							<input id="outputDiskon" type="text" class="input-medium text-right" value="<?php echo number_format($harga_dasar); ?>" disabled />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="outputTotal">Total</label>
						<div class="controls">
							<input type="text" id="outputTotal" class="input-medium text-right" value="0" disabled />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputSubmit"></label>
						<div class="controls">
							<div class="btn-group">
								<button id="submitDraft" class="btn" onclick="buka('<?php echo $randId; ?>')" disabled>Simpan</button>
								<button class="btn" onclick="buka('<?php echo $backId; ?>')">Kembali</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
			break;
		default:
			// Form pencarian barang
			if(isset($rinci)){
				$kunci	= $rinci;
			}
			try {
				$kode_do	= $_SESSION['kode_do'];
				$que		= "SELECT a.*,SUM(IFNULL(d.jumlah_penjualan,0)) AS draft FROM (SELECT b.kode_barang,b.nama_barang,c.kode_satuan,c.nama_satuan,d.harga_dasar,SUM(a.jumlah_stok) AS stok FROM tabel_pembelian a JOIN ref_harga d ON(d.kode_barang=a.kode_barang AND d.kode_satuan=a.kode_satuan AND a.kode_status=1) JOIN ref_barang b ON(b.kode_barang=a.kode_barang) JOIN ref_satuan c ON(c.kode_satuan=a.kode_satuan) WHERE b.kode_barang LIKE '%".$kunci."%' OR b.nama_barang LIKE '%".$kunci."%' GROUP BY a.kode_barang,a.kode_satuan) a LEFT JOIN tabel_penjualan d ON(d.kode_barang=a.kode_barang AND d.kode_satuan=a.kode_satuan AND d.kode_do='".$kode_do."' AND d.kode_status=3) GROUP BY a.kode_barang,a.kode_satuan ORDER BY a.nama_barang LIMIT $limit_awal,$jml_perpage";
				$sth 		= $link->prepare($que);
				$sth->execute();
				$data		= $sth->fetchAll(PDO::FETCH_ASSOC);
				$link 		= null;

				/*	menentukan keberadaan operasi next page	*/
				if(count($data)>=$jml_perpage){
					$next_mess	= "<button class=\"btn\" onClick=\"buka('next_page')\">Next</button>";
				}
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess('Gagal melakukan inquiri data barang');
				$log->logDB($que);
				$erno = true;
			}
			
			if(strlen($kunci)==0){
				$placeholder = "placeholder=\"Kode atau Nama Barang\"";
			}
			else{
				$placeholder = "value=\"".$kunci."\"";
			}
?>
<?php if(strlen($kunci)>0){ ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="kunci" 		value="<?php echo $kunci; 		?>" />
<?php } ?>
<input type="hidden" class="next_page pref_page next_cari" 	name="targetId"		value="<?php echo $targetId;	?>" />
<input type="hidden" class="next_page pref_page next_cari" 	name="targetUrl" 	value="modul/030201_form.php"		/>
<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $next_page;	?>" />
<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $pref_page; 	?>" />
<input type="hidden" class="next_cari" 						name="pg" 			value="1" 							/>
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<div class="form-search">
			<input type="text" class="input-medium search-query span5 next_cari" name="rinci" <?php echo $placeholder; ?> onchange="buka('next_cari')" onmouseover="$(this).select()" />
		</div>
	</div>
	<div class="modal-body">
		<table class="table table-striped">
		  <thead>
			<tr>
				<th>Nama Barang</th>
				<th>Satuan</th>
				<th><div class="text-center">Harga</div></th>
				<th>Stok</th>
			</tr>
		  </thead>
		  <tbody>
<?php
			if(count($data)<=$jml_perpage){
				$jml_perpage=count($data);
			}
			for($i=0;$i<$jml_perpage;$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
				$stok 	= $stok-$draft;
				$randId	= getToken(mt_rand(1,9999));
?>
			<tr>
				<td><?php echo $nama_barang;	?></td>
				<td><?php echo $nama_satuan;	?></td>				
				<td><div class="text-right"><?php echo number_format($harga_dasar);?></div></td>
				<td>
					<div class="text-right">
						<a class="badge badge-success" <?php if($stok>0) echo "onclick=\"buka('".$randId."')\""; ?> >
							<?php echo number_format($stok);?>
						</a>
					</div>
					<input type="hidden" class="<?php echo $randId; ?>" name="targetUrl"	value="modul/030201_form.php" 			/>
					<input type="hidden" class="<?php echo $randId; ?>" name="targetId"		value="<?php echo $targetId;		?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="proses"		value="setBarang" 						/>
					<input type="hidden" class="<?php echo $randId; ?>" name="kode_barang" 	value="<?php echo $kode_barang; 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="kode_satuan" 	value="<?php echo $kode_satuan; 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="nama_barang"	value="<?php echo $nama_barang;	 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="nama_satuan"	value="<?php echo $nama_satuan;	 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="harga_dasar"	value="<?php echo $harga_dasar;	 	?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="stok"			value="<?php echo $stok;	 		?>" />
					<input type="hidden" class="<?php echo $randId; ?>" name="back"			value="<?php echo $pg;	 			?>" />
					<?php if(strlen($kunci)>0){ ?>
					<input type="hidden" class="<?php echo $randId; ?>" name="kunci"		value="<?php echo $kunci;	 		?>" />
					<?php } ?>
				</td>
			</tr>
<?php
			}
?>
			<tr>
				<td colspan="4">
					<div class="btn-group">
					  <?php echo $pref_mess; ?>
					  <?php echo $next_mess; ?>
					</div>
				</td>
			</tr>
		  </tbody>
		</table>
	</div>
</div>
<?php
	}
?>