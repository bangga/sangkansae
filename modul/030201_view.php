<?php
	if(!$erno) die();
?>
<!-- Show message -->
<input type="hidden" class="showMess" 	name="targetUrl" 	value="modul/030201_form.php"/>
<input type="hidden" class="showMess" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="showMess"	name="proses"		value="showMess"/>
<!-- Form pencarian pelanggan -->
<input type="hidden" class="cariNopel" 	name="targetUrl" 	value="modul/030201_form.php"/>
<input type="hidden" class="cariNopel" 	name="proses" 		value="cariNopel"/>
<!-- Set pelanggan berdasarkan nomer pelanggan -->
<input type="hidden" class="setNopel" 	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="setNopel" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="setNopel" 	name="proses" 		value="setNopel"/>
<!-- Refresh pelanggan -->
<input type="hidden" class="refOrder" 	name="targetUrl" 	value="modul/030201_form.php"/>
<input type="hidden" class="refOrder" 	name="targetId"		value="refOrder"/>
<input type="hidden" class="refOrder"	name="proses"		value="refOrder"/>
<!-- Reset order -->
<input type="hidden" class="resetDO" 	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="resetDO" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="resetDO"	name="proses"		value="deleteDO"/>
<!-- Form pencarian barang -->
<input type="hidden" class="cariBarang"	name="targetUrl" 	value="modul/030201_form.php"/>
<!-- Refresh list barang -->
<input type="hidden" class="refBarang" 	name="targetUrl" 	value="modul/030201_form.php"/>
<input type="hidden" class="refBarang" 	name="targetId"		value="listBarang"/>
<input type="hidden" class="refBarang"	name="proses"		value="listBarang"/>
<!-- Form pembayaran -->
<input type="hidden" class="bayar" 		name="targetUrl" 	value="modul/030201_form.php"/>
<input type="hidden" class="bayar" 		name="proses" 		value="formBayar"/>
<!-- Transaksi pembayaran -->
<input type="hidden" class="setBayar"	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="setBayar" 	name="targetId"		value="targetBayarId"/>
<input type="hidden" class="setBayar"	name="proses" 		value="setBayar"/>
<!-- Cetak resi -->
<input type="hidden" class="cetak" 		name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="cetak" 		name="targetId"		value="targetMessId"/>
<input type="hidden" class="cetak"		name="proses"		value="cetakResi"/>

<h4 class="muted"><?php echo _NAME; ?></h4>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder">
	<div class="span6">
		<div class="form-horizontal">
			<?php if(_KODE=='030202'){ ?>
			<div class="control-group">
				<label class="control-label" for="inputPelanggan">Pelanggan</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" id="inputPelanggan" class="span6 cariNopel setNopel" name="kunci" placeholder="Nama atau Kode" onmouseover="$('#'+this.id).select()" />
						<button class="btn" onclick="buka('setNopel')">Set</button>
						<button class="btn" onclick="nonghol('cariNopel')">Cari</button>
					</div>
				</div>
			</div>
			<?php } else{ ?>
			<?php $proses 	= "setNopel"; 	?>
			<?php $kunci 	= "000000"; 	?>
			<?php require _PROC;			?>
			<div class="control-group">
				<label class="control-label" for="inputBarang">Barang</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" id="inputBarang" class="span8" placeholder="Nama atau Kode" onmouseover="$('#'+this.id).select()" />
						<button class="btn" onclick="nonghol('cariBarang')">Cari</button>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<div class="row-fluid" id="listBarang"></div>