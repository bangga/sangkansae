<?php
	switch($proses){
		case "showMess":
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $pesan; ?>
		</div>
	</div>
<?php
			break;
		default:
			try{
				$que 	= "select concat(repeat(_utf8'0',(6 - length(`b`.`kode_barang`))),`b`.`kode_barang`) AS `kode_barang`,`b`.`nama_barang`,`b`.`jenis_barang`,`c`.`nama_satuan`,max(`a`.`harga`) AS `harga_perolehan`,`d`.`harga_dasar` AS `harga_penjualan`,sum(`a`.`jumlah_stok`) AS `jumlah_persediaan`,(`a`.`harga` * `a`.`jumlah_stok`) AS `total_pokok`,(`d`.`harga_dasar` * `a`.`jumlah_stok`) AS `total_aset` from (((`tabel_pembelian` `a` join `ref_harga` `d` on(((`d`.`kode_barang` = `a`.`kode_barang`) and (`d`.`kode_satuan` = `a`.`kode_satuan`)))) join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) group by `a`.`kode_barang`,`a`.`kode_satuan` order by if(`a`.`jumlah_stok`>0,1,0) desc,`b`.`nama_barang`";
				$sth 	= $link->prepare($que);
				$sth->execute();
				$data	= $sth->fetchAll(PDO::FETCH_ASSOC);
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data persediaan");
				$log->logDB($que);
			}
?>
<!-- Show message -->
<input type="hidden" class="showMess" 	name="targetUrl" 	value="<?php echo _PROC; 	?>"/>
<input type="hidden" class="showMess" 	name="targetId"		value="targetMessId"/>
<input type="hidden" class="showMess"	name="proses"		value="showMess"/>
<!-- Validasi stok -->
<input type="hidden" class="validasi" 	name="targetUrl" 	value="<?php echo _PROC; ?>"/>
<input type="hidden" class="validasi" 	name="targetId"		value="refOrder"/>
<input type="hidden" class="validasi"	name="proses"		value="validasi"/>
<!-- Cetak stok -->
<input type="hidden" class="cetak" 		name="targetUrl" 	value="<?php echo _PROC; 	?>"/>
<input type="hidden" class="cetak" 		name="targetId"		value="refOrder"/>
<input type="hidden" class="cetak"		name="proses"		value="cetakin"/>
<input type="hidden" class="cetak" 		name="stringFile" 	value="daftar_persediaan" 	/>

<h4 class="muted cetak"><?php echo _NAME; ?></h4>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder"></div>
<div class="row-fluid" id="listBarang">
	<div class="span12">
		<div class="skrin">Laporan Persediaan per <?php echo $tgl_sekarang; ?></div>
		<table class="table table-bordered">
			<thead>
				<tr class="cetak">
					<th colspan="9" class="btn-group">
						<button class="btn" onclick="buka('cetak')">Cetak</button>
						<button class="btn" onclick="buka('validasi')">Validasi</button>
					</th>
				</tr>
				<tr>
					<th class="prn_head">Kode Barang</th>
					<th class="prn_head">Nama Barang</th>
					<th class="prn_head">Satuan</th>
					<th class="prn_head" width="100px">Jenis</th>
					<th class="prn_head">Harga Perolehan</th>
					<th class="prn_head">Harga Penjualan</th>
					<th class="prn_head">Persediaan</th>
					<th class="prn_head">Total Perolehan</th>
					<th class="prn_head">Total Asset</th>
				</tr>
			</thead>
			<tbody>
<?php
			// line untuk ff continous paper
			$stringCetak  = chr(27).chr(67).chr(1);
			// enable paper out sensor
			$stringCetak .= chr(27).chr(57);
			// draft mode
			$stringCetak .= chr(27).chr(120).chr(48);
			// line spacing x/72
			$stringCetak .= chr(27).chr(65).chr(12);
			$stringCetak .= str_repeat(" ",59)."Daftar Persediaan".chr(10);
			$stringCetak .= str_repeat("_",119).chr(10);
			$stringCetak .= printRight("No. ",6).printLeft("Kode",6).printLeft(" Nama",36).printLeft("Sat.",7).printRight("Stok",5).printRight("H. Perolehan",14).printRight("H. Penjualan",15).printRight("T. Perolehan",15).printRight("T. Penjualan",15).chr(10);
			$stringCetak .= str_repeat("-",119).chr(10);
			
			for($i=0;$i<count($data);$i++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$nilai	= $data[$i];
				if(count($nilai)>0){
					$konci	= array_keys($nilai);
				}
				for($b=0;$b<count($konci);$b++){
					$$konci[$b]	= $nilai[$konci[$b]];
				}
				/* getParam **/
				$stringCetak .= printRight(($i+1)." ",6).printLeft($kode_barang,6).printLeft(" ".$nama_barang,36).printLeft($nama_satuan,7).printRight($jumlah_persediaan,5).printRight(number_format($harga_perolehan)." ",14).printRight(number_format($harga_penjualan)." ",15).printRight(number_format($total_pokok)." ",15).printRight(number_format($total_aset),15).chr(10);
				$grand_pokok[]	= $total_pokok;
				$grand_asset[]	= $total_aset;
?>
				<tr>
					<td class="right prn_cell"><?php echo $kode_barang; 	?></td>
					<td class="right prn_left prn_cell"><?php echo $nama_barang;	?></td>
					<td class="right prn_left prn_cell"><?php echo $nama_satuan;	?></td>
					<td class="right prn_left prn_cell"><span class="cetak"><?php echo $jenis_barang;	?></span></td>
					<td class="right prn_cell"><div class="text-right"><?php echo number_format($harga_perolehan); 		?></div></td>
					<td class="right prn_cell"><div class="text-right"><?php echo number_format($harga_penjualan);		?></div></td>
					<td class="right prn_cell"><div class="text-right"><?php echo number_format($jumlah_persediaan); 	?></div></td>
					<td class="right prn_cell"><div class="text-right"><?php echo number_format($total_pokok);		 	?></div></td>
					<td class="right prn_cell"><div class="text-right"><?php echo number_format($total_aset); 			?></div></td>
				</tr>
<?php
			}
			$stringCetak .= str_repeat("-",119).chr(10);
			$stringCetak .= str_repeat(" ",89).printRight(number_format(array_sum($grand_pokok))." ",15).printRight(number_format(array_sum($grand_asset)),15).chr(10);
			$stringCetak .= chr(12);
?>
			</tbody>
			<thead>
				<tr>
					<th class="prn_cell" colspan="7">
						<input type="hidden" class="cetak" 	name="stringCetak" 	value="<?php echo base64_encode($stringCetak); 	?>" />
					</th>
					<th class="prn_cell"><div class="text-right"><?php echo number_format(array_sum($grand_pokok)); 	?></div></th>
					<th class="prn_cell"><div class="text-right"><?php echo number_format(array_sum($grand_asset)); 	?></div></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<?php
	}
?>