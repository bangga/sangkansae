<?php
	if(!$erno) die();
	switch($proses){
		case "formBayar":
			try{
				$que	= "SELECT CONCAT(REPEAT('0',6-LENGTH(d.kode_pelanggan)),d.kode_pelanggan) AS kode_pelanggan,d.nama,d.alamat,DATE_FORMAT(getTanggal(a.kode_po),'%d-%m-%Y') AS tgl_transaksi,encodeResi(a.kode_po) AS reff,a.kode_po,IFNULL(c.byr_sts,0) AS byr_cetak,SUM(b.harga*b.jumlah_pembelian) AS total_transaksi,IFNULL(c.byr_total,0) AS total_dibayar,a.kode_bayar,IFNULL(DATE_FORMAT(c.byr_tgl,'%d-%m-%Y'),'-') AS remark_tgl FROM tabel_po a JOIN tabel_pembelian b ON(b.kode_po=a.kode_po) JOIN tabel_pelanggan d ON(d.kode_pelanggan=a.kode_vendor) LEFT JOIN (SELECT `a`.`kode_do` AS `kode_po`,CAST(MAX(`a`.`byr_tgl`) AS DATE) AS `byr_tgl`,SUM(`a`.`byr_total`) AS `byr_total`,SUM(`a`.`byr_sts`) AS byr_sts FROM `tabel_pembayaran` `a` JOIN `tabel_po` `b` ON(`b`.`kode_po`=`a`.`kode_do`) WHERE `b`.`kode_vendor`=ABS('".$kunci."') OR b.kode_po=decodeResi('".$kunci."') GROUP BY `b`.`kode_po`) c ON(c.kode_po=a.kode_po) WHERE a.kode_bayar<2 AND (a.kode_vendor=ABS('".$kunci."') OR a.kode_po=decodeResi('".$kunci."')) GROUP BY a.kode_po ORDER BY a.kode_po LIMIT 1";
				$data	= $link->query($que)->fetch();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data tagihan");
				$log->logDB($que);
			}
			
			if($data){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$konci	= array_keys($data);
				for($i=0;$i<count($konci);$i++){
					$$konci[$i]	= $data[$konci[$i]];
				}
				/* getParam **/
				
				$sisa_tagihan	= $total_transaksi - $total_dibayar;
				$byr_cetak++;
				
				if($sisa_tagihan==0){
					$erno  	= false;
					$kelas	= "alert-info";
					$pesan	= "<strong>Info :</strong> Tagihan dengan kode $reff telah dilunasi pada $remark_tgl";
				}
				else{
					$erno = true;
				}
			}
			else{
				$erno 	= false;
				$kelas	= "alert-error";
				$pesan	= "<strong>Info :</strong> Tagihan tidak ditemukan";
			}
?>
<script>
	function setKembali(opt){
		var inputTotal 		= $('#inputTotal').attr('value');
		if(opt>=parseInt(inputTotal)){
			var inputKembali = opt - parseInt(inputTotal);
		}
		else{
			var inputKembali = 0;
		}
		if(opt>0){
			$('#inputKembali').attr('value',$.formatNumber(inputKembali, {format:'#,###', locale:'us'}));
			$('#submitDraft').prop('disabled', false);
			$('#submitDraft').focus();
		}
		else{
			$('#submitDraft').prop('disabled', true);
			$('#inputKembali').attr('value',0);
		}
	}
</script>
<input id="inputTotal" 	type="hidden" class="setBayar" name="sisa_tagihan" 	value="<?php echo $sisa_tagihan; 	?>" />
<input 					type="hidden" class="setBayar" name="tutupId" 		value="<?php echo $targetId; 		?>" />
<input 					type="hidden" class="setBayar" name="kode_po" 		value="<?php echo $kode_po;			?>" />
<input 					type="hidden" class="setBayar" name="byr_cetak"		value="<?php echo $byr_cetak;		?>" />
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4 class="muted">Form Pembayaran</h4>
	</div>
	<div class="modal-body" id="targetBayarId">
		<?php if($erno){ ?>
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputTagihan">Kode Transaksi</label>
				<label class="control-label text-left" for="inputTagihan"><?php echo $reff; ?></label>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputTagihan">Total Transaksi</label>
				<div class="controls">
					<input type="text" class="input-medium text-right" value="<?php echo number_format($total_transaksi); ?>" disabled />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="sisaTagihan">Sisa Tagihan</label>
				<div class="controls">
					<input type="text" class="input-medium text-right" value="<?php echo number_format($sisa_tagihan); ?>" disabled />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputDibayar">Dibayar</label>
				<div class="controls">
					<input type="text" class="input-medium text-right setBayar" name="do_dibayar" placeholder="<?php echo number_format($sisa_tagihan); ?>" onchange="setKembali(this.value)" onmouseover="$(this).select()" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="Kembali">Kembali</label>
				<div class="controls">
					<input type="text" id="inputKembali" class="input-medium text-right" placeholder="0" disabled />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputSubmit"></label>
				<div class="controls">
					<div class="btn-group">
						<button id="submitDraft" class="btn" onclick="buka('setBayar')" disabled >Simpan</button>
						<button class="btn" onclick="tutup('<?php echo $targetId; ?>')">Kembali</button>
					</div>
				</div>
			</div>
		</div>
		<?php } else{ ?>
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
		<center><button class="btn" onclick="tutup('<?php echo $targetId; ?>')">Kembali</button></center>
		<?php } ?>
	</div>
</div>
<?php
			break;
		default:
?>
<!-- Proses bayar -->
<input 	type="hidden" class="setBayar" name="proses" 	value="setBayar" 			 />
<input 	type="hidden" class="setBayar" name="targetId" 	value="targetBayarId" 		 />
<input 	type="hidden" class="setBayar" name="targetUrl" value="<?php echo _PROC; ?>" />
<!-- Form pencarian pelanggan -->
<input type="hidden" class="formBayar" name="targetUrl" value="<?php echo _FILE; ?>" />
<input type="hidden" class="formBayar" name="proses" 	value="formBayar" />
<h4 class="muted"><?php echo _NAME; ?></h4>
<div class="row-fluid" id="targetMessId"></div>
<div class="row-fluid" id="refOrder">
	<div class="span6">
		<div class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="inputPelanggan">Kode Pelanggan</label>
				<div class="controls">
					<div class="input-append">
						<input type="text" class="span7 formBayar" name="kunci" placeholder="Kode Referensi" onmouseover="$(this).select()">
						<button class="btn" onclick="nonghol('formBayar')">Periksa</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row-fluid" id="listBarang"></div>
<?php
	}
?>