<?php
	if(!$erno) die();
	switch($proses){
		case "cetakResi":
			try{
				$wsdl_url	= "http://"._PRIN."/printClient/printServer.wsdl";
				$client   	= new SoapClient($wsdl_url, array('cache_wsdl' => WSDL_CACHE_NONE) );
				$stringFile	= $stringFile.".txt";
				$client->cetak($stringCetak,$stringFile);
				$pesan		= "Resi telah berhasil dicetak";
				$kelas		= "alert-success";
			}
			catch (Exception $e){
				$pesan 		= $e->getMessage();
				$kelas		= "alert-error";
			}
?>
<div class="span12">
	<div class="alert <?php echo $kelas; ?>">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $pesan; ?>
	</div>
</div>
<?php
			break;
		case "setBayar":
			$kode_do	= $_SESSION['kode_do'];
			try {
				$link->beginTransaction();
				// Tambahkan informasi pembayaran jika ada pelunasan atau uang muka disetor
				if($do_dibayar>=$grand_total){
					$pesan			= "Transaksi pelunasan telah dilakukan";
					$kode_bayar		= 2;
					$jatuh_tempo	= 0;
					$uang_muka		= $grand_total;
					$que			= "INSERT INTO tabel_pembayaran(byr_no,byr_tgl,kode_do,kar_id,lok_ip,byr_loket,byr_total,byr_kembali,byr_cetak,byr_upd_sts,byr_sts) VALUES("._TOKN.",NOW(),$kode_do,'"._USER."','"._HOST."','P',$grand_total,($do_dibayar-$grand_total),1,NOW(),1)";
					$res2 			= $link->exec($que);
					$log->logDB($que);
					$log->logMess($pesan);
					// Proses validasi dan pelunasan, kode_status diset dari 3 ke 0 dan kode_bayar diset dari 0 ke 2
					$que	= "UPDATE tabel_pengiriman SET tanggal_drop=NOW(),tanggal_bayar=NOW(),kode_status=1,kode_bayar=$kode_bayar,jatuh_tempo=0,kar_id='"._USER."' WHERE kode_do='$kode_do' AND kode_status=3";
				}
				else if($do_dibayar>0){
					$pesan			= "Transaksi pembayaran uang muka telah dilakukan";
					$kode_bayar		= 1;
					$uang_muka		= $do_dibayar;
					$que			= "INSERT INTO tabel_pembayaran(byr_no,byr_tgl,kode_do,kar_id,lok_ip,byr_loket,byr_total,byr_kembali,byr_cetak,byr_upd_sts,byr_sts) VALUES("._TOKN.",NOW(),$kode_do,'"._USER."','"._HOST."','P',$do_dibayar,0,1,NOW(),1)";
					$res2 			= $link->exec($que);
					$log->logDB($que);
					$log->logMess($pesan);
					// Proses validasi dan pembayaran uang muka, kode_status diset dari 3 ke 1 dan kode_bayar diset dari 0 ke 1
					$que	= "UPDATE tabel_pengiriman SET tanggal_drop=NOW(),tanggal_bayar=NOW(),kode_status=1,kode_bayar=$kode_bayar,jatuh_tempo=$jatuh_tempo,kar_id='"._USER."' WHERE kode_do='$kode_do' AND kode_status=3";
				}
				else{
					$pesan			= "Transaksi pembayaran tanpa uang muka";
					$res2			= 1;
					$kode_bayar		= 0;
					$uang_muka		= 0;
					$log->logMess($pesan);
					// Hanya proses validasi, kode_status diset dari 3 ke 1
					$que	= "UPDATE tabel_pengiriman SET tanggal_drop=NOW(),kode_status=1,kode_bayar=$kode_bayar,jatuh_tempo=$jatuh_tempo,kar_id='"._USER."' WHERE kode_do='$kode_do' AND kode_status=3";
				}
				
				// Set kode_status dari 3 ke 1 untuk menandakan bahwa order telah divalidasi
				$res1 	= $link->exec($que);
				$log->logDB($que);
				
				if(($res1*$res2)>0){
					$link->commit();
					$pesan 					= "Validasi transaksi pembayaran telah dilakukan";
					$kelas 					= "alert-success";
					?><input type="hidden" class="refBarang" name="jatuh_tempo" value="<?php echo $jatuh_tempo; 	?>" /><?php
					?><input type="hidden" class="refBarang" name="uang_muka" 	value="<?php echo $uang_muka; 		?>" /><?php
					?><input type="hidden" class="refBarang" name="kode_do" 	value="<?php echo $kode_do; 		?>" /><?php
					// Masih ada error pada request refBarang saat tidak ada pelunasan
					?><script>buka('refBarang');</script><?php
					?><script>buka('setNopel');</script><?php
					$kode_do				= getToken('2000');
					$nomer_do				= '000000';
					$_SESSION['appl_tokn']	= $kode_do;
					$_SESSION['kode_do']	= $kode_do;
					$_SESSION['nomer_do']	= $nomer_do;
				}
				else{
					$link->rollBack();
					$pesan = "Validasi transaksi pembayaran tidak dapat dilakukan";
					$kelas = "alert-notice";
				}
				$log->logMess($pesan);
			}
			catch (Exception $e){
				$pesan = "Gagal melakukan proses transaksi pembayaran";
				$kelas = "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
<div class="span11">
	<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
	<p class="text-center"><button class="btn" onclick="tutup('<?php echo $tutupId; ?>')">Tutup</button></p>
</div>
<?php
			break;
		case "deleteDO":
			$kode_do	= $_SESSION['kode_do'];
			$nama		= $_SESSION['nama'];
			$erno		= false;
			try {
				$link->beginTransaction();
				$que		= "DELETE FROM tabel_pengiriman WHERE kode_do='$kode_do' AND kode_status=3 AND kode_bayar=0";
				$res 		= $link->exec($que);
				if($res>0){
					$pesan 	= "Drop order $nama telah di hapus";
					$kelas	= "alert-success";
					unset($_SESSION['kode_do']);
					unset($_SESSION['nomer_do']);
					unset($_SESSION['appl_tokn']);
					unset($_SESSION['nama']);
					unset($_SESSION['kode_pelanggan']);
					unset($_SESSION['alamat']);
					unset($_SESSION['kota']);
					unset($_SESSION['badge']);
					?><script>buka('<?php echo _KODE; ?>')</script><?php
				}
				else{
					$pesan 	= "<strong>Notice!</strong> drop order $nama tidak bisa dihapus";
					$kelas	= "alert-notice";
					$erno	= true;
				}
				$log->logDB($que);
				$log->logMess($pesan);
				$link->commit();
			}
			catch (Exception $e){
				$pesan	= "<strong>Error!</strong> Proses delete drop order gagal dilakukan";
				$kelas	= "alert-error";
				$erno	= true;
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
			
			if($erno){
				?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; ?>" /><?php
				?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; ?>" /><?php
				?><script>buka('showMess')</script><?php
			}
			break;
		case "setNopel":
			if(!isset($nama)){
				try {
					if(isset($kode_pelanggan)){
						$kunci = $kode_pelanggan;
					}
					$que	= "SELECT IFNULL(a.kode_do,1) AS kode_do,IFNULL(a.nomer_do,'-') AS nomer_do,b.kode_pelanggan,b.nama,b.alamat,b.kota,'0.5' AS badge FROM tabel_pengiriman a RIGHT JOIN tabel_pelanggan b ON(b.kode_pelanggan=a.kode_pelanggan AND a.kode_status=3) WHERE b.kode_pelanggan='".$kunci."' ORDER BY IFNULL(a.kode_do,0) LIMIT 1";
					$res 	= $link->prepare($que);
					$res->execute();
					$data	= $res->fetch(PDO::FETCH_ASSOC);
					if($res->rowCount()>0){
						/** getParam 
							memindahkan semua nilai dalam array POST ke dalam
							variabel yang bersesuaian dengan masih kunci array
						*/
						$konci	= array_keys($data);
						for($i=0;$i<count($konci);$i++){
							$$konci[$i]	= $data[$konci[$i]];
						}
						/* getParam **/
						$erno	= true;
					}
					else{
						$pesan 	= "<strong>Notice!</strong> Data Pelanggan tidak ditemukan";
						$kelas	= "alert-notice";
						$erno	= false;
					}
				}
				catch(Exception $e){
					$pesan 	= "<strong>Error!</strong> Gagal melakukan inquiri data pelanggan";
					$kelas	= "alert-error";
					$log->errorDB($e->getMessage());
					$log->logMess($pesan);
					$log->logDB($que);
				}
			}
			
			// Set parameter pelanggan ke session
			if($erno){
				$_SESSION['kode_pelanggan']	= $kode_pelanggan;
				$_SESSION['nama']			= $nama;
				$_SESSION['alamat']			= $alamat;
				$_SESSION['kota']			= $kota;
				$_SESSION['badge']			= $badge;
			}
			
			if($erno and $kode_do>1){
				$_SESSION['kode_do'] 	= $kode_do;
				$_SESSION['nomer_do']	= $nomer_do;
				?><script>buka('refOrder');</script><?php
				?><script>buka('refBarang');</script><?php
			}
			else if($erno){
				$kode_do 	= $_SESSION['kode_do'];
				$nomer_do 	= $_SESSION['nomer_do'];
				try {
					$link->beginTransaction();
					$que 	= "INSERT INTO tabel_pengiriman(kode_do,tanggal_do,nomer_do,nomer_kontrak,kode_pelanggan,kar_id,keterangan) VALUES('$kode_do',CURDATE(),'$nomer_do','-','$kode_pelanggan','"._USER."','AUTO')";
					$pesan	= "Drop order $nama telah di simpan";
					$link->exec($que);
					$log->logDB($que);
					$log->logMess($pesan);
					$link->commit();
					?><script>buka('refOrder');</script><?php
				}
				catch (Exception $e){
					$pesan 	= "<strong>Error!</strong> Gagal melakukan input drop order";
					$kelas	= "alert-error";
					$link->rollBack();
					$log->errorDB($e->getMessage());
					$log->logMess($pesan);
					$log->logDB($que);
					?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; 	?>"/><?php
					?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; 	?>"/><?php
					?><script>buka('showMess');</script><?php
				}
			}
			else{
				?><input type="hidden" class="showMess" name="pesan" value="<?php echo $pesan; 	?>"/><?php
				?><input type="hidden" class="showMess" name="kelas" value="<?php echo $kelas; 	?>"/><?php
				?><script>buka('showMess');</script><?php
			}
			if(isset($tutupId)){
				?><script>tutup('<?php echo $tutupId; ?>');</script><?php
			}
			break;
		case "deleteDraft":
			try {
				$link->beginTransaction();
				$que = "DELETE FROM tabel_penjualan WHERE kode_penjualan=$kode_penjualan AND kode_status=3";
				$res = $link->exec($que);
				$log->logDB($que);
				$link->commit();
				if($res>0){
					$pesan = "Rincian $jumlah_penjualan $nama_satuan $nama_barang telah dihapus";
				}
				else{
					$pesan = "Rincian drop order tidak bisa dihapus";
				}
			}
			catch (Exception $e){
				$pesan = "Gagal melakukan proses hapus rincian drop order";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
			}
			$log->logMess($pesan);
?>
			<button class="btn btn-danger">Hapus</button>
			<script>buka('refBarang');</script>
<?php
			break;
		case "tambahDO":
			$kode_do	= $_SESSION['kode_do'];
			$nama		= $_SESSION['nama'];
			try {
				$link->beginTransaction();
				$que 		= "SELECT kode_do FROM tabel_pengiriman WHERE kode_do='$kode_do' AND kode_status=1";
				$res 		= $link->query($que);
				if($res->fetchColumn()>0){
					$pesan 	= "<strong>Notice!</strong> Order telah divalidasi";
					$kelas	= "alert-notice";
				}
				else{
					$pesan 	= "<strong>Success!</strong> $jumlah_penjualan $nama_satuan $nama_barang telah ditambahkan";
					$kelas	= "alert-success";
					$que 	= "INSERT INTO tabel_penjualan(kode_do,kode_barang,kode_satuan,jumlah_penjualan,harga_penjualan,harga_diskon,diskon,kode_status,kar_id,keterangan) VALUES('$kode_do','$kode_barang',$kode_satuan,$jumlah_penjualan,$harga_jual,$harga_diskon,$diskon,3,'"._USER."','AUTO')";
					$res 	= $link->exec($que);
					$log->logDB($que);
					$log->logMess($pesan);
					$link->commit();
				}
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan penambahan drop order";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
		<p class="text-center"><button class="btn" onclick="buka('<?php echo $backId; ?>')">Kembali</button></p>
		<script>buka('refBarang');</script>
	</div>
<?php
			break;
		default :
			$log->logMess("Tidak ada proses yang terdefinisi");
	}
?>