<?php
	if(!$erno) die();
?>
<h4 class="muted"><?php echo _NAME; ?></h4>
<div class="row">
<?php
	try{
		$jml_perpage	= 15;
		$link 			= new PDO('sqlite:./_data/logging.db');
		$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$que			= "SELECT * FROM logMess ORDER BY tanggal DESC LIMIT $limit_awal,$jml_perpage";
		$res 			= $link->prepare($que);
		$res->execute();
		$data			= $res->fetchAll(PDO::FETCH_ASSOC);
		/*	menentukan keberadaan operasi next page	*/
		if(count($data)>=$jml_perpage){
			$next_mess	= "<button class=\"btn\" onClick=\"buka('next_page')\">Next</button>";
		}
	}
	catch(Exception $e){
		echo $e->getMessage();
	}
	
	for($i=0;$i<count($data);$i++){
		/** getParam 
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		$nilai	= $data[$i];
		if(count($nilai)>0){
			$konci	= array_keys($nilai);
		}
		for($b=0;$b<count($konci);$b++){
			$$konci[$b]	= $nilai[$konci[$b]];
		}
		/* getParam **/
		if($pg>1){
			$nomer = ($i+1) + ($pg-1)*$jml_perpage;
		}
		else{
			$nomer = ($i+1);
		}
?>
	<div class="span3">
		<button type="button" class="close"><small><?php echo $nomer ?></small></button>
		<div class="alert alert-info"><?php echo $keterangan; ?></div>
	</div>
<?php
	}
?>
</div>
<div class="row">
	<input type="hidden" class="next_page pref_page next_cari" 	name="targetId"		value="content" />
	<input type="hidden" class="next_page pref_page next_cari" 	name="jml_perpage" 	value="<?php echo $jml_perpage;	?>"	/>
	<input type="hidden" class="next_page pref_page next_cari" 	name="targetUrl" 	value="<?php echo _FILE;		?>"	/>
	<input type="hidden" class="next_page" 						name="pg" 			value="<?php echo $next_page;	?>" />
	<input type="hidden" class="pref_page" 						name="pg" 			value="<?php echo $pref_page; 	?>" />
	<div class="span2 offset10 text-right">
		<div class="btn-group"><?php echo $pref_mess; ?><?php echo $next_mess; ?></div>
	</div>
</div>