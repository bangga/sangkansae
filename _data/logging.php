<?php
	class errorLog{
		public function __construct(){
			$this->db = new PDO('sqlite:./_data/logging.db');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		public function errorDB($mess){
			$que	= "INSERT INTO errorDB VALUES(strftime(\"%s\",\"now\"),"._TOKN.",\""._KODE."\",\""._USER."\",\""._HOST."\",\"".$mess."\")";
			try {
				$sth 	= $this->db->prepare($que);
				$hasil	= $sth->execute();
			}
			catch(Exception $e){
				$hasil = $e->getMessage();
			}
			return $hasil;
		}
		public function logDB($mess){
			$que	= "INSERT INTO logDB VALUES(strftime(\"%s\",\"now\"),"._TOKN.",\""._KODE."\",\""._USER."\",\""._HOST."\",\"".$mess."\")";
			try {
				$sth 	= $this->db->prepare($que);
				$hasil	= $sth->execute();
			}
			catch(Exception $e){
				$hasil = $e->getMessage();
			}
			return $hasil;
		}
		public function logMess($mess){
			$que	= "INSERT INTO logMess VALUES(strftime(\"%s\",\"now\"),"._TOKN.",\""._KODE."\",\""._USER."\",\""._HOST."\",\"".$mess."\")";
			try {
				$sth 	= $this->db->prepare($que);
				$hasil	= $sth->execute();
			}
			catch(Exception $e){
				$hasil = $e->getMessage();
			}
			return $hasil;
		}
	}
?>