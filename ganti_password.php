<?php
	if(!$erno) die();
	if(isset($password_lama)){
		if($password_baru == $validasi_pass){
			try {
				$link->beginTransaction();
				$que	= "UPDATE tabel_user SET kar_pass='".md5($validasi_pass)."' WHERE kar_id='"._USER."' AND kar_pass='".md5($password_lama)."'";
				$res 	= $link->exec($que);
				if($res>0){
					$pesan	= "<strong>Success!</strong> Perubahan password berhasil dilakukan";
					$kelas	= "alert-success";
					$log->logDB($que);
					$log->logMess($pesan);
					$link->commit();
				}
				else{
					$pesan	= "<strong>Notice!</strong> Perubahan password tidak dapat dilakukan";
					$kelas	= "alert-notice";
				}
			}
			catch (Exception $e){
				$pesan 	= "<strong>Error!</strong> Gagal melakukan perubahan passowrd";
				$kelas	= "alert-error";
				$link->rollBack();
				$log->errorDB($e->getMessage());
				$log->logDB($que);
				$log->logMess($pesan);
			}
		}
		else{
			$pesan	= "<strong>Notice!</strong> Validasi password tidak bersesuaian";
			$kelas	= "alert-notice";
		}
?>
	<div class="span12">
		<div class="alert <?php echo $kelas; ?>"><?php echo $pesan; ?></div>
		<p class="text-center"><button class="btn" onclick="buka('<?php echo _KODE; ?>')">Kembali</button></p>
	</div>
<?php
	}
	else{
		$procID = getToken(mt_rand(1,9999));
?>
<h4 class="muted"><?php echo _NAME; ?></h4>
<div id="<?php echo $procID; ?>" class="row">
	<input type="hidden" class="simpan" name="targetUrl" 	value="<?php echo _FILE; 	?>" />
	<input type="hidden" class="simpan" name="targetId" 	value="<?php echo $procID;	?>" />
	<div class="form-horizontal">
		<div class="control-group">
			<label class="control-label">Password Lama</label>
			<div class="controls">
				<input type="password" class="simpan" name="password_lama" placeholder="Password Lama" onmouseover="$(this).select()" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Password Baru</label>
			<div class="controls">
				<input type="password" class="simpan" name="password_baru" placeholder="Password Baru" onmouseover="$(this).select()" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Validasi Password</label>
			<div class="controls">
				<input type="password" class="simpan" name="validasi_pass" placeholder="Validasi Password" onmouseover="$(this).select()" />
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<button class="btn" onclick="buka('simpan')">Simpan</button>
			</div>
		</div>
	</div>
</div>
<?php } ?>