<?php include "lib.php"; ?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $appl_nama; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
		<link href="css/datepicker.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" type="text/css" href="css/mainindex_screen.css" media="screen"/>
		<link rel="stylesheet" type="text/css" href="css/mainindex_print.css" media="print"/>
		<style id="holderjs-style" type="text/css">
			.holderjs-fluid {font-size:16px;font-weight:bold;text-align:center;font-family:sans-serif;margin:0}
		</style>
	</head>
<body>
    <div class="container">
		<div class="masthead cetak">
			<h4 class="muted text-right"><?php echo $appl_owner; ?><br/><small><?php echo $add0_owner; ?><br/><?php echo $add1_owner; ?></small></h4>
			<div class="navbar">
				<div class="navbar-inner">
					<div class="container">
						<ul class="nav row-fluid" id="targetMenu">
							<?php session_start();				 	?>
							<?php if(isset($_SESSION['User_c'])){ 	?>
							<li onmouseover="jQuery(this).addClass('active');" onmouseout="jQuery(this).removeClass('active');">
								<a href="./logout.php">Logout</a>
							</li>
							<?php require "getMenu.php"; 			?>
							<?php } else{							?>
							<li onmouseover="jQuery(this).addClass('active');" onmouseout="jQuery(this).removeClass('active');">
								<a href="./">Home</a>
							</li>
							<div class="navbar-form pull-right">
								<input type="hidden" 	class="login" 		name="targetId" 	value="targetMenu" 	/>
								<input type="hidden" 	class="login" 		name="targetUrl" 	value="login.php" 	/>
								<input type="hidden" 	class="login" 		name="login" 		value="1" 			/>
								<input id="inputID" 	class="login span5" name="kar_nama"		type="text" 	placeholder="User Name" onmouseover="$(this).select()" />
								<input 					class="login span5" name="kar_pass"		type="password" placeholder="Password" 	onmouseover="$(this).select()" />
								<button class="btn" onclick="buka('login')">Masuk</button>
							</div>
							<?php }									?>
						</ul>
					</div>
				</div>
			</div>
			<!-- /.navbar -->
		</div>

		<div id="content" style="height: 475px">
			<!-- Show message -->
			<input type="hidden" class="showMess" 	name="targetUrl" 	value="pesan_error.php"/>
			<input type="hidden" class="showMess" 	name="targetId"		value="targetMessId"/>
			<div class="row-fluid" id="targetMessId"></div>
		</div>
		<hr class="cetak">

		<div class="footer cetak">
			<p>&copy; Prototype 2013</p>
		</div>
    </div>
	<!-- /container -->
	<!-- Javascript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/kontrol.js"></script>
	<script src="js/jshashtable-2.1.js"></script>
	<script src="js/jquery.numberformatter-1.2.3.js"></script>
	<script src="js/holder.js"></script>
	<script>
		jQuery(document).ready(function(){
			jQuery('#inputID').focus();
		});
	</script>
</body>
</html>