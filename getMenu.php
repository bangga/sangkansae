<?php
	if(!$login){
		require "model/setDB.php";
		require "_data/logging.php";
		require "fungsi.php";
		cek_pass();
		$waktu		= getdate();
		if($ipClient=='::1'){
			$ipClient = '127.0.0.1';
		}
		$randId = mt_rand(1,9999);
		define("_TOKN",$waktu[0].str_repeat('0',4-strlen($randId)).$randId);
		define("_KODE","000000");
		define("_HOST",$ipClient);
		
		$link 	= new PDO("mysql:host=".$DHOST.";dbname=".$DNAME, $DUSER, $DPASS);
		$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$log 	= new errorLog();
	}
	else{
?>
<li onmouseover="jQuery(this).addClass('active');" onmouseout="jQuery(this).removeClass('active');">
	<a href="./logout.php">Logout</a>
</li>
<?php
	}

	try {
		$que	= "SELECT *FROM v_menu_item WHERE appl_sts=0 AND getMenu('"._GRUP."',appl_kode)>0";
		$res 	= $link->prepare($que);
		$res->execute();
		foreach($res->fetchAll(PDO::FETCH_ASSOC) as $row){
			if($row['l2'] == '00' and $row['l3'] == '00'){
				$l1[$row['appl_kode']]['appl_kode'] = $row['appl_kode'];
				$l1[$row['appl_kode']]['appl_name'] = $row['appl_name'];
			}
			else if($row['l3'] == '00'){
				$l2[$row['parent_id']][$row['appl_kode']]['appl_kode'] = $row['appl_kode'];
				$l2[$row['parent_id']][$row['appl_kode']]['appl_name'] = $row['appl_name'];
			}
			else{
				$l3[$row['parent_id']][$row['appl_kode']]['appl_kode'] = $row['appl_kode'];
				$l3[$row['parent_id']][$row['appl_kode']]['appl_name'] = $row['appl_name'];
				$l3[$row['parent_id']][$row['appl_kode']]['appl_file'] = $row['appl_file'];
				$l3[$row['parent_id']][$row['appl_kode']]['appl_proc'] = $row['appl_proc'];
				$l3[$row['parent_id']][$row['appl_kode']]['appl_desc'] = $row['appl_desc'];
			}
		}
		$link 	= null;
		if($res->rowCount()==0){
			$log->logMess("Informasi menu tidak ditemukan");
			$log->logDB($que);
			$erno	= false;
		}
		else{
			$erno	= true;
		}
	}
	catch(Exception $e){
		$log->errorDB($e->getMessage());
		$log->logMess("Gagal melakukan inquiri data menu");
		$log->logDB($que);
	}

	/* menu level 1 */
	if(count($l1)){
		$k1 = array_keys($l1);
	}
	for($i=0;$i<count($l1);$i++){
		$appl_name = $l1[$k1[$i]]["appl_name"];
		$appl_kode = $l1[$k1[$i]]["appl_kode"];
?>
<li class="dropdown" onmouseover="jQuery(this).addClass('active');" onmouseout="jQuery(this).removeClass('active');">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $appl_name; ?> <b class="caret"></b></a>
	<ul class="dropdown-menu">
<?php
		/* menu level 2 tanpa submenu */
		if(count($l3[$appl_kode])>0){
			$k3 = array_keys($l3[$appl_kode]);
			for($j=0;$j<count($k3);$j++){
				$menu03 = $l3[$appl_kode][$k3[$j]];
				$buka 	= "buka('".$menu03["appl_kode"]."')";
?>
		<li>
			<a onclick="buka('<?php echo $menu03["appl_kode"]; ?>')"><?php echo $menu03["appl_name"]; ?></a>
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="targetId" 	value="content" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="targetUrl" 	value="<?php echo $menu03["appl_file"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_name" 	value="<?php echo $menu03["appl_name"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_kode" 	value="<?php echo $menu03["appl_kode"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_file" 	value="<?php echo $menu03["appl_file"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_proc" 	value="<?php echo $menu03["appl_proc"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_desc" 	value="<?php echo $menu03["appl_desc"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="from_menu" 	value="1" />
		</li>
<?php
			}
		}
		/* menu level 2 dengan submenu */
		if(count($l2[$appl_kode])>0){
			$k2 = array_keys($l2[$appl_kode]);
			for($k=0;$k<count($k2);$k++){
				$menu02 = $l2[$appl_kode][$k2[$k]];
?>
										<li class="divider"></li>
										<li class="nav-header"><?php echo $menu02["appl_name"]; ?></li>
<?php
				/* menu level 3 */
				if(count($l3[$menu02["appl_kode"]])){
					$k3 = array_keys($l3[$menu02["appl_kode"]]);
					for($j=0;$j<count($k3);$j++){
						$menu03 = $l3[$menu02["appl_kode"]][$k3[$j]];
?>
		<li>
			<a onclick="buka('<?php echo $menu03["appl_kode"]; ?>')"><?php echo $menu03["appl_name"]; ?></a>
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="targetId" 	value="content" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="targetUrl" 	value="<?php echo $menu03["appl_file"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_name" 	value="<?php echo $menu03["appl_name"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_kode" 	value="<?php echo $menu03["appl_kode"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_file" 	value="<?php echo $menu03["appl_file"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_proc" 	value="<?php echo $menu03["appl_proc"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="appl_desc" 	value="<?php echo $menu03["appl_desc"]; ?>" />
			<input type="hidden" class="<?php echo $menu03["appl_kode"]; ?>" name="from_menu" 	value="1" />
		</li>
<?php
					}
				}
			}
		}
?>
	</ul>
</li>
<?php
	}
?>