-- MySQL dump 10.13  Distrib 5.5.17, for FreeBSD9.0 (amd64)
--
-- Host: localhost    Database: tran_qwey
-- ------------------------------------------------------
-- Server version	5.5.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `tran_qwey`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tran_qwey` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tran_qwey`;

--
-- Temporary table structure for view `050101_view`
--

DROP TABLE IF EXISTS `050101_view`;
/*!50001 DROP VIEW IF EXISTS `050101_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `050101_view` (
  `tgl_transaksi` varchar(10),
  `hari` int(7),
  `bulan` int(6),
  `kode_vendor` char(6),
  `nama_vendor` varchar(45),
  `kode_barang` int(11),
  `nama_barang` varchar(45),
  `kode_satuan` int(11),
  `nama_satuan` varchar(45),
  `harga_perolehan` int(11),
  `jumlah_pembelian` int(11),
  `jumlah_penjualan` int(11),
  `jumlah_persediaan` int(11),
  `keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `050102_view`
--

DROP TABLE IF EXISTS `050102_view`;
/*!50001 DROP VIEW IF EXISTS `050102_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `050102_view` (
  `tgl_transaksi` varchar(7),
  `halaman` int(7),
  `bulan` int(6),
  `kode_barang` int(11),
  `nama_barang` varchar(45),
  `kode_satuan` int(11),
  `nama_satuan` varchar(45),
  `harga_perolehan` int(11),
  `jumlah_pembelian` int(11),
  `jumlah_penjualan` int(11),
  `jumlah_persediaan` int(11),
  `keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `050201_view`
--

DROP TABLE IF EXISTS `050201_view`;
/*!50001 DROP VIEW IF EXISTS `050201_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `050201_view` (
  `tgl_transaksi` date,
  `hari` int(7),
  `bulan` int(6),
  `kode_vendor` char(6),
  `nama_pelanggan` varchar(45),
  `kode_barang` int(11),
  `nama_barang` varchar(45),
  `nama_satuan` varchar(45),
  `harga_penjualan` int(11),
  `jumlah_penjualan` int(11),
  `keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cek_persediaan`
--

DROP TABLE IF EXISTS `cek_persediaan`;
/*!50001 DROP VIEW IF EXISTS `cek_persediaan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cek_persediaan` (
  `Kode` mediumtext,
  `Nama Barang` varchar(45),
  `Posisi Stok` varchar(77),
  `Satuan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_barang`
--

DROP TABLE IF EXISTS `form_barang`;
/*!50001 DROP VIEW IF EXISTS `form_barang`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_barang` (
  `nama_barang` varchar(45),
  `jenis_barang` enum('Obat Dewasa','Obat Anak','Obat Dalam','Obat Luar')
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_harga`
--

DROP TABLE IF EXISTS `form_harga`;
/*!50001 DROP VIEW IF EXISTS `form_harga`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_harga` (
  `nama_barang` varchar(45),
  `nama_satuan` varchar(45),
  `harga_jual` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_opname`
--

DROP TABLE IF EXISTS `form_opname`;
/*!50001 DROP VIEW IF EXISTS `form_opname`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_opname` (
  `kode_status` tinyint(4)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_pelanggan`
--

DROP TABLE IF EXISTS `form_pelanggan`;
/*!50001 DROP VIEW IF EXISTS `form_pelanggan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_pelanggan` (
  `contact_person` varchar(45),
  `nama` varchar(45),
  `alamat` varchar(90),
  `kota` varchar(45),
  `email` varchar(45),
  `phone_number` varchar(45),
  `kode_pos` char(5)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_satuan`
--

DROP TABLE IF EXISTS `form_satuan`;
/*!50001 DROP VIEW IF EXISTS `form_satuan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_satuan` (
  `nama_satuan` varchar(45),
  `jumlah_satuan` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `form_vendor`
--

DROP TABLE IF EXISTS `form_vendor`;
/*!50001 DROP VIEW IF EXISTS `form_vendor`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `form_vendor` (
  `contact_person` varchar(45),
  `nama` varchar(45),
  `alamat` varchar(90),
  `kota` varchar(45),
  `email` varchar(45),
  `phone_number` varchar(45),
  `fax_number` varchar(45),
  `kode_pos` char(5)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `frm_opname`
--

DROP TABLE IF EXISTS `frm_opname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frm_opname` (
  `id_opname` bigint(20) NOT NULL AUTO_INCREMENT,
  `opname_tgl` datetime DEFAULT NULL,
  `kode_status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_opname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frm_opname`
--

LOCK TABLES `frm_opname` WRITE;
/*!40000 ALTER TABLE `frm_opname` DISABLE KEYS */;
/*!40000 ALTER TABLE `frm_opname` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_opname` BEFORE INSERT ON `frm_opname` FOR EACH ROW begin set new.opname_tgl=NOW(); call proses_harian(); end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `kode_biaya`
--

DROP TABLE IF EXISTS `kode_biaya`;
/*!50001 DROP VIEW IF EXISTS `kode_biaya`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `kode_biaya` (
  `Kode` char(5),
  `Keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `kode_satuan`
--

DROP TABLE IF EXISTS `kode_satuan`;
/*!50001 DROP VIEW IF EXISTS `kode_satuan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `kode_satuan` (
  `Nama` varchar(45),
  `Jumlah Item` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_biaya`
--

DROP TABLE IF EXISTS `laporan_biaya`;
/*!50001 DROP VIEW IF EXISTS `laporan_biaya`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_biaya` (
  `Bulan` int(2),
  `Tahun` int(4),
  `Jenis Biaya` varchar(45),
  `01` decimal(32,0),
  `02` decimal(32,0),
  `03` decimal(32,0),
  `04` decimal(32,0),
  `05` decimal(32,0),
  `06` decimal(32,0),
  `07` decimal(32,0),
  `08` decimal(32,0),
  `09` decimal(32,0),
  `10` decimal(32,0),
  `11` decimal(32,0),
  `12` decimal(32,0),
  `13` decimal(32,0),
  `14` decimal(32,0),
  `15` decimal(32,0),
  `16` decimal(32,0),
  `17` decimal(32,0),
  `18` decimal(32,0),
  `19` decimal(32,0),
  `20` decimal(32,0),
  `21` decimal(32,0),
  `22` decimal(32,0),
  `23` decimal(32,0),
  `24` decimal(32,0),
  `25` decimal(32,0),
  `26` decimal(32,0),
  `27` decimal(32,0),
  `28` decimal(32,0),
  `29` decimal(32,0),
  `30` decimal(32,0),
  `31` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_laba`
--

DROP TABLE IF EXISTS `laporan_laba`;
/*!50001 DROP VIEW IF EXISTS `laporan_laba`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_laba` (
  `Bulan` int(2),
  `Tahun` int(4),
  `Jenis Barang` varchar(45),
  `Satuan` varchar(45),
  `01` decimal(43,0),
  `02` decimal(43,0),
  `03` decimal(43,0),
  `04` decimal(43,0),
  `05` decimal(43,0),
  `06` decimal(43,0),
  `07` decimal(43,0),
  `08` decimal(43,0),
  `09` decimal(43,0),
  `10` decimal(43,0),
  `11` decimal(43,0),
  `12` decimal(43,0),
  `13` decimal(43,0),
  `14` decimal(43,0),
  `15` decimal(43,0),
  `16` decimal(43,0),
  `17` decimal(43,0),
  `18` decimal(43,0),
  `19` decimal(43,0),
  `20` decimal(43,0),
  `21` decimal(43,0),
  `22` decimal(43,0),
  `23` decimal(43,0),
  `24` decimal(43,0),
  `25` decimal(43,0),
  `26` decimal(43,0),
  `27` decimal(43,0),
  `28` decimal(43,0),
  `29` decimal(43,0),
  `30` decimal(43,0),
  `31` decimal(43,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_opname`
--

DROP TABLE IF EXISTS `laporan_opname`;
/*!50001 DROP VIEW IF EXISTS `laporan_opname`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_opname` (
  `Nama Barang` varchar(45),
  `Persediaan` varchar(47),
  `Satuan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_pembelian_barang`
--

DROP TABLE IF EXISTS `laporan_pembelian_barang`;
/*!50001 DROP VIEW IF EXISTS `laporan_pembelian_barang`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_pembelian_barang` (
  `Bulan` int(2),
  `Tahun` int(4),
  `Jenis Barang` varchar(45),
  `Satuan` varchar(45),
  `01` decimal(32,0),
  `02` decimal(32,0),
  `03` decimal(32,0),
  `04` decimal(32,0),
  `05` decimal(32,0),
  `06` decimal(32,0),
  `07` decimal(32,0),
  `08` decimal(32,0),
  `09` decimal(32,0),
  `10` decimal(32,0),
  `11` decimal(32,0),
  `12` decimal(32,0),
  `13` decimal(32,0),
  `14` decimal(32,0),
  `15` decimal(32,0),
  `16` decimal(32,0),
  `17` decimal(32,0),
  `18` decimal(32,0),
  `19` decimal(32,0),
  `20` decimal(32,0),
  `21` decimal(32,0),
  `22` decimal(32,0),
  `23` decimal(32,0),
  `24` decimal(32,0),
  `25` decimal(32,0),
  `26` decimal(32,0),
  `27` decimal(32,0),
  `28` decimal(32,0),
  `29` decimal(32,0),
  `30` decimal(32,0),
  `31` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_penjualan_barang`
--

DROP TABLE IF EXISTS `laporan_penjualan_barang`;
/*!50001 DROP VIEW IF EXISTS `laporan_penjualan_barang`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_penjualan_barang` (
  `Bulan` int(2),
  `Tahun` int(4),
  `Jenis Barang` varchar(45),
  `Satuan` varchar(45),
  `01` decimal(32,0),
  `02` decimal(32,0),
  `03` decimal(32,0),
  `04` decimal(32,0),
  `05` decimal(32,0),
  `06` decimal(32,0),
  `07` decimal(32,0),
  `08` decimal(32,0),
  `09` decimal(32,0),
  `10` decimal(32,0),
  `11` decimal(32,0),
  `12` decimal(32,0),
  `13` decimal(32,0),
  `14` decimal(32,0),
  `15` decimal(32,0),
  `16` decimal(32,0),
  `17` decimal(32,0),
  `18` decimal(32,0),
  `19` decimal(32,0),
  `20` decimal(32,0),
  `21` decimal(32,0),
  `22` decimal(32,0),
  `23` decimal(32,0),
  `24` decimal(32,0),
  `25` decimal(32,0),
  `26` decimal(32,0),
  `27` decimal(32,0),
  `28` decimal(32,0),
  `29` decimal(32,0),
  `30` decimal(32,0),
  `31` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_rekap_biaya_perjenis`
--

DROP TABLE IF EXISTS `laporan_rekap_biaya_perjenis`;
/*!50001 DROP VIEW IF EXISTS `laporan_rekap_biaya_perjenis`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_rekap_biaya_perjenis` (
  `Bulan` int(2),
  `Tahun` int(4),
  `Jenis Biaya` varchar(45),
  `Jumlah Biaya` decimal(32,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_rinci_biaya`
--

DROP TABLE IF EXISTS `laporan_rinci_biaya`;
/*!50001 DROP VIEW IF EXISTS `laporan_rinci_biaya`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_rinci_biaya` (
  `Tanggal` datetime,
  `Jenis Biaya` varchar(45),
  `Jumlah Biaya` int(11),
  `Keterangan` varchar(45),
  `Status` tinyint(4)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_rinci_pembelian`
--

DROP TABLE IF EXISTS `laporan_rinci_pembelian`;
/*!50001 DROP VIEW IF EXISTS `laporan_rinci_pembelian`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_rinci_pembelian` (
  `tanggal` date,
  `nomer_po` varchar(45),
  `nomer_kontrak` varchar(45),
  `vendor` varchar(45),
  `nama_barang` varchar(45),
  `nama_satuan` varchar(45),
  `harga` int(11),
  `jumlah_pembelian` int(11),
  `jumlah_penjualan` int(11),
  `jumlah_stok` int(11),
  `keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_rinci_penjualan`
--

DROP TABLE IF EXISTS `laporan_rinci_penjualan`;
/*!50001 DROP VIEW IF EXISTS `laporan_rinci_penjualan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_rinci_penjualan` (
  `tanggal` date,
  `nomer_do` varchar(45),
  `nomer_kontrak` varchar(45),
  `nama_pelanggan` varchar(45),
  `nama_barang` varchar(45),
  `nama_satuan` varchar(45),
  `harga_penjualan` int(11),
  `jumlah_penjualan` int(11),
  `keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `laporan_stok`
--

DROP TABLE IF EXISTS `laporan_stok`;
/*!50001 DROP VIEW IF EXISTS `laporan_stok`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `laporan_stok` (
  `Bulan` int(2),
  `Tahun` int(4),
  `Nama Barang` varchar(45),
  `Satuan` varchar(45),
  `01` double,
  `02` double,
  `03` double,
  `04` double,
  `05` double,
  `06` double,
  `07` double,
  `08` double,
  `09` double,
  `10` double,
  `11` double,
  `12` double,
  `13` double,
  `14` double,
  `15` double,
  `16` double,
  `17` double,
  `18` double,
  `19` double,
  `20` double,
  `21` double,
  `22` double,
  `23` double,
  `24` double,
  `25` double,
  `26` double,
  `27` double,
  `28` double,
  `29` double,
  `30` double,
  `31` double
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `nama_barang`
--

DROP TABLE IF EXISTS `nama_barang`;
/*!50001 DROP VIEW IF EXISTS `nama_barang`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `nama_barang` (
  `Nama` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `persediaan_harian`
--

DROP TABLE IF EXISTS `persediaan_harian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persediaan_harian` (
  `Tanggal Proses` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Tanggal Stok` date NOT NULL,
  `Nama Barang` varchar(45) CHARACTER SET utf8 NOT NULL,
  `Persediaan` varchar(47) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Satuan` varchar(45) CHARACTER SET utf8 NOT NULL,
  `Harga Barang` int(11) NOT NULL,
  `Kode Barang` int(11) NOT NULL,
  `Kode Satuan` int(11) NOT NULL,
  PRIMARY KEY (`Tanggal Stok`,`Kode Barang`,`Kode Satuan`),
  KEY `TANGGAL` (`Tanggal Proses`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persediaan_harian`
--

LOCK TABLES `persediaan_harian` WRITE;
/*!40000 ALTER TABLE `persediaan_harian` DISABLE KEYS */;
/*!40000 ALTER TABLE `persediaan_harian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_barang`
--

DROP TABLE IF EXISTS `ref_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_barang` (
  `kode_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(45) DEFAULT NULL,
  `jenis_barang` enum('Obat Dewasa','Obat Anak','Obat Dalam','Obat Luar') NOT NULL,
  `kode_status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=1441 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_barang`
--

LOCK TABLES `ref_barang` WRITE;
/*!40000 ALTER TABLE `ref_barang` DISABLE KEYS */;
INSERT INTO `ref_barang` VALUES (1,'OBH MADU 55 ML','Obat Luar',1),(2,'OBH MENTHOL 55 ML','Obat Luar',1),(3,'OBH JAHE 55 ML','Obat Luar',1),(4,'Ikamycetin 5m 3.5gr','Obat Luar',1),(5,'microgynon','Obat Luar',1),(6,'Andalan pil KB','Obat Luar',1),(7,'Erlamycetin ED (TT)','Obat Luar',1),(8,'Erlamycetin ED (TM)','Obat Luar',1),(9,'Ventolin inhealer','Obat Luar',1),(10,'Omedom syr.','Obat Luar',1),(11,'Alletrol TM','Obat Luar',1),(12,'Calnic plus kaplet','Obat Luar',1),(13,'Silopect forte','Obat Luar',1),(14,'Tibigon 500mg','Obat Luar',1),(15,'Floxigra 500','Obat Dalam',1),(16,'Omestan 500 str','Obat Dalam',1),(17,'molapect syr. 30mg','Obat Luar',1),(18,'Gastrucid syr.','Obat Luar',1),(19,'Hansaplast roll 1.25x5','Obat Luar',1),(20,'Opistan 500','Obat Luar',1),(21,'Vit K 10mg (erela)','Obat Dalam',1),(22,'Molapect syr 15','Obat Luar',1),(23,'Thiampenicol','Obat Luar',1),(24,'Supertetra cap.','Obat Luar',1),(25,'Fg troches','Obat Luar',1),(26,'Amoxan capsul','Obat Luar',1),(27,'Clindamycin 300 mg box','Obat Dalam',1),(28,'Cefixime syr','Obat Luar',1),(29,'Cefadroxil 125 syr','Obat Luar',1),(30,'Amoxilin syr 125mg','Obat Luar',1),(31,'Cefixime cap.','Obat Luar',1),(32,'Actifed Cough/merah 60ml','Obat Dalam',1),(33,'Actifed EX/hijau 60ml','Obat Luar',1),(34,'Antimo Tblt','Obat Dalam',1),(35,'Antimo Anak Cair','Obat Anak',1),(36,'Anakonidin syr 30ml','Obat Anak',1),(37,'Anakonidin Syr 60ml','Obat Anak',1),(38,'Alkohol 70% 100 ml','Obat Luar',1),(39,'Augentonic Drop 5cc','Obat Anak',1),(40,'Ascardia tab','Obat Luar',1),(41,'Andalan Lactasi','Obat Luar',1),(42,'Abbocath no 20 terumo','Obat Luar',1),(43,'Acyclovir 400 mg str','Obat Dalam',1),(44,'Adidryl inj','Obat Luar',1),(45,'Adimidon inj','Obat Luar',1),(46,'Ambroxol syr','Obat Dalam',1),(47,'Antalgin box','Obat Dalam',1),(48,'Alkohol 70% 1 liter','Obat Luar',1),(49,'Ataroc 50 mg','Obat Dalam',1),(50,'Asmef Landson','Obat Dalam',1),(51,'Abbocath no 22 terumo','Obat Luar',1),(52,'Alphamol','Obat Dalam',1),(53,'ANREMA 100 MG','Obat Dalam',1),(54,'NEW ABSSOLUTE','Obat Dalam',1),(55,'ASAM TRANEKSAMAT INJ','Obat Dalam',1),(56,'ABBOCAT 24','Obat Luar',1),(57,'ANACETINE NEW SYR','Obat Dalam',1),(58,'ANTASIDA DEON KLG','Obat Dalam',1),(59,'AMBEVEN','Obat Luar',1),(60,'Amoxan DRY syr','Obat Luar',1),(61,'Adem Sari','Obat Luar',1),(62,'arfen kaplet','Obat Luar',1),(63,'alkohol pad','Obat Luar',1),(64,'Asma Solon','Obat Luar',1),(65,'Acyclovir 400mg','Obat Dalam',1),(66,'Antalgin str','Obat Dalam',1),(67,'Ambroxol box','Obat Dalam',1),(68,'Amlodipine 10mg tab','Obat Dalam',1),(69,'Albothyl 5ml','Obat Luar',1),(70,'Anakonidin OBH 30ml','Obat Anak',1),(71,'Anakonidin OBH 60ml','Obat Anak',1),(72,'Acnes Cream wash','Obat Luar',1),(73,'Anlene actif','Obat Luar',1),(74,'Anlene gold','Obat Luar',1),(75,'Antangin JRG tab','Obat Luar',1),(76,'Antangin JRG cair','Obat Luar',1),(77,'Aminophylin 200mg','Obat Luar',1),(78,'Asthma Soho','Obat Luar',1),(79,'Acyclovir cream 5gr','Obat Luar',1),(80,'Anastan Forte','Obat Dalam',1),(81,'supertetra cap','Obat Luar',1),(82,'Amdixal 5mg','Obat Luar',1),(83,'Asam tranexamat','Obat Luar',1),(84,'Alprazolam','Obat Luar',1),(85,'Asepso soap','Obat Luar',1),(86,'Absolute fem hyg','Obat Luar',1),(87,'Acnes sealing Gel','Obat Luar',1),(88,'Anadex','Obat Luar',1),(89,'Alvita','Obat Dalam',1),(90,'Acifar cr','Obat Luar',1),(91,'Anrema 50 mg tablet','Obat Dalam',1),(92,'Alofar 100 mg','Obat Dalam',1),(93,'Ampicillin 500 mg','Obat Dalam',1),(94,'Amlodipin 5 mg','Obat Dalam',1),(95,'Alofar 300 mg box','Obat Dalam',1),(96,'Amlodipin 10 mg box','Obat Dalam',1),(97,'Alpara box','Obat Dalam',1),(98,'Alleron box','Obat Dalam',1),(99,'Allopurinol 100 mg box','Obat Dalam',1),(100,'Antasid DOEN box','Obat Dalam',1),(101,'Axamed','Obat Dalam',1),(102,'Abbocath no 18 G','Obat Luar',1),(103,'Aludona A','Obat Dalam',1),(104,'Asam Mefenamat ','Obat Dalam',1),(105,'RHEUMASON MINYAK ANGIN','Obat Luar',1),(106,'ANTANGIN JUNIOR SYR 10 ML','Obat Anak',1),(107,'ALKOHOL SWAB 100\"S ONEMED','Obat Luar',1),(108,'ABAJOS','Obat Luar',1),(109,'ALTIDIN','Obat Luar',1),(110,'ALBOTHYL 10 ML','Obat Luar',1),(111,'AKITA','Obat Dalam',1),(112,'AMINOPHILIN ERPHA 200MG','Obat Luar',1),(113,'ALBION CAP','Obat Luar',1),(114,'Aspilet tab','Obat Luar',1),(115,'Arkavit','Obat Luar',1),(116,'Allerin exp syr 60ml','Obat Luar',1),(117,'acyclovir 200 inf','Obat Luar',1),(118,'Allopurinol 300 mg','Obat Luar',1),(119,'Actifed ML/kuning 60ml syr','Obat Luar',1),(120,'Abbocat no.20 one med','Obat Luar',1),(121,'Aludonna D','Obat Luar',1),(122,'amoxilin tab','Obat Luar',1),(123,'Asifit btl 30 caplet','Obat Luar',1),(124,'Anlene 250 gr','Obat Luar',1),(125,'Aspirin tab','Obat Luar',1),(126,'Adrome','Obat Luar',1),(127,'Bodrexin Tblt','Obat Anak',1),(128,'Batugin Elixir 120ml','Obat Luar',1),(129,'Betadin Sol 5ml','Obat Luar',1),(130,'Betadin Sol 15ml','Obat Luar',1),(131,'Betadin Sol 30ml','Obat Luar',1),(132,'Biogesic syr','Obat Anak',1),(133,'Biolysin Syr60ml','Obat Anak',1),(134,'Biolysin Smart 60ml','Obat Luar',1),(135,'Bisolvon Elixir syr 60ml','Obat Luar',1),(136,'Bisolvon flu Syr 60ml','Obat Luar',1),(137,'Bodrex Extra','Obat Luar',1),(138,'Bodrex Migra','Obat Luar',1),(139,'Balsem Lang 20gr','Obat Luar',1),(140,'Balsem Lang 10gr','Obat Luar',1),(141,'Uni baby\'s Syr','Obat Luar',1),(142,'CDR Fortos','Obat Luar',1),(143,'CTM Zenith','Obat Luar',1),(144,'Breastpum young2','Obat Luar',1),(145,'Bintang tujuh masuk angin','Obat Dalam',1),(146,'Bintang 7 turun panas','Obat Dalam',1),(147,'Biomega','Obat Dalam',1),(148,'Biovitan syr','Obat Dalam',1),(149,'Bufagan syr','Obat Dalam',1),(150,'Broadamox 500','Obat Dalam',1),(151,'BERLOCID TAB','Obat Dalam',1),(152,'Balpirik Merah','Obat Luar',1),(153,'Biogesic Tab','Obat Luar',1),(154,'Biolysin Kids Tab','Obat Luar',1),(155,'betadin kumur 190 cc','Obat Luar',1),(156,'Becombion Forte','Obat Luar',1),(157,'Benoson N 5gr ','Obat Luar',1),(158,'Bodrexin Flu&batuk syr','Obat Anak',1),(159,'Bye bye fever child','Obat Luar',1),(160,'Bye bye fever baby','Obat Luar',1),(161,'Becombion drop','Obat Luar',1),(162,'Becombion syr','Obat Luar',1),(163,'Benoson cream 15gr','Obat Luar',1),(164,'Bufacort-N cream','Obat Luar',1),(165,'Bevalex Cream','Obat Luar',1),(166,'Bioplacenton Jelly 15gr','Obat Luar',1),(167,'Betametasone cream','Obat Luar',1),(168,'Betason N 5gr cream','Obat Luar',1),(169,'Bronchitin syr.','Obat Luar',1),(170,'Bactoprim syr','Obat Luar',1),(171,'Bodrex Flu&batuk','Obat Luar',1),(172,'Biodiar','Obat Luar',1),(173,'Breast pump mimi','Obat Luar',1),(174,'Betadine Feminine ','Obat Luar',1),(175,'Betadine Kumur 100 ml','Obat Luar',1),(176,'Betadine salep','Obat Luar',1),(177,'Bioralit','Obat Dalam',1),(178,'Bronex','Obat Dalam',1),(179,'Bimastan','Obat Dalam',1),(180,'Biropyron','Obat Dalam',1),(181,'Bintamox 500 mg','Obat Dalam',1),(182,'Betahistin','Obat Dalam',1),(183,'Bufantacid tab','Obat Dalam',1),(184,'Bioplan jell','Obat Luar',1),(185,'Bufantacid syr ','Obat Dalam',1),(186,'Bimacyl tab','Obat Dalam',1),(187,'Bimaflox 500 mg','Obat Dalam',1),(188,'Bimoxyl 500 mg','Obat Dalam',1),(189,'Bimadex loz 0,5','Obat Dalam',1),(190,'Bidium','Obat Dalam',1),(191,'Bufacaryl','Obat Dalam',1),(192,'Bicolax','Obat Dalam',1),(193,'Bronkris','Obat Dalam',1),(194,'Benoson cream 5 gr','Obat Luar',1),(195,'Bronchosal','Obat Dalam',1),(196,'Bisoprolol 5 mg','Obat Dalam',1),(197,'BREAST PUMP MAMAE','Obat Luar',1),(198,'BIMALCUS TAB','Obat Luar',1),(199,'BIMADEX LOZ 0,75','Obat Dalam',1),(200,'BALSEM OTOT  ONYMED 10GR','Obat Luar',1),(201,'bitampram 20 mg','Obat Luar',1),(202,'Biolergy tab','Obat Luar',1),(203,'Bodrexyn tmp syr','Obat Luar',1),(204,'Balpirik kuning','Obat Luar',1),(205,'Balpirik hijau','Obat Luar',1),(206,'Biolysin syr 100ml','Obat Luar',1),(207,'Bisolvon extra 60ml','Obat Luar',1),(208,'Botol KP 4 eco pigeon','Obat Luar',1),(209,'Botol KP 8 eco pigeon','Obat Luar',1),(210,'Bitampram 10 mg','Obat Luar',1),(211,'Batugin elixir 300 ml','Obat Luar',1),(212,'Braito original 5 ml','Obat Luar',1),(213,'Bional tab','Obat Luar',1),(214,'Brotec 0,01 mg','Obat Luar',1),(215,'Bear Brand','Obat Luar',1),(216,'CDR Sweet EFF','Obat Luar',1),(217,'Combantrin 125mg','Obat Luar',1),(218,'Combantrin Syr Jeruk','Obat Luar',1),(219,'Combi Kids Jeruk','Obat Anak',1),(220,'Canesten 3gr','Obat Luar',1),(221,'Curcuma Plus Syr 60ml','Obat Anak',1),(222,'Curcuma + DHA 60ml','Obat Anak',1),(223,'Curcuma Emulsion 200 ml','Obat Anak',1),(224,'Colfin Syr','Obat Anak',1),(225,'Chiliplast 1/2x1 (kecil)','Obat Luar',1),(226,'Cimetidine ','Obat Dalam',1),(227,'Colipred cr','Obat Luar',1),(228,'Cydifar','Obat Dalam',1),(229,'Cimetidine str','Obat Dalam',1),(230,'Coparcetin kid syr','Obat Dalam',1),(231,'Centabio gel 20 gr','Obat Luar',1),(232,'Caviplex drop','Obat Dalam',1),(233,'Cavicur DHA syr','Obat Dalam',1),(234,'Cimexol','Obat Dalam',1),(235,'Cetirizin Landson','Obat Dalam',1),(236,'Cortison inj','Obat Luar',1),(237,'Canesten 5 gr','Obat Luar',1),(238,'Calortusin','Obat Dalam',1),(239,'CTM Trifa','Obat Dalam',1),(240,'CIPROFLOXACYN hx','Obat Dalam',1),(241,'Contrexyn','Obat Luar',1),(242,'ciprofloxacin 500 mg berno','Obat Luar',1),(243,'CDR Fruit Punch','Obat Luar',1),(244,'Cetirizine novel','Obat Dalam',1),(245,'Carbamazepine','Obat Luar',1),(246,'Cataflam 50','Obat Luar',1),(247,'Cotrimoxazole','Obat Dalam',1),(248,'Callusol 10ml','Obat Luar',1),(249,'Coldrexin syr','Obat Luar',1),(250,'Caladine lotion 95ml','Obat Luar',1),(251,'Caladine Lotion 60ml','Obat Luar',1),(252,'Counterpain 15gr','Obat Luar',1),(253,'Counterpain 5gr','Obat Luar',1),(254,'Counterpain 30gr','Obat Luar',1),(255,'Cusson baby powder 50mg','Obat Luar',1),(256,'Cusson baby powder 100gr','Obat Luar',1),(257,'Care free UT Usc 20\'','Obat Luar',1),(258,'Care free super dry 20\'','Obat Luar',1),(259,'Care free Loug Usc 20\'','Obat Luar',1),(260,'Charm','Obat Luar',1),(261,'Cerebrofort syr 100ml','Obat Luar',1),(262,'Cerebrofort gold 200cc','Obat Luar',1),(263,'Calc 500mg','Obat Luar',1),(264,'Chloramfecort H cream','Obat Luar',1),(265,'Cendo Corthon ED 5cc','Obat Luar',1),(266,'Cendo xytrol ED 5cc','Obat Luar',1),(267,'Combi kids straw','Obat Luar',1),(268,'Citocetin syr ','Obat Luar',1),(269,'Ciprofloxacin 500mg box novel','Obat Dalam',1),(270,'Cerebrofit Xcell','Obat Luar',1),(271,'Cotton buds reg','Obat Luar',1),(272,'Cotton buds Extra fine','Obat Luar',1),(273,'Cusson baby soap milk','Obat Luar',1),(274,'Cusson baby soap soft','Obat Luar',1),(275,'Cendo Catarlent','Obat Luar',1),(276,'Cerebrofort Gold 100','Obat Luar',1),(277,'Combi kids fruit&veggie','Obat Dewasa',1),(278,'chloramfenicol 250 pot holi','Obat Dalam',1),(279,'Calcifar plus','Obat Dalam',1),(280,'Camivita syr','Obat Dalam',1),(281,'Caviplex tab','Obat Dalam',1),(282,'Cazetin drops','Obat Dalam',1),(283,'Cyclofem inj','Obat Luar',1),(284,'Calcifar tab','Obat Dalam',1),(285,'Cat gut chromic','Obat Luar',1),(286,'Coparcetin syr','Obat Dalam',1),(287,'Cefadroxil 500 mg box soho','Obat Dalam',1),(288,'Ctm HOLI','Obat Dalam',1),(289,'Cayodine 30 ml ','Obat Luar',1),(290,'Clindamycin 300 mg cap','Obat Dalam',1),(291,'Captopril 25 mg box','Obat Dalam',1),(292,'Captopril 12,5 mg box','Obat Dalam',1),(293,'Clindamycin 150 mg','Obat Dalam',1),(294,'Carsida box','Obat Dalam',1),(295,'Cefadroxil 500 mg Berno','Obat Dalam',1),(296,'Cat gut Plain','Obat Dalam',1),(297,'Combivent 2,5 ml UDV','Obat Luar',1),(298,'CINOLON CREAM','Obat Luar',1),(299,'CINOLON N CREAM','Obat Luar',1),(300,'CURVIPLEX SY','Obat Dalam',1),(301,'CONTREXYN DEMAM SYR','Obat Luar',1),(302,'catoon bat 100 by','Obat Luar',1),(303,'cateter no 16','Obat Luar',1),(304,'CAUNTERY SET CORONA GL-999','Obat Luar',1),(305,'CALADINE POWDER 60GR','Obat Luar',1),(306,'CAVIPLEX','Obat Luar',1),(307,'Cendo cyclon Drop 5 ml','Obat Luar',1),(308,'CAMYDRIL INJ 10ML','Obat Luar',1),(309,'cotrimoxazole syr','Obat Luar',1),(310,'Curliv plus syr 120ml','Obat Luar',1),(311,'Cetirizine INF','Obat Luar',1),(312,'custodiol','Obat Luar',1),(313,'Combantrin 250mg','Obat Luar',1),(314,'COLESTEROL NESCO','Obat Luar',1),(315,'Curvit cl emulsion Syr 175 ml','Obat Luar',1),(316,'Curcuma Fct ','Obat Luar',1),(317,'Comtusi syr 60 ml','Obat Luar',1),(318,'Cateter no 18','Obat Luar',1),(319,'Cyanocobalamine 500 ml inj','Obat Luar',1),(320,'Ceteme 4 mg','Obat Luar',1),(321,'Curvit syr 60 ml','Obat Luar',1),(322,'Curcuma plus imuns 60ml','Obat Luar',1),(323,'Cerelac 120 g','Obat Luar',1),(324,'Decadryl EXP 120ml','Obat Luar',1),(325,'Decadryl EXP 60ml','Obat Luar',1),(326,'Decolsin CPL','Obat Luar',1),(327,'Decolsin Syr 60ml','Obat Luar',1),(328,'Dettol Anti Septic 50ml','Obat Luar',1),(329,'Dettol Anti Septic 100ml','Obat Luar',1),(330,'Dextamin Tblt','Obat Luar',1),(331,'Diapet NR','Obat Luar',1),(332,'Diapet Tblt','Obat Luar',1),(333,'Dancau instan enriched 200 g','Obat Dalam',1),(334,'Dapyrin box','Obat Luar',1),(335,'Dextral','Obat Dalam',1),(336,'Dulcolax tab 4\'s','Obat Dalam',1),(337,'Dekorin','Obat Dalam',1),(338,'Dulcolax sup dewasa 10 mg','Obat Luar',1),(339,'Depo progestin','Obat Luar',1),(340,'dimenhidrinat','Obat Luar',1),(341,'DOMPERIDON BOX','Obat Dalam',1),(342,'Dancau instan coklat enrich 200 g','Obat Luar',1),(343,'Decolgen Tab','Obat Luar',1),(344,'Dumin Tab','Obat Dalam',1),(345,'Dialet','Obat Luar',1),(346,'Dancau ful cream 200 g','Obat Luar',1),(347,'Dexa-m tab 0,75','Obat Dalam',1),(348,'Dettol Sabun','Obat Luar',1),(349,'Dactarin 15gr','Obat Luar',1),(350,'Dactarin 5gr','Obat Luar',1),(351,'Durol tonic 225cc','Obat Luar',1),(352,'Dexamethasone 0.5','Obat Luar',1),(353,'Denomix cream','Obat Luar',1),(354,'Dermasolone cream','Obat Luar',1),(355,'Dulcolax 5mg 200\'','Obat Luar',1),(356,'Dexamethasone 0.5','Obat Luar',1),(357,'Dexycol 500 box','Obat Dalam',1),(358,'Dehista','Obat Dalam',1),(359,'Durex 25','Obat Luar',1),(360,'Durex close fit','Obat Luar',1),(361,'Erlamycetin SM','Obat Luar',1),(362,'Dancau 1+ honey probitc 200 g','Obat Luar',1),(363,'Dermatix Ultra','Obat Luar',1),(364,'Dexanta','Obat Luar',1),(365,'Dionicol syr','Obat Dalam',1),(366,'Dancau 3+ honey probitc 200 g','Obat Dalam',1),(367,'Dexteem tab','Obat Dalam',1),(368,'Dexamethason inj','Obat Luar',1),(369,'Dexicorta','Obat Dalam',1),(370,'Diaston','Obat Dalam',1),(371,'Dexacap 25 mg','Obat Dalam',1),(372,'Dionicol 500 mg','Obat Dalam',1),(373,'Digoxin box','Obat Dalam',1),(374,'Dextral Syr','Obat Dalam',1),(375,'Dicloflam 50 mg','Obat Dalam',1),(376,'Dulcolax sup anak 5 mg','Obat Luar',1),(377,'Digoxin str','Obat Dalam',1),(378,'Dexycol 500 mg cap','Obat Dalam',1),(379,'Dexteem plus box','Obat Dalam',1),(380,'Dexamethason 0,75 mg box','Obat Dalam',1),(381,'Depo neo','Obat Luar',1),(382,'DEXYL SYR','Obat Luar',1),(383,'Dermafix','Obat Luar',1),(384,'DARSI','Obat Luar',1),(385,'DIONICOL','Obat Luar',1),(386,'DEXTAF','Obat Luar',1),(387,'Dumin syr','Obat Luar',1),(388,'Diane-35','Obat Luar',1),(389,'DECUBAL CR 20 GR','Obat Luar',1),(390,'Dettol talk 75 gr','Obat Luar',1),(391,'Dot pigeon','Obat Luar',1),(392,'Dextral Forte','Obat Luar',1),(393,'DEHAF','Obat Dalam',1),(394,'Dapyrin syr','Obat Luar',1),(395,'Enervon C','Obat Luar',1),(396,'Etaflusin Syr','Obat Luar',1),(397,'Ester C Btl','Obat Luar',1),(398,'Erpha mazol','Obat Luar',1),(399,'Erla neo hydrocort','Obat Luar',1),(400,'Enervon C SYR 120 ML','Obat Luar',1),(401,'Extra Joss','Obat Luar',1),(402,'Enkasari 120ml','Obat Luar',1),(403,'Em Kapsul','Obat Luar',1),(404,'Ester C','Obat Luar',1),(405,'Enatin','Obat Luar',1),(406,'Ever E','Obat Luar',1),(407,'Etamox syr','Obat Dalam',1),(408,'Easy touch gula','Obat Luar',1),(409,'Easy touch asam urat','Obat Luar',1),(410,'Etamox 500 mg','Obat Dalam',1),(411,'Emturnas','Obat Dalam',1),(412,'Etafungal','Obat Dalam',1),(413,'Easy touch kolesterol','Obat Luar',1),(414,'ETABION','Obat Dalam',1),(415,'Etacurvita syr','Obat Dalam',1),(416,'Eltazon','Obat Dalam',1),(417,'Etadril syr','Obat Dalam',1),(418,'ETAMOXUL SYR','Obat Luar',1),(419,'ULCERANIN','Obat Dalam',1),(420,'ERPHAMAG TAB','Obat Luar',1),(421,'ETAMOX FORTE SY','Obat Dalam',1),(422,'esperson 5 gr','Obat Luar',1),(423,'Erladerm - N','Obat Luar',1),(424,'Erlamol','Obat Luar',1),(425,'Etaflox','Obat Luar',1),(426,'ESENE FACE MASK PINK','Obat Luar',1),(427,'ESENE FACE MASK GREEN','Obat Luar',1),(428,'ESENE BREAS MASK GOLD','Obat Luar',1),(429,'Elkana syr','Obat Luar',1),(430,'Exergi','Obat Luar',1),(431,'Efisol liquid 10 ml','Obat Luar',1),(432,'Empeng pigeon','Obat Luar',1),(433,'Enbatic ','Obat Luar',1),(434,'Eta flusin kpl','Obat Luar',1),(435,'Esene Breast Mask','Obat Luar',1),(436,'Efisol Lozenges','Obat Luar',1),(437,'Enervon c 30\'','Obat Luar',1),(438,'Fitkom rasa','Obat Luar',1),(439,'Farizol','Obat Dalam',1),(440,'Fameprim tablet','Obat Dalam',1),(441,'Fameprim syr','Obat Dalam',1),(442,'Fameprim Forte','Obat Dalam',1),(443,'Fludane syr','Obat Dalam',1),(444,'Flameson','Obat Dalam',1),(445,'Flucadex syr','Obat Dalam',1),(446,'FARSIFEN PLUS','Obat Dalam',1),(447,'FUNGIDERM 5gr','Obat Luar',1),(448,'FRESH Care','Obat Luar',1),(449,'Fiesta Fassion','Obat Luar',1),(450,'Fludane Plus','Obat Luar',1),(451,'Feminax','Obat Luar',1),(452,'Fitkom Gummy','Obat Luar',1),(453,'Fatigon Viro','Obat Luar',1),(454,'Fatigon Spirit','Obat Luar',1),(455,'Fatigon','Obat Luar',1),(456,'Furosemide injection 10mg','Obat Luar',1),(457,'Fitkom jeruk','Obat Luar',1),(458,'Fatigon C plus','Obat Luar',1),(459,'Fresh Care telon','Obat Luar',1),(460,'Farmalat','Obat Dalam',1),(461,'Flucetin syr','Obat Dalam',1),(462,'Farsifen syr','Obat Dalam',1),(463,'Fasidol syr','Obat Dalam',1),(464,'Floxifar','Obat Dalam',1),(465,'Farmoten 25','Obat Dalam',1),(466,'Farmoten 12,5','Obat Dalam',1),(467,'Farmabes','Obat Dalam',1),(468,'Flamigra','Obat Dalam',1),(469,'Flucadex box','Obat Dalam',1),(470,'Fargoxin','Obat Dalam',1),(471,'Fasidol 500 mg','Obat Dalam',1),(472,'Fasidol Forte','Obat Dalam',1),(473,'Faxiden 20 mg','Obat Dalam',1),(474,'Farsifen 400 mg','Obat Dalam',1),(475,'Fenamin 500 mg','Obat Dalam',1),(476,'Faxiden 10 mg','Obat Dalam',1),(477,'Farsifen 200 mg','Obat Dalam',1),(478,'Flumin tab','Obat Dalam',1),(479,'Flumin syr','Obat Dalam',1),(480,'Flutamol','Obat Dalam',1),(481,'Fasiprim','Obat Dalam',1),(482,'Fasidol drops','Obat Dalam',1),(483,'Fasidol F syr','Obat Dalam',1),(484,'Furosemid box','Obat Dalam',1),(485,'Fulopin10 mg','Obat Dalam',1),(486,'Futalin','Obat Dalam',1),(487,'FLUTOP-C SYR','Obat Anak',1),(488,'FLOXIFAR','Obat Luar',1),(489,'FULOPIN 10 GR','Obat Luar',1),(490,'FLUNAX','Obat Luar',1),(491,'FAXIDEN 20','Obat Luar',1),(492,'FORMULA KUMUR','Obat Luar',1),(493,'FLUDANE TAB','Obat Luar',1),(494,'FORMULA ACTION PROTECTOR 190 G','Obat Luar',1),(495,'FLUDANE STRIP','Obat Luar',1),(496,'Flutamol syr','Obat Luar',1),(497,'Famotidin 20mg','Obat Luar',1),(498,'Feminax lancar haid sugar free','Obat Luar',1),(499,'Frozz','Obat Luar',1),(500,'Geliga 10gr','Obat Dewasa',1),(501,'Geliga 20gr','Obat Luar',1),(502,'Geliga 40gr','Obat Luar',1),(503,'Grafadon box','Obat Dalam',1),(504,'Guanistrep Syr','Obat Luar',1),(505,'Gentian Violet','Obat Luar',1),(506,'Grafachlor str','Obat Dalam',1),(507,'Grafacetin box','Obat Dalam',1),(508,'Grafalin 4 mg','Obat Dalam',1),(509,'Gricin 125 mg box','Obat Dalam',1),(510,'GP Care testpect','Obat Luar',1),(511,'GRafadon drop','Obat Dalam',1),(512,'GENOINT SM','Obat Luar',1),(513,'GELIGA MO 30ml','Obat Luar',1),(514,'GPU 60ml','Obat Luar',1),(515,'Gandapura Ika 60ml','Obat Luar',1),(516,'Glucobay 100mg','Obat Luar',1),(517,'GOM (borax gliserin)','Obat Luar',1),(518,'Gentamycin 0.1% 5gr','Obat Luar',1),(519,'griseofulvin 125 tab','Obat Luar',1),(520,'Glucophage 500','Obat Luar',1),(521,'Gastrucid box','Obat Dalam',1),(522,'GPU 30ml','Obat Luar',1),(523,'Gludepatic','Obat Luar',1),(524,'Grafamic','Obat Dalam',1),(525,'Grahabion box','Obat Dalam',1),(526,'Grafix','Obat Luar',1),(527,'Griseofulvin 500','Obat Luar',1),(528,'Geliga Mo 60ml','Obat Dewasa',1),(529,'Gentamicin salep','Obat Luar',1),(530,'Grafalin 2 mg','Obat Dalam',1),(531,'Gratheos 50 mg','Obat Dalam',1),(532,'Ginifar','Obat Dalam',1),(533,'Grafazol 500 mg','Obat Dalam',1),(534,'Genoint SK','Obat Luar',1),(535,'Genoint TM','Obat Luar',1),(536,'Gentalex salep','Obat Luar',1),(537,'Grafadon syr','Obat Dalam',1),(538,'Gentamicin inj','Obat Dalam',1),(539,'Grafachlor box','Obat Dalam',1),(540,'Gralixa box','Obat Dalam',1),(541,'Grantusif','Obat Dalam',1),(542,'Grazeo 10','Obat Dalam',1),(543,'Grazeo 20','Obat Dalam',1),(544,'Grathazon','Obat Dalam',1),(545,'Grafadon forte','Obat Dalam',1),(546,'Griseofulvin 125 box','Obat Dalam',1),(547,'Gabiten','Obat Dalam',1),(548,'Glucolab test','Obat Luar',1),(549,'Glucolab alat','Obat Luar',1),(550,'Glibenclamide 5 mg box','Obat Dalam',1),(551,'gandapura 30 ml','Obat Luar',1),(552,'GLUDEPATIC','Obat Luar',1),(553,'GLUCOSE STRIP NESCO 25\'\'S','Obat Luar',1),(554,'GITRI SYR','Obat Luar',1),(555,'GITRI TAB','Obat Luar',1),(556,'GLUCOSE UNICAP 5%/OGB','Obat Luar',1),(557,'Glimepiride 2 mg tab','Obat Luar',1),(558,'Histigo','Obat Dalam',1),(559,'Hemaviton ACT','Obat Luar',1),(560,'Hufagrip BP Syr','Obat Anak',1),(561,'Hufagrip Flu ','Obat Luar',1),(562,'Hufagrip Pilek','Obat Anak',1),(563,'Hufalisin Syr','Obat Luar',1),(564,'Hufagrip F Tblt','Obat Dalam',1),(565,'Hemaviton energi drink','Obat Luar',1),(566,'Hufamag plus box','Obat Dalam',1),(567,'hansaplas fun','Obat Dalam',1),(568,'Hufaflox','Obat Dalam',1),(569,'Hufaxol syr','Obat Dalam',1),(570,'Hufalact','Obat Dalam',1),(571,'Hufaneuron','Obat Dalam',1),(572,'Hufaxicam 15 mg','Obat Dalam',1),(573,'Hufagrip TMP','Obat Luar',1),(574,'Hemaviton jreng Anggur','Obat Luar',1),(575,'Herocyn 75gr','Obat Luar',1),(576,'Herocyn 150gr','Obat Luar',1),(577,'Hansaplast standar','Obat Luar',1),(578,'Hansaplast roll 1.25x1','Obat Luar',1),(579,'Hansaplast Kompres','Obat Luar',1),(580,'Hydrocortisone 2.5% 5gr','Obat Luar',1),(581,'Habbasy Plus 120','Obat Luar',1),(582,'Habbasy 100','Obat Luar',1),(583,'Habbat\'s Sehat','Obat Luar',1),(584,'Habbat\'s kid honey','Obat Luar',1),(585,'Habbat\'s Nigellive 30\'','Obat Luar',1),(586,'Habbat\'s Blackseed oil 10ml','Obat Luar',1),(587,'Nichomycin 500','Obat Luar',1),(588,'Habbat\'s Plus','Obat Luar',1),(589,'Heel soft','Obat Luar',1),(590,'Hufanoxyl 500','Obat Dalam',1),(591,'Hemaviton Stamina +','Obat Luar',1),(592,'Hufadin','Obat Dalam',1),(593,'hansaplas koyo hangat','Obat Luar',1),(594,'Hexymer','Obat Luar',1),(595,'Hufalysin New','Obat Luar',1),(596,'Hufalysin plus','Obat Luar',1),(597,'Hufalysin Plus DHA ','Obat Luar',1),(598,'hansaplas koyo panas','Obat Dalam',1),(599,'Hufagesic 500 mg','Obat Dalam',1),(600,'Hufabion','Obat Dalam',1),(601,'Hufalerzin syr','Obat Dalam',1),(602,'Hufralgin','Obat Dalam',1),(603,'Helixim cap','Obat Dalam',1),(604,'Hufanoxil syr','Obat Dalam',1),(605,'Hufamaag syr ','Obat Dalam',1),(606,'hansaplast jumbo','Obat Dalam',1),(607,'Helixim syr','Obat Dalam',1),(608,'Herocyn baby100gr','Obat Luar',1),(609,'Herocyn baby 200gr','Obat Luar',1),(610,'HEMORID','Obat Luar',1),(611,'HUFAGESIC SYR','Obat Dalam',1),(612,'HAND&MOUNTH BABY WIPES','Obat Luar',1),(613,'HOLIMOX SYR FORTE','Obat Luar',1),(614,'HOLIMYCETIN SYR','Obat Luar',1),(615,'HUFAGRIF TAB','Obat Luar',1),(616,'Hydrocortison 1%cr 5gr','Obat Luar',1),(617,'Holidon tab','Obat Luar',1),(618,'Holimox syr','Obat Luar',1),(619,'Hilo gold coklat 250g','Obat Luar',1),(620,'Hilo teen 250g','Obat Luar',1),(621,'Histaklor tab','Obat Luar',1),(622,'Halmezin syr 100ml','Obat Luar',1),(623,'Holidryl syr','Obat Luar',1),(624,'Hufaxol tab','Obat Luar',1),(625,'Infusan RL','Obat Dalam',1),(626,'Insto 7,5ml','Obat Luar',1),(627,'Insto 15ml','Obat Luar',1),(628,'Intunal Forte','Obat Luar',1),(629,'Inadoxin Forte','Obat Dalam',1),(630,'Ifasma','Obat Dalam',1),(631,'Intunal F box','Obat Dalam',1),(632,'Ikadryl syr 60','Obat Luar',1),(633,'Inzana','Obat Luar',1),(634,'Inza','Obat Luar',1),(635,'Igastrum plus syr 60 ml','Obat Luar',1),(636,'Incidal OD','Obat Luar',1),(637,'Inamid box','Obat Dalam',1),(638,'Isosorbide Dinitrat','Obat Dalam',1),(639,'Ibuprofen 400 tab','Obat Dalam',1),(640,'Ichtyiol salep','Obat Luar',1),(641,'Itramol syr','Obat Luar',1),(642,'Imufor plus syr','Obat Luar',1),(643,'Inerson cream','Obat Luar',1),(644,'Inflason','Obat Luar',1),(645,'infuset dewasa','Obat Luar',1),(646,'Intunal strip','Obat Dalam',1),(647,'Infatrim syr','Obat Dalam',1),(648,'Ikamicetin SM','Obat Dalam',1),(649,'Itramol 500 mg','Obat Dalam',1),(650,'Infatrim F','Obat Dalam',1),(651,'Infatrim tab','Obat Dalam',1),(652,'Infalgin ','Obat Dalam',1),(653,'Iodin povidon 30 ml','Obat Luar',1),(654,'Inamid str','Obat Dalam',1),(655,'Intunal box','Obat Dalam',1),(656,'Iodin povidon 60 ml','Obat Luar',1),(657,'Infus set otsuka','Obat Luar',1),(658,'Ibuprofen 400 box HOLLI','Obat Dalam',1),(659,'INFUSION SET ONEMEO','Obat Luar',1),(660,'IBUPROFEN INF','Obat Luar',1),(661,'IGASTRUM SYR','Obat Luar',1),(662,'Isorbid','Obat Luar',1),(663,'INFUSET COSMO MED','Obat Luar',1),(664,'IMBOOS FORCE 60ML','Obat Luar',1),(665,'Ifidex 0,5','Obat Luar',1),(666,'Incidal tab','Obat Luar',1),(667,'Itrabat syr','Obat Luar',1),(668,'IMBOOST FORCE KAP','Obat Dalam',1),(669,'Imodium','Obat Luar',1),(670,'Imboost tab','Obat Luar',1),(671,'Imboost syr 60ml','Obat Luar',1),(672,'Jess Cool','Obat Luar',1),(673,'Jf sulfur sabun','Obat Luar',1),(674,'Johnson baby cream','Obat Luar',1),(675,'Johnson baby powder reg','Obat Luar',1),(676,'Johnson baby top to wash','Obat Luar',1),(677,'Johnson powder blossom 100gr','Obat Luar',1),(678,'Johnson baby soap','Obat Luar',1),(679,'JB oil','Obat Luar',1),(680,'Jarum hekting','Obat Luar',1),(681,'JohNson Baby powder bloosom 500 gr','Obat Luar',1),(682,'JoHNson baby powder bloosom 300 gr','Obat Luar',1),(683,'Ketoprofen 100mg','Obat Dalam',1),(684,'KSR','Obat Dalam',1),(685,'Ketorolac inj','Obat Luar',1),(686,'Kandistatin drop','Obat Dalam',1),(687,'Kanna 30 gr','Obat Luar',1),(688,'KIDIKOL SYR','Obat Dalam',1),(689,'Konidin Strong Mint','Obat Luar',1),(690,'Koyo Cabe','Obat Luar',1),(691,'Komix KIDS','Obat Luar',1),(692,'Konidin','Obat Anak',1),(693,'Kuku Bima Anggur','Obat Luar',1),(694,'Kalmethasone tab','Obat Dalam',1),(695,'Kalnex 500','Obat Luar',1),(696,'Ketoprofen 50mg tab','Obat Dalam',1),(697,'Kiranti','Obat Luar',1),(698,'Kratingdaeng','Obat Luar',1),(699,'Kapsul Cacing','Obat Luar',1),(700,'Kaotin syr','Obat Luar',1),(701,'Kanna 15 gr','Obat Luar',1),(702,'Kaka tua','Obat Luar',1),(703,'Kamilosan Ointment','Obat Luar',1),(704,'Kalpanax cair 10cc','Obat Luar',1),(705,'Kalpanax cream','Obat Luar',1),(706,'Kapas 500gr Bunda','Obat Luar',1),(707,'Kapas 100gr Bunda','Obat Luar',1),(708,'Kapas 50gr Bunda','Obat Luar',1),(709,'Kapas 25gr Bunda','Obat Luar',1),(710,'Kasa steril Bunda Biru','Obat Luar',1),(711,'Kasa Steril Husada','Obat Luar',1),(712,'Kapas 250gr Bunda','Obat Luar',1),(713,'Kapas kharisma','Obat Luar',1),(714,'Komix','Obat Luar',1),(715,'Kompolax syr','Obat Dalam',1),(716,'Ketoconazol cr','Obat Luar',1),(717,'Kurkumex tab','Obat Dalam',1),(718,'Klip besar','Obat Luar',1),(719,'Klip sedang','Obat Luar',1),(720,'Kalmethason box','Obat Dalam',1),(721,'Kresek putih','Obat Luar',1),(722,'Kasa gulung 1 m','Obat Luar',1),(723,'Klip kecil','Obat Luar',1),(724,'Ketoconazol 200 mg box','Obat Dalam',1),(725,'Klorfeson','Obat Luar',1),(726,'kertas puyer','Obat Luar',1),(727,'KOTAK P3K','Obat Luar',1),(728,'KOMIX DT','Obat Luar',1),(729,'KEJI BELING','Obat Luar',1),(730,'KAPSIDA CAP','Obat Luar',1),(731,'Konvermex suspensi 125 mg','Obat Luar',1),(732,'Legiron','Obat Luar',1),(733,'Laxing','Obat Luar',1),(734,'Lokev','Obat Dalam',1),(735,'Lanakalk','Obat Dalam',1),(736,'Lexapram tab','Obat Dalam',1),(737,'laxadine 30 ml','Obat Dalam',1),(738,'Livron B Plex','Obat Dalam',1),(739,'Lostacef box','Obat Dalam',1),(740,'Loratadine Landson','Obat Dalam',1),(741,'Licodexon 0,5 berlico','Obat Dalam',1),(742,'LICOKALK','Obat Dalam',1),(743,'LEVOFLOXACYN NOVEL','Obat Dalam',1),(744,'LANCET COSMO MED','Obat Luar',1),(745,'LASERIN 30ml','Obat Luar',1),(746,'LASERIN 60ml','Obat Luar',1),(747,'LASERIN 100ml','Obat Luar',1),(748,'LASERIN MADU 30ml','Obat Luar',1),(749,'LASERIN MADU 60ml','Obat Luar',1),(750,'Lotte TM','Obat Luar',1),(751,'Lacto B','Obat Luar',1),(752,'Lelap','Obat Luar',1),(753,'Listerin Cool Mint 250ml','Obat Luar',1),(754,'Listerin Original 80ml','Obat Luar',1),(755,'Listerin Fres Citrus 80ml','Obat Luar',1),(756,'Listerin Cool Mint 80ml','Obat Luar',1),(757,'Larutan Kaki 3 200ml','Obat Luar',1),(758,'Larutan Kaleng jambu','Obat Luar',1),(759,'Larutan kaleng straw','Obat Luar',1),(760,'Laxadin 60ml','Obat Luar',1),(761,'Laurier SM','Obat Luar',1),(762,'Lyphen','Obat Luar',1),(763,'Lipitor 40mg','Obat Luar',1),(764,'Levofloxacin 500mg tablet','Obat Luar',1),(765,'Lincomycin 500mg','Obat Dalam',1),(766,'Loratadine 10mg box','Obat Dalam',1),(767,'Lansoprazole 30mg novel','Obat Dalam',1),(768,'Latibet','Obat Luar',1),(769,'Lerzin syr','Obat Dalam',1),(770,'Lostacef syr','Obat Dalam',1),(771,'Lerzin tab','Obat Dalam',1),(772,'Lexahist','Obat Dalam',1),(773,'Librozym','Obat Dalam',1),(774,'Lazol','Obat Dalam',1),(775,'Lytamin syr','Obat Dalam',1),(776,'Loratadin str','Obat Dalam',1),(777,'Lanadexon ','Obat Dalam',1),(778,'Lidocain inj','Obat Luar',1),(779,'Levofloxacin 500 INf box','Obat Dalam',1),(780,'Lansoprazole 30 mg INF','Obat Dalam',1),(781,'L-Mulvit syr','Obat Dalam',1),(782,'Latibet box','Obat Dalam',1),(783,'Laxatab','Obat Dalam',1),(784,'Leukoplas 1/2x5 (sedang)','Obat Luar',1),(785,'Leukoplas 1x5 (besar)','Obat Luar',1),(786,'LANCET MEDILANCE','Obat Luar',1),(787,'LEUKOPLAST 2X5','Obat Luar',1),(788,'LOMATUELL H','Obat Luar',1),(789,'LIBROZIM','Obat Luar',1),(790,'LIFLAMAL 400','Obat Luar',1),(791,'LEUKOPLAS 3 X5','Obat Luar',1),(792,'LANCAR ASI','Obat Luar',1),(793,'Lexacorton','Obat Luar',1),(794,'Lifadrox ds','Obat Luar',1),(795,'LEXAPRAM SY','Obat Dalam',1),(796,'LEXICAM 20','Obat Dalam',1),(797,'LEXICAM 10','Obat Dalam',1),(798,'Lactogen Pro 180 g','Obat Luar',1),(799,'Lactogen 1 probiotics 180 g','Obat Luar',1),(800,'Melanox cream','Obat Luar',1),(801,'Mipi 60ml','Obat Luar',1),(802,'MINYAK KAYU PUTIH 15ml','Obat Luar',1),(803,'MINYAK KAYU PUTIH 30ml','Obat Luar',1),(804,'MINYAK KAYU PUTIH 60ml','Obat Luar',1),(805,'MKP LANG 120ml','Obat Luar',1),(806,'MA KAPAK 3ml','Obat Luar',1),(807,'MA KAPAK 5ml','Obat Luar',1),(808,'MA KAPAK 14ml','Obat Luar',1),(809,'M TELON LANG 60ml','Obat Luar',1),(810,'M TELON LANG 30 ml','Obat Luar',1),(811,'MOLEX FLU strip','Obat Luar',1),(812,'MOLAGIT','Obat Dalam',1),(813,'Molaneuron','Obat Dalam',1),(814,'Mirasic box','Obat Dalam',1),(815,'Mecodiar','Obat Dalam',1),(816,'Mecoxon','Obat Dalam',1),(817,'Mofulex cr','Obat Luar',1),(818,'Mycazol','Obat Luar',1),(819,'MIRASIC FORTE','Obat Dalam',1),(820,'MIRASIC SYR','Obat Dalam',1),(821,'Mecobalamin 500mg','Obat Luar',1),(822,'MKP Sidola 15ml','Obat Luar',1),(823,'MKP Sidola 30ml','Obat Luar',1),(824,'MKP Sidola 55ml','Obat Luar',1),(825,'M Telon Meneer 60ml','Obat Luar',1),(826,'M Telon Meneer 30ml','Obat Luar',1),(827,'M Telon Konicare 30ml','Obat Luar',1),(828,'Mipi 30ml','Obat Luar',1),(829,'MA Kapak 10ml','Obat Luar',1),(830,'Ma Kapak 28ml','Obat Luar',1),(831,'M Tawon DD 30ml','Obat Luar',1),(832,'Mextril','Obat Luar',1),(833,'Mixagrip Flu','Obat Luar',1),(834,'Mixagrip Flu&Batuk','Obat Luar',1),(835,'Mepromag str','Obat Dalam',1),(836,'mylanta tab','Obat Luar',1),(837,'Mensana','Obat Luar',1),(838,'Metronidazole','Obat Luar',1),(839,'Mycoral tab','Obat Luar',1),(840,'Mefinal','Obat Luar',1),(841,'Meloxicam 15mg tab','Obat Dalam',1),(842,'MethylPrednisolone 4mg box','Obat Dalam',1),(843,'M 150','Obat Luar',1),(844,'Mylanta liq 150ml','Obat Luar',1),(845,'Mylanta liq 50ml','Obat Luar',1),(846,'Melanox F cream','Obat Luar',1),(847,'Micorex tincture 10cc','Obat Luar',1),(848,'Microlac gel','Obat Luar',1),(849,'Molakrim 30gr','Obat Luar',1),(850,'Masker karet','Obat Luar',1),(851,'Masker tali','Obat Luar',1),(852,'Molexflu','Obat Dalam',1),(853,'Mecola','Obat Luar',1),(854,'Mixagrip Pegal linu','Obat Luar',1),(855,'Micoral cream','Obat Luar',1),(856,'Mycoral SK','Obat Luar',1),(857,'Molason cr','Obat Luar',1),(858,'Moxigra 500 mg','Obat Dalam',1),(859,'Mixalgin','Obat Dalam',1),(860,'Meloxicam 7.5 mg','Obat Dalam',1),(861,'Miconazol','Obat Luar',1),(862,'Myloxan','Obat Dalam',1),(863,'MethylPrednisolone 4 mg str','Obat Dalam',1),(864,'Methyl prednisolon 8 mg','Obat Dalam',1),(865,'Meloxicam 15 mg box','Obat Dalam',1),(866,'Mepromag box','Obat Dalam',1),(867,'Maagel syr','Obat Dalam',1),(868,'M TAWON EE 60 ML','Obat Luar',1),(869,'M TAWON FF 90 ML','Obat Luar',1),(870,'MOMILEN 15 GR','Obat Luar',1),(871,'MOMILEN 30 GR','Obat Luar',1),(872,'MOLADERM','Obat Luar',1),(873,'MEFINAL','Obat Luar',1),(874,'MADICAL REGULATOR','Obat Luar',1),(875,'MWF PRO COOL N FRESH 90 ML','Obat Luar',1),(876,'M TELON KONICARE 60 ML','Obat Luar',1),(877,'M KAYU PUTIH ONYMED 60 ML','Obat Luar',1),(878,'M KAYU PUTIH ONYMED 30 ML','Obat Luar',1),(879,'M KAYU PUTIH ONYMED 15 ML','Obat Luar',1),(880,'M TELON ONYMED 60 ML','Obat Luar',1),(881,'M TELON ONYMED 30 ML','Obat Luar',1),(882,'M TELON ONYMED 15 ML','Obat Luar',1),(883,'MOLACORT','Obat Luar',1),(884,'MA KAPAK 56ML','Obat Luar',1),(885,'MA AROMATIC','Obat Luar',1),(886,'MOLACORT 0,75 MG','Obat Luar',1),(887,'MIRASIC F TAB','Obat Luar',1),(888,'MY BABY MINYAK TELON PLUS 60 ML','Obat Luar',1),(889,'MINYAK OLES BOKASHI 12 ML','Obat Luar',1),(890,'MINYAK OLES BOKASHI 35 ML','Obat Luar',1),(891,'miratrim susp 60ml syr','Obat Luar',1),(892,'mycoral tab','Obat Luar',1),(893,'Minyak kayu putih 120ml','Obat Luar',1),(894,'Maximus cap','Obat Luar',1),(895,'MERIT BESAR','Obat Luar',1),(896,'Molagit tab','Obat Luar',1),(897,'Momilen cr baby','Obat Luar',1),(898,'Migran','Obat Luar',1),(899,'Neurotropin inj','Obat Luar',1),(900,'needle 24 terumo','Obat Dalam',1),(901,'NEO RHEUMACYL','Obat Luar',1),(902,'NEO RHEMACYL NEURO','Obat Luar',1),(903,'needle 26 terumo','Obat Dalam',1),(904,'NEUROBION','Obat Luar',1),(905,'Novamox 500 box','Obat Dalam',1),(906,'Novamag syr','Obat Dalam',1),(907,'NEW DIATAB','Obat Luar',1),(908,'NUFADEX 0,5mg','Obat Luar',1),(909,'NUFADEX 0,75mg','Obat Dalam',1),(910,'Novadex 0,5 loz','Obat Dalam',1),(911,'NATURE E','Obat Luar',1),(912,'Neozep Forte','Obat Luar',1),(913,'Neo Entrostop','Obat Luar',1),(914,'Neo Entrostop Anak','Obat Luar',1),(915,'Norit','Obat Luar',1),(916,'Neo Sanmag Fast','Obat Luar',1),(917,'Neo Napacin','Obat Luar',1),(918,'Neurodex box','Obat Dalam',1),(919,'Neuralgin RX tab','Obat Dalam',1),(920,'Nouriskin Ultimate','Obat Luar',1),(921,'Natur Slim','Obat Luar',1),(922,'Neosma','Obat Luar',1),(923,'Nutrimama 2 Blister','Obat Luar',1),(924,'Nerva plus','Obat Luar',1),(925,'Nouriskin','Obat Luar',1),(926,'Neprolit tab','Obat Luar',1),(927,'Neo rheumacyl salep','Obat Luar',1),(928,'Neo rheumacyl cr merah','Obat Luar',1),(929,'Neo rheumacyl cr hijau','Obat Luar',1),(930,'Neple shit Young2','Obat Luar',1),(931,'Needle 23 Bd','Obat Luar',1),(932,'Novatrim syr','Obat Dalam',1),(933,'Novadryl syr','Obat Dalam',1),(934,'Nisagon cr','Obat Luar',1),(935,'Neuromec','Obat Dalam',1),(936,'Neuropyron V','Obat Dalam',1),(937,'Nifedipin','Obat Dalam',1),(938,'Nutradon loz','Obat Dalam',1),(939,'Nefromex ','Obat Dalam',1),(940,'Neuralgin RX box','Obat Dalam',1),(941,'Natrium Diklofenak box','Obat Dalam',1),(942,'Neurodex str','Obat Dalam',1),(943,'needle 25 bd','Obat Luar',1),(944,'NEEDLE 24 BD','Obat Luar',1),(945,'NEEDLE 27 Bd','Obat Luar',1),(946,'NEUROMEC','Obat Luar',1),(947,'Neolaqi','Obat Luar',1),(948,'Noverty tab','Obat Luar',1),(949,'Neo ultraciline cr','Obat Luar',1),(950,'Naclex','Obat Luar',1),(951,'Nutriflam cap','Obat Luar',1),(952,'new astar crm','Obat Luar',1),(953,'Neutropic inj','Obat Luar',1),(954,'Norvom syr','Obat Luar',1),(955,'Ottoprim syr','Obat Dalam',1),(956,'Omeric 300','Obat Dalam',1),(957,'Omegrip','Obat Dalam',1),(958,'Omegrip syr','Obat Dalam',1),(959,'Oxytetra SM','Obat Luar',1),(960,'OBH COMBI plus flu 60ml','Obat Luar',1),(961,'OBH COMBI BRDAHAK 100ml','Obat Dalam',1),(962,'OBH COMBI ANAK rasa 60ml','Obat Luar',1),(963,'OBIMIN AF','Obat Luar',1),(964,'OBH NELCO SPESIAL 100ml','Obat Luar',1),(965,'Oskadon SP','Obat Luar',1),(966,'Oskadon','Obat Luar',1),(967,'Oralit','Obat Luar',1),(968,'Omeprazole sanbe','Obat Luar',1),(969,'Omeprazole NOVEL','Obat Dalam',1),(970,'Omekur box','Obat Dalam',1),(971,'Ondansetron 8mg','Obat Luar',1),(972,'OBH Combi plus flu 100ml','Obat Luar',1),(973,'OB Herbal dwasa','Obat Luar',1),(974,'Omevomid syr.','Obat Luar',1),(975,'Omedom box','Obat Dalam',1),(976,'Ondansetron 8mg','Obat Luar',1),(977,'Omeproxil 500 str','Obat Luar',1),(978,'Ofloxacin 200 mg novel','Obat Dalam',1),(979,'Omegtrim syr','Obat Luar',1),(980,'Obat gigi burung kakak tua','Obat Luar',1),(981,'One med testpect','Obat Luar',1),(982,'Omeproxil 500 mg box','Obat Dalam',1),(983,'OBH Nellco anak 100 ml','Obat Anak',1),(984,'OBH Nellco anak 55 ml','Obat Anak',1),(985,'Omegdiar syr','Obat Dalam',1),(986,'Omemox syr','Obat Dalam',1),(987,'Omeroxol syr','Obat Dalam',1),(988,'Omeroxol tab','Obat Dalam',1),(989,'Omemox 500 mg','Obat Dalam',1),(990,'Ottoprim tab','Obat Dalam',1),(991,'Orphen','Obat Dalam',1),(992,'Oxytetracycline SK','Obat Luar',1),(993,'Obh Itrasal','Obat Dalam',1),(994,'Otoryl','Obat Dalam',1),(995,'Ottopan tab','Obat Dalam',1),(996,'Ofloxacin 200 mg INF','Obat Dalam',1),(997,'Omestan 500 mg box','Obat Dalam',1),(998,'Ofloxacin 400 mg','Obat Dalam',1),(999,'Omeneuron box','Obat Dalam',1),(1000,'Omedom str','Obat Dalam',1),(1001,'Omekur str','Obat Dalam',1),(1002,'Omegluphage box','Obat Dalam',1),(1003,'OBAT BATUK IBU DAN ANAK','Obat Luar',1),(1004,'OBH NELCO ANAK 30 ML','Obat Anak',1),(1005,'OBH NELLCO SPESIAL 30 CC','Obat Dalam',1),(1006,'OBH NELLCO SPESIAL 55 CC MADU','Obat Dalam',1),(1007,'OBH NELLCO SPESIAL 55 CC JAHE','Obat Dalam',1),(1008,'OBH NELLCO SPECIAL 55 CC MENTHOL','Obat Dalam',1),(1009,'OBH NELLCO BB 55 ML','Obat Dalam',1),(1010,'OBP ITRASAL 100 ML','Obat Dalam',1),(1011,'OBH NELCO PLUS 55 ML','Obat Luar',1),(1012,'ORPHEN','Obat Luar',1),(1013,'Omefulvin','Obat Luar',1),(1014,'Oxygen mask','Obat Luar',1),(1015,'OBH combi berdahak 60ml','Obat Luar',1),(1016,'Omepros','Obat Luar',1),(1017,'Ovutest Scope','Obat Luar',1),(1018,'Obh combi btk flu madu 100 ml','Obat Luar',1),(1019,'Obh combi btk flu madu 60 ml','Obat Luar',1),(1020,'OB Herbal Junior','Obat Luar',1),(1021,'Procold Promuno','Obat Dalam',1),(1022,'Pronicy','Obat Dalam',1),(1023,'Protexinal tab','Obat Dalam',1),(1024,'Prodermis','Obat Luar',1),(1025,'Piracetam 800 mg','Obat Dalam',1),(1026,'Paraflu','Obat Dalam',1),(1027,'PREDNISON POT ZENITH','Obat Dalam',1),(1028,'AMLODIPIN 5 MG STP','Obat Dalam',1),(1029,'KADITIC','Obat Dalam',1),(1030,'ONDANCETRON INJ 4 MG','Obat Dalam',1),(1031,'PANADOL','Obat Luar',1),(1032,'PANADOL EXTRA','Obat Luar',1),(1033,'PANADOL biru','Obat Luar',1),(1034,'PANADOL CHEW','Obat Luar',1),(1035,'PHARMATON FORMULA','Obat Luar',1),(1036,'PEDITOX 50ml','Obat Luar',1),(1037,'PREDNISONE','Obat Luar',1),(1038,'Phylocin 250 cap','Obat Luar',1),(1039,'Procold','Obat Luar',1),(1040,'Panadol Anak','Obat Luar',1),(1041,'Poldan Mig','Obat Luar',1),(1042,'Paramex Tab','Obat Luar',1),(1043,'plester 5mx5m mufix','Obat Luar',1),(1044,'Pharolit','Obat Luar',1),(1045,'Polysilane Tab','Obat Dalam',1),(1046,'Promag Tab ','Obat Luar',1),(1047,'Pil Kita','Obat Luar',1),(1048,'Protecal Solid Eff Tube','Obat Luar',1),(1049,'Protecal Solid Tab','Obat Luar',1),(1050,'Plantacid Forte','Obat Luar',1),(1051,'Ponstan 500','Obat Luar',1),(1052,'Pedialyte 500ml','Obat Luar',1),(1053,'Pocari sweat 350ml','Obat Luar',1),(1054,'Pocari Sweat 500ml','Obat Luar',1),(1055,'Propepsa ','Obat Luar',1),(1056,'Polycrol syr','Obat Dalam',1),(1057,'Polysilane susp 100ml','Obat Luar',1),(1058,'Pil tuntas','Obat Luar',1),(1059,'Mikorek salep','Obat Luar',1),(1060,'Pabanox cream','Obat Luar',1),(1061,'Permen tolak angin','Obat Luar',1),(1062,'pampers banitore dws','Obat Luar',1),(1063,'Pembalut kambium','Obat Luar',1),(1064,'PK','Obat Luar',1),(1065,'Panadol syr','Obat Luar',1),(1066,'Proris susp','Obat Luar',1),(1067,'Proris Forte','Obat Luar',1),(1068,'Pamol syr 60ml','Obat Luar',1),(1069,'Planotab.','Obat Luar',1),(1070,'polly crape no.3','Obat Luar',1),(1071,'Pagoda Salep','Obat Luar',1),(1072,'Pagoda Pastiles mint 100gr','Obat Luar',1),(1073,'Pacdin cough syr','Obat Luar',1),(1074,'Permen woods strong','Obat Luar',1),(1075,'Paramex flu&batuk','Obat Luar',1),(1076,'Puyer 16','Obat Luar',1),(1077,'Primadol capl','Obat Luar',1),(1078,'Primadex syr','Obat Luar',1),(1079,'Panadol syr 30ml','Obat Luar',1),(1080,'Promag Gazero','Obat Luar',1),(1081,'Polamec syr','Obat Dalam',1),(1082,'Pyrazinamid','Obat Dalam',1),(1083,'Polamec tab','Obat Dalam',1),(1084,'Primadex F','Obat Dalam',1),(1085,'Papaverin loz','Obat Dalam',1),(1086,'Polycrol tablet','Obat Dalam',1),(1087,'Polycrol F syr','Obat Dalam',1),(1088,'Primavon tab','Obat Dalam',1),(1089,'Primavon syr','Obat Dalam',1),(1090,'Polofar plus','Obat Dalam',1),(1091,'Paracetamol 500 mg box','Obat Dalam',1),(1092,'Piroxicam 20 box INf','Obat Dalam',1),(1093,'Piroxicam 20 Graha','Obat Dalam',1),(1094,'Proris Flavoritz 60 ml','Obat Luar',1),(1095,'PEHACAIN INJ','Obat Luar',1),(1096,'pispot bab','Obat Luar',1),(1097,'PATIGON','Obat Luar',1),(1098,'PANTENE SHAMPO 90 ML','Obat Luar',1),(1099,'Pasaba syr','Obat Luar',1),(1100,'Phenobiotic','Obat Luar',1),(1101,'POLAMEC','Obat Luar',1),(1102,'PARATUSIN SYR','Obat Luar',1),(1103,'POLYSILANE SUSP 180 ML','Obat Luar',1),(1104,'Paratusin tab','Obat Luar',1),(1105,'Piroxicam 10mg','Obat Luar',1),(1106,'Phylocin DS syr','Obat Luar',1),(1107,'phytomenadion','Obat Luar',1),(1108,'Plantacid syr 100ml','Obat Luar',1),(1109,'Plantacid tab','Obat Luar',1),(1110,'Parem kocok 25 ml','Obat Luar',1),(1111,'Primperan inj','Obat Luar',1),(1112,'Pimtracol syr','Obat Luar',1),(1113,'Permen nano2','Obat Luar',1),(1114,'Purbasari ss romatic 60 ml','Obat Luar',1),(1115,'Paracetamol drop','Obat Luar',1),(1116,'Procurma syr','Obat Luar',1),(1117,'Prenatal','Obat Luar',1),(1118,'PRITAGESIC','Obat Dalam',1),(1119,'promolut','Obat Dalam',1),(1120,'Rifampicin 450 berno','Obat Dalam',1),(1121,'Remufit','Obat Dalam',1),(1122,'Renapar','Obat Dalam',1),(1123,'Ranitidin hexa','Obat Dalam',1),(1124,'Rexavin 500 mg','Obat Dalam',1),(1125,'Rifampicin 600 mg','Obat Dalam',1),(1126,'Roverton syr','Obat Dalam',1),(1127,'Ramoxyl syr','Obat Dalam',1),(1128,'Ramavek','Obat Dalam',1),(1129,'RIVANOL 300 ML','Obat Luar',1),(1130,'RANACID F SYR','Obat Dalam',1),(1131,'ROHTO','Obat Luar',1),(1132,'ROHTO COOL 10ml','Obat Luar',1),(1133,'REDOXON EFF','Obat Luar',1),(1134,'REDOXON FORTIMUN','Obat Luar',1),(1135,'RIVANOL 100ml','Obat Luar',1),(1136,'Renapar tab','Obat Luar',1),(1137,'Rapet Wangi','Obat Luar',1),(1138,'Roxidene 20mg','Obat Luar',1),(1139,'Robamox 500','Obat Luar',1),(1140,'Rohto cool 7 ml','Obat Luar',1),(1141,'Rohto tears','Obat Luar',1),(1142,'Nacl  infus','Obat Dalam',1),(1143,'Ranitidin inj','Obat Luar',1),(1144,'Radin','Obat Dalam',1),(1145,'Rhemafar','Obat Dalam',1),(1146,'Renadinac 50 mg','Obat Dalam',1),(1147,'Renabetic','Obat Dalam',1),(1148,'Roverton tab','Obat Dalam',1),(1149,'Reco TM','Obat Luar',1),(1150,'Ranitidine box dexa','Obat Dalam',1),(1151,'Ranitidine box berno','Obat Dalam',1),(1152,'RHEUMASON SPECIAL CREM SCH','Obat Luar',1),(1153,'RHEMASON WHITE CREAM SCH','Obat Luar',1),(1154,'RODECA LOTION 60 CC','Obat Luar',1),(1155,'REXAVIN 125','Obat Luar',1),(1156,'RHEUMASON WHITE CREAM','Obat Luar',1),(1157,'RHEUMASON SPESIAL CREAM','Obat Luar',1),(1158,'RHEUMASON INHALER','Obat Luar',1),(1159,'RHEUMASON SEREH','Obat Dalam',1),(1160,'RHEUMASON CLASSIC','Obat Luar',1),(1161,'RHEUMASON AROMA TERAPI','Obat Luar',1),(1162,'RHEUMASON CLASSIC BALM','Obat Luar',1),(1163,'RHEUMASON BALSEM HIJAU','Obat Luar',1),(1164,'RENADINAC','Obat Luar',1),(1165,'REJOISE SHAMPO','Obat Luar',1),(1166,'Renapar st','Obat Luar',1),(1167,'Rodeca Powder 60 gr','Obat Luar',1),(1168,'Rivanol 285ml','Obat Luar',1),(1169,'Recadryl inj','Obat Luar',1),(1170,'Saccorit','Obat Luar',1),(1171,'SIMVASTATIN 10 MG BERNO','Obat Luar',1),(1172,'Sohobion','Obat Dalam',1),(1173,'Salycil nellco biru','Obat Luar',1),(1174,'Superhoid','Obat Luar',1),(1175,'Simvastatin 10 mg yarindo','Obat Dalam',1),(1176,'Synalten cr','Obat Luar',1),(1177,'SOLVITA','Obat Dalam',1),(1178,'SANGOBION','Obat Luar',1),(1179,'SARIDON','Obat Luar',1),(1180,'STOP COLD','Obat Luar',1),(1181,'SAKATONIK ABC GRAPE','Obat Luar',1),(1182,'SAKATONIK LIVER 100ml','Obat Luar',1),(1183,'SPASMINAL box','Obat Dalam',1),(1184,'sotatic inj','Obat Luar',1),(1185,'Salonpas Pain Relief','Obat Luar',1),(1186,'Salonpas Koyo','Obat Luar',1),(1187,'SAFE Care','Obat Luar',1),(1188,'Sutra 3\'S','Obat Luar',1),(1189,'Sutra OK 12\"S','Obat Luar',1),(1190,'Super Magic','Obat Luar',1),(1191,'Sanaflu','Obat Luar',1),(1192,'Sanmol 500 mg box','Obat Dalam',1),(1193,'Strepsils Honey&lemon','Obat Luar',1),(1194,'Strepsils Reg','Obat Luar',1),(1195,'Stimuno','Obat Luar',1),(1196,'Sakatonik Abc Orange,staw','Obat Luar',1),(1197,'Simvastatin 10 box','Obat Dalam',1),(1198,'Sensodyne 25gr Ori','Obat Luar',1),(1199,'Siladex 60 Hijau','Obat Luar',1),(1200,'Siladex 60 merah','Obat Luar',1),(1201,'Siladex 60 Biru','Obat Luar',1),(1202,'Salep 88','Obat Luar',1),(1203,'Salonpas Gel','Obat Luar',1),(1204,'Salycyl talk menthol 80','Obat Luar',1),(1205,'Sarung tangan steril','Obat Luar',1),(1206,'Sarung tangan industri','Obat Luar',1),(1207,'Sanmol syr 60','Obat Luar',1),(1208,'Sanmol Drop','Obat Luar',1),(1209,'Scotts Emul 200ml','Obat Anak',1),(1210,'Sari kurma Aljazira','Obat Luar',1),(1211,'Stimuno syr','Obat Luar',1),(1212,'Synarcus cream','Obat Luar',1),(1213,'Sakatonik Liver 310ml','Obat Luar',1),(1214,'Salycill KF','Obat Luar',1),(1215,'Sutra Ok 3\'s','Obat Luar',1),(1216,'Salbutamol syr','Obat Luar',1),(1217,'Stileran','Obat Luar',1),(1218,'Supertin ','Obat Luar',1),(1219,'Stop-X 30gr','Obat Luar',1),(1220,'safety box 8 ltr','Obat Luar',1),(1221,'Stetoskop ABN','Obat Dalam',1),(1222,'Salycil talk ika','Obat Luar',1),(1223,'Speet 3 cc BD','Obat Luar',1),(1224,'Selvim','Obat Dalam',1),(1225,'Selesnizol','Obat Dalam',1),(1226,'Solinfec tab','Obat Dalam',1),(1227,'Solinfec cr','Obat Luar',1),(1228,'Scopma tab','Obat Dalam',1),(1229,'Saccarin pot','Obat Dalam',1),(1230,'Salycil talk NST','Obat Luar',1),(1231,'Sanmol 500 mg str','Obat Dalam',1),(1232,'Simvastatin 10 mg str','Obat Dalam',1),(1233,'Salbutamol 4 INF box','Obat Dalam',1),(1234,'Salbutamol 2 mg box','Obat Dalam',1),(1235,'Salep 2-4','Obat Luar',1),(1236,'Spironolactone','Obat Dalam',1),(1237,'SELTIFORT KIDS SYR','Obat Luar',1),(1238,'seinodine 15 ml','Obat Luar',1),(1239,'SPASMAL','Obat Luar',1),(1240,'SUTRA 12\"S','Obat Luar',1),(1241,'Sarung Tangan cosmo med','Obat Luar',1),(1242,'SANMAG SUSPEN. SYR','Obat Luar',1),(1243,'SIMVASTATIN 10 MG HEXA','Obat Luar',1),(1244,'Stop x 15 g','Obat Luar',1),(1245,'Sensi popok dewasa xl','Obat Luar',1),(1246,'sensi sarung tangan','Obat Luar',1),(1247,'Sanmol infus','Obat Luar',1),(1248,'Sanadryl 60cc syr','Obat Luar',1),(1249,'Supravit tab','Obat Luar',1),(1250,'Scopma plus','Obat Luar',1),(1251,'MATRIUM KLORIDA 0,9%','Obat Luar',1),(1252,'Strepsil tab','Obat Luar',1),(1253,'Sabun sirih manjakani','Obat Luar',1),(1254,'Sensi baby diaper nb12','Obat Luar',1),(1255,'Sensi baby diaper dry s12','Obat Luar',1),(1256,'Sensi baby diaper dry m 10','Obat Luar',1),(1257,'Sensi baby diaper dry l 8','Obat Luar',1),(1258,'Siladex merah syr 30ml','Obat Luar',1),(1259,'Siladex hijau syr 30 ml','Obat Luar',1),(1260,'Sun crem','Obat Luar',1),(1261,'Selesmol','Obat Luar',1),(1262,'Selesfen','Obat Luar',1),(1263,'Siladek biru syr 30 ml','Obat Luar',1),(1264,'SOHOBION INJ','Obat Dalam',1),(1265,'Spartax cap','Obat Luar',1),(1266,'Sensi baby diaper dry XL14','Obat Luar',1),(1267,'Sensi baby diaper dry XXL12','Obat Luar',1),(1268,'Scotts emuls 400 ml','Obat Luar',1),(1269,'Sensi pad alas popok L','Obat Luar',1),(1270,'Sakaneuron','Obat Dalam',1),(1271,'Salycil nellco merah','Obat Luar',1),(1272,'Sensodyne Ori 100gr','Obat Luar',1),(1273,'Tokasid','Obat Dalam',1),(1274,'Tiaryt','Obat Dalam',1),(1275,'Trifacyclin SK','Obat Dalam',1),(1276,'TRIAMINIC batuk pilek','Obat Luar',1),(1277,'TRIAMINIC EXP.','Obat Luar',1),(1278,'TRIAMINIC pilek','Obat Luar',1),(1279,'TERMOREX 30ml','Obat Luar',1),(1280,'TERMOREX 60ml','Obat Luar',1),(1281,'TERMOREX PLUS 30ml','Obat Luar',1),(1282,'TERMOREX PLUS 60ml','Obat Luar',1),(1283,'TONIKUM BAYER 100ml','Obat Luar',1),(1284,'tensi aneroid ABN','Obat Anak',1),(1285,'Trifamycetin','Obat Luar',1),(1286,'Trifamycetin skin','Obat Luar',1),(1287,'Teosal','Obat Dalam',1),(1288,'Transamin 500','Obat Dalam',1),(1289,'Tuselix syr','Obat Luar',1),(1290,'Timbangan gantung','Obat Luar',1),(1291,'Tisu nasa travel','Obat Luar',1),(1292,'Tisu best','Obat Luar',1),(1293,'Thermometer safety','Obat Luar',1),(1294,'Thermometer digital','Obat Luar',1),(1295,'Tempra drop','Obat Luar',1),(1296,'Tempra syr','Obat Luar',1),(1297,'Tolak angin anak','Obat Luar',1),(1298,'Tolak Angin Cair','Obat Luar',1),(1299,'Thrombophob Gel 20gr','Obat Luar',1),(1300,'Terramycin SM 1%','Obat Luar',1),(1301,'Tensocrape','Obat Luar',1),(1302,'Terra-cortril 5gr','Obat Luar',1),(1303,'tensi aneroid','Obat Luar',1),(1304,'Tuzalos','Obat Luar',1),(1305,'Tramadol hx','Obat Luar',1),(1306,'Tay Pin San','Obat Luar',1),(1307,'Thermolyte Plus','Obat Luar',1),(1308,'Tranec 500','Obat Luar',1),(1309,'Trianta syr','Obat Dalam',1),(1310,'Triocid syr','Obat Dalam',1),(1311,'Termagon 500 mg','Obat Dalam',1),(1312,'Termagon F','Obat Dalam',1),(1313,'Tidifar 200 mg','Obat Dalam',1),(1314,'Tera F box','Obat Dalam',1),(1315,'Trianta box','Obat Dalam',1),(1316,'Thermometer Ruang','Obat Dalam',1),(1317,'Toximed','Obat Dalam',1),(1318,'THYMCAL SYR','Obat Luar',1),(1319,'Trimakalk','Obat Luar',1),(1320,'tissu amora core [rol]','Obat Luar',1),(1321,'Timbangan Badan camry','Obat Luar',1),(1322,'tissu batik jogja n bali','Obat Luar',1),(1323,'tissu pocket','Obat Luar',1),(1324,'tissu ketupat lebaran','Obat Luar',1),(1325,'tissu leganci','Obat Luar',1),(1326,'tramadol ','Obat Luar',1),(1327,'TERMAGON TAB','Obat Luar',1),(1328,'Tifestan tab','Obat Luar',1),(1329,'Tonikum 330ml','Obat Luar',1),(1330,'Tetracyline 250mg','Obat Luar',1),(1331,'Tropicana Sweetener 2,5 grx25','Obat Luar',1),(1332,'Tropicana sweetener Diabetics 25\'s','Obat Luar',1),(1333,'Tremenza','Obat Luar',1),(1334,'Tissue super magic man','Obat Luar',1),(1335,'Tropic plus menthol 100 ml','Obat Luar',1),(1336,'Tropic plus menthol 60 ml','Obat Luar',1),(1337,'UPIXON SYR. 10ml','Obat Luar',1),(1338,'Urine wadah','Obat Luar',1),(1339,'Ultraflu','Obat Luar',1),(1340,'URINE BAG','Obat Luar',1),(1341,'URIC ACID STRIP NESCO 25\'\'S','Obat Luar',1),(1342,'ULCERANIN TAB','Obat Luar',1),(1343,'Vicks 44 dws 54ml','Obat Dewasa',1),(1344,'Vit B1 50 mg AFI','Obat Dalam',1),(1345,'Vit B12 inj Aditama','Obat Luar',1),(1346,'Viagra 100 mg','Obat Dalam',1),(1347,'Vit B Kompleks NST','Obat Dalam',1),(1348,'VICKS INHALER','Obat Luar',1),(1349,'VICKS VAPORUB 10gr','Obat Luar',1),(1350,'VISINE 6ml','Obat Luar',1),(1351,'VITAMIN A IPI','Obat Luar',1),(1352,'VITAMIN B1 IPI','Obat Luar',1),(1353,'VITAMIN B COMP. IPI','Obat Luar',1),(1354,'VITACIMIN 500mg','Obat Luar',1),(1355,'VOLTAREN GEL 5gr','Obat Luar',1),(1356,'VITAZYM box','Obat Dalam',1),(1357,'VITALONG C','Obat Luar',1),(1358,'Vegeta Herbal','Obat Luar',1),(1359,'Vegeta Jeruk','Obat Luar',1),(1360,'Vipro-G','Obat Luar',1),(1361,'Vege Blend 21Jr','Obat Luar',1),(1362,'Vidoran plus','Obat Luar',1),(1363,'VITAMIN C IPI','Obat Luar',1),(1364,'Vitalong C Btl','Obat Luar',1),(1365,'Voltadex 50 box','Obat Dalam',1),(1366,'Vicks 44 anak 54ml','Obat Luar',1),(1367,'Vicks 44 Anak 27ml','Obat Luar',1),(1368,'Vicks 44 DT 27ml','Obat Luar',1),(1369,'Vicks 44 dws 27ml','Obat Luar',1),(1370,'Veril','Obat Luar',1),(1371,'Voltaren gel 20gr','Obat Luar',1),(1372,'Voltaren Gel 10gr','Obat Luar',1),(1373,'Vitacid cream 20gr','Obat Luar',1),(1374,'Verban 10cm HAI','Obat Luar',1),(1375,'Verban 5cm HAI','Obat Luar',1),(1376,'Verban 15cm Bunda','Obat Luar',1),(1377,'Verban 10cm Bunda','Obat Luar',1),(1378,'Verban 5cm Bunda','Obat Luar',1),(1379,'vit B1 50 mg','Obat Dalam',1),(1380,'Vosea','Obat Dalam',1),(1381,'Visine extra','Obat Luar',1),(1382,'Vicks vaporub 25gr','Obat Dewasa',1),(1383,'Vitaquin','Obat Luar',1),(1384,'Vesverum syr','Obat Dalam',1),(1385,'Vosea syr','Obat Dalam',1),(1386,'Vesverum tab','Obat Dalam',1),(1387,'Vitazym str','Obat Dalam',1),(1388,'Ventolin nebules 2,5 mg','Obat Luar',1),(1389,'Vit B6 10 mg SELES','Obat Dalam',1),(1390,'Vit B complex','Obat Luar',1),(1391,'PERMEN VICK','Obat Luar',1),(1392,'VICK SASET','Obat Luar',1),(1393,'VITAMIN C YOU. 1000 ORG6','Obat Luar',1),(1394,'VICK 44 DT 54 ML','Obat Luar',1),(1395,'Vit B6 BERLICO','Obat Luar',1),(1396,'Vomitrol syr','Obat Luar',1),(1397,'Vitacurcumaris syr','Obat Luar',1),(1398,'valdres','Obat Luar',1),(1399,'Viostin DS','Obat Luar',1),(1400,'Verban 40x80 bunda','Obat Luar',1),(1401,'VIDORAN GUMMY MULTIVITAMIN','Obat Luar',1),(1402,'VIDORAN GUMMY VIT C','Obat Luar',1),(1403,'VIDORAN SMART 120 ML','Obat Luar',1),(1404,'Vitamin c 25 mg ','Obat Luar',1),(1405,'Vitamin B1 Licobevit','Obat Luar',1),(1406,'Wiatrim','Obat Dalam',1),(1407,'Wizol','Obat Dalam',1),(1408,'Woods Ant syr','Obat Luar',1),(1409,'Woods Exp syr','Obat Luar',1),(1410,'Winasal syr','Obat Dalam',1),(1411,'Winatin','Obat Dalam',1),(1412,'WALIKUKUN 30 ML','Obat Luar',1),(1413,'WALIKUKUN 60 ML','Obat Luar',1),(1414,'WIZOL','Obat Luar',1),(1415,'WHISVER','Obat Luar',1),(1416,'wood 100 ml','Obat Dalam',1),(1417,'WATER INJ 25 ML OTSU','Obat Luar',1),(1418,'Xon-Ce','Obat Luar',1),(1419,'xyltrop','Obat Luar',1),(1420,'Yefamox 500 box','Obat Dalam',1),(1421,'Y-RINS','Obat Luar',1),(1422,'You c 1000 Orange','Obat Luar',1),(1423,'Yusimox syr','Obat Dalam',1),(1424,'Yusimox 500 mg','Obat Dalam',1),(1425,'Yusimox F syr','Obat Dalam',1),(1426,'YEKAMULVITA SYR PLUS','Obat Luar',1),(1427,'Zantifar','Obat Dalam',1),(1428,'ZENIREX SYR.','Obat Luar',1),(1429,'Zevit Grow','Obat Luar',1),(1430,'Zenichlor syr','Obat Dalam',1),(1431,'Zendalat','Obat Dalam',1),(1432,'Zevask','Obat Dalam',1),(1433,'Zemoxil syr','Obat Dalam',1),(1434,'Zinc','Obat Dalam',1),(1435,'Zetamol syr','Obat Dalam',1),(1436,'Zidalev','Obat Dalam',1),(1437,'ZEVIT GROW SACHET','Obat Luar',1),(1439,'AAA','Obat Dewasa',1),(1440,'AAA','Obat Dewasa',1);
/*!40000 ALTER TABLE `ref_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_biaya`
--

DROP TABLE IF EXISTS `ref_biaya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_biaya` (
  `kode_biaya` char(5) NOT NULL,
  `keterangan_biaya` varchar(45) DEFAULT NULL,
  `kode_status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`kode_biaya`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_biaya`
--

LOCK TABLES `ref_biaya` WRITE;
/*!40000 ALTER TABLE `ref_biaya` DISABLE KEYS */;
INSERT INTO `ref_biaya` VALUES ('A001','Biaya PDAM',1),('A003','Biaya Speedy',1),('B001','Gaji Pegawai A',1),('B002','Gaji Pegawai B',1),('B003','Gaji Pegawai C',1),('C001','Biaya Bensin Motor A',1),('C002','Biaya Bensin Motor B',1),('C003','Biaya Bensin Motor C',1),('C004','Biaya Bensin Motor D',1);
/*!40000 ALTER TABLE `ref_biaya` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_harga`
--

DROP TABLE IF EXISTS `ref_harga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_harga` (
  `kode_barang` int(11) NOT NULL,
  `kode_satuan` int(11) NOT NULL,
  `harga_dasar` int(11) NOT NULL,
  UNIQUE KEY `kode_harga_unique` (`kode_barang`,`kode_satuan`),
  KEY `kode_barang_harga` (`kode_barang`),
  KEY `kode_satuan_harga` (`kode_satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_harga`
--

LOCK TABLES `ref_harga` WRITE;
/*!40000 ALTER TABLE `ref_harga` DISABLE KEYS */;
INSERT INTO `ref_harga` VALUES (1,3,9500),(2,3,5500),(3,3,9500),(4,15,3800),(5,12,12000),(6,12,5000),(7,3,3400),(8,3,5850),(9,3,100000),(10,3,3850),(11,3,11000),(12,7,3300),(13,3,12000),(14,8,55000),(15,8,37500),(16,12,2000),(17,3,8000),(18,3,3750),(19,16,8000),(20,8,19500),(21,14,200),(22,3,6500),(23,7,700),(24,7,900),(25,14,1000),(26,12,4000),(27,8,41000),(28,3,33000),(29,3,8000),(30,3,4000),(31,8,103500),(32,3,30000),(33,3,30000),(34,12,5000),(35,11,1000),(36,3,6000),(37,3,10500),(38,3,3000),(39,3,22000),(40,8,92000),(41,12,12000),(42,4,12000),(43,12,6000),(44,9,3900),(45,9,4200),(46,3,3750),(47,8,15000),(48,3,25000),(49,8,172500),(50,8,17500),(51,4,11000),(52,8,34000),(53,8,65000),(54,13,13750),(55,9,5000),(56,16,12000),(57,3,4200),(58,5,42500),(59,7,1250),(60,3,26000),(61,12,1200),(62,8,25000),(63,8,19000),(64,12,2000),(65,8,46000),(66,12,2000),(67,8,14000),(68,14,1700),(69,3,20000),(70,3,6000),(71,3,10500),(72,15,11000),(73,8,12500),(74,8,12500),(75,12,1700),(76,11,1800),(77,5,13000),(78,14,350),(79,15,3500),(80,8,19000),(81,14,5700),(82,14,5300),(83,8,99000),(84,14,1200),(85,4,5000),(86,3,24000),(87,15,11000),(88,12,4000),(89,8,26500),(90,15,4000),(91,8,45000),(92,8,15500),(93,8,35000),(94,8,25000),(95,8,31000),(96,8,35000),(97,8,58000),(98,8,12000),(99,8,14500),(100,8,7500),(101,8,200000),(102,4,14000),(103,8,55000),(104,8,16500),(105,3,9000),(106,11,1500),(107,8,21000),(108,8,47500),(109,8,20500),(110,4,30000),(111,8,18500),(112,5,77500),(113,8,17500),(114,8,52000),(115,8,14500),(116,3,12500),(117,8,28000),(118,8,29000),(119,3,30000),(120,4,5000),(121,8,52000),(122,8,30000),(123,3,51000),(124,8,0),(125,12,4500),(126,8,40000),(127,12,3500),(128,3,16500),(129,3,3500),(130,3,9000),(131,3,15500),(132,3,23000),(133,3,10000),(134,3,12000),(135,3,27000),(136,3,29500),(137,12,5000),(137,14,600),(138,12,1600),(139,10,5200),(140,10,2700),(141,3,3500),(142,15,32000),(143,5,16000),(144,4,30000),(145,11,1800),(146,11,600),(147,8,32500),(148,3,3200),(149,3,3100),(150,8,36000),(151,8,17500),(152,3,5500),(153,12,2000),(154,3,10500),(155,3,18000),(156,14,750),(157,15,12500),(158,3,6500),(159,12,10000),(160,12,7000),(161,3,22000),(162,3,22000),(163,15,20000),(164,15,4000),(165,15,7000),(166,15,12500),(167,15,2500),(168,15,10000),(169,3,4000),(170,3,6500),(171,12,1500),(172,14,1700),(173,4,8500),(174,3,23000),(175,3,9500),(176,15,8500),(177,11,800),(178,8,11000),(179,8,17000),(180,8,31000),(181,8,48000),(182,8,33000),(183,8,14000),(184,15,8500),(185,3,5000),(186,8,19500),(187,8,33000),(188,8,37500),(189,5,27000),(190,8,13000),(191,8,15000),(192,8,12000),(193,8,10000),(194,15,10500),(195,8,12500),(196,8,232000),(197,8,13000),(198,8,14000),(199,5,44000),(200,4,3000),(201,8,16500),(202,12,3000),(203,3,6500),(204,15,5500),(205,15,5500),(206,3,15500),(207,3,29500),(208,4,23500),(209,4,25500),(210,8,13000),(211,3,30000),(212,4,7500),(213,8,31000),(214,16,0),(215,5,7500),(216,15,32000),(217,12,11000),(218,3,13500),(219,8,20000),(220,15,17500),(221,3,10000),(222,3,12500),(223,3,18000),(224,3,4750),(225,16,1750),(226,8,13500),(227,15,3400),(228,8,8000),(229,12,2000),(230,3,3750),(231,15,15000),(232,3,3500),(233,3,6500),(234,8,19000),(235,8,30000),(236,9,16500),(237,15,21500),(238,8,21500),(239,5,8500),(240,8,33000),(241,12,700),(242,8,29000),(243,15,29600),(244,8,17500),(245,12,300),(246,14,5500),(247,8,20000),(248,3,27000),(249,3,4000),(250,3,14500),(251,3,11000),(252,15,22500),(253,15,9500),(254,15,33000),(255,3,2800),(256,3,5500),(257,6,6200),(258,6,5900),(259,6,8500),(260,8,3300),(261,3,15200),(262,3,31700),(263,14,100),(264,15,9500),(265,3,21500),(266,3,28700),(267,12,5000),(268,3,3750),(269,8,38000),(270,8,14500),(271,8,3300),(272,8,3700),(273,16,2300),(274,16,2500),(275,3,21000),(276,3,47500),(277,11,5000),(278,10,222000),(279,8,13000),(280,3,3100),(281,8,32000),(282,3,16500),(283,9,9000),(284,8,11500),(285,4,4500),(286,3,3800),(287,8,32500),(288,12,13000),(289,3,3500),(290,7,1000),(291,8,15000),(292,8,10000),(293,8,25000),(294,8,17000),(295,8,67000),(296,4,8700),(297,4,12500),(298,15,17000),(299,15,19000),(300,3,4200),(301,3,5500),(302,4,4000),(303,4,15000),(304,11,1250000),(305,4,7500),(306,12,4000),(307,3,20500),(308,4,4600),(309,3,2500),(310,3,43500),(311,8,17500),(312,8,14000),(313,12,10000),(314,4,145000),(315,3,50000),(316,8,75000),(317,3,36500),(318,4,15000),(319,9,1250),(320,12,1500),(321,3,22000),(322,3,15000),(323,4,7500),(324,3,12500),(325,3,9000),(326,12,2300),(327,3,16000),(328,3,5500),(329,3,12500),(330,14,2000),(331,12,2750),(332,12,1500),(333,8,20000),(334,8,32500),(335,8,42500),(336,12,5000),(337,8,39000),(338,13,17000),(339,9,6500),(340,10,12000),(341,8,37500),(342,8,18500),(343,12,1500),(344,12,4500),(345,12,1500),(346,8,19000),(347,8,0),(348,4,3000),(349,15,30000),(350,15,17500),(351,3,18400),(352,5,26000),(352,8,32000),(353,15,8500),(354,15,17500),(355,12,11000),(356,5,26000),(356,8,32000),(357,8,57000),(358,8,12500),(359,8,7500),(360,8,11800),(361,15,4000),(362,8,22000),(363,15,135000),(364,8,18000),(365,3,4000),(366,8,21500),(367,8,10000),(368,17,2300),(369,8,11000),(370,8,11000),(371,8,9500),(372,8,52000),(373,8,16500),(374,3,5800),(375,8,22000),(376,13,12500),(377,12,1600),(378,7,570),(379,8,21000),(380,8,36000),(381,9,7250),(382,3,3300),(383,4,108000),(384,4,7500),(385,12,6000),(386,8,15000),(387,3,17500),(388,8,99000),(389,15,19500),(390,4,10000),(391,4,10000),(392,8,0),(393,8,10500),(394,3,5000),(395,12,5000),(396,3,3500),(397,3,36000),(398,15,3500),(399,15,4000),(400,3,17500),(401,4,800),(402,3,14000),(403,8,7500),(404,12,5000),(405,7,1500),(406,7,2250),(407,3,3600),(408,8,75000),(409,8,75000),(410,8,32500),(411,8,12500),(412,8,50000),(413,4,139000),(414,8,18000),(415,3,3500),(416,8,11000),(417,3,3200),(418,3,3850),(419,8,25000),(420,5,56000),(421,3,5750),(422,15,44300),(423,15,4000),(424,8,11500),(425,8,32500),(426,4,12500),(427,4,12500),(428,4,23000),(429,3,22500),(430,12,3000),(431,3,21000),(432,4,22000),(433,15,0),(434,8,23600),(435,4,25000),(436,8,0),(437,3,30000),(438,3,11500),(439,8,18000),(440,8,21000),(441,3,4250),(442,8,37000),(443,3,11000),(444,8,30500),(445,3,6250),(446,8,19500),(447,15,12000),(448,3,10000),(449,4,7000),(450,12,3500),(451,12,2000),(452,8,15800),(453,14,750),(454,14,1200),(455,12,2800),(456,8,51000),(457,3,11200),(458,12,4700),(459,3,11500),(460,8,14500),(461,3,3750),(462,3,3750),(463,3,3000),(464,8,34000),(464,12,5000),(465,8,12500),(466,8,10000),(467,8,17500),(468,8,11500),(469,8,33500),(470,8,15000),(471,8,13000),(472,8,15500),(473,8,16000),(474,8,29500),(475,8,19000),(476,8,12000),(477,8,15000),(478,8,19000),(479,3,4500),(480,8,20000),(481,8,19000),(482,3,7250),(483,3,3350),(484,8,12500),(485,8,236000),(486,8,206000),(487,3,4250),(488,8,34000),(488,12,5000),(489,12,25000),(490,8,33500),(491,12,2500),(492,3,7000),(493,8,58000),(494,4,8000),(495,12,2500),(496,3,5500),(497,8,6500),(498,12,0),(499,4,6500),(500,10,3500),(501,10,6000),(502,10,11500),(503,8,13750),(504,3,4700),(505,3,2850),(506,12,2500),(507,8,36000),(508,8,15500),(509,8,24000),(510,9,2500),(511,3,6000),(512,15,4850),(513,3,10000),(514,3,11000),(515,3,11000),(516,15,3100),(517,3,1950),(518,15,2500),(519,14,250),(520,7,1500),(521,8,19500),(522,3,6500),(523,8,15500),(523,12,3000),(524,8,18500),(525,8,33000),(526,7,1300),(527,14,1000),(528,3,13500),(529,15,2400),(530,8,9000),(531,8,10000),(532,8,19000),(533,8,23000),(534,15,3750),(535,3,5500),(536,15,3000),(537,3,4400),(538,1,4200),(539,8,13000),(540,8,13000),(541,8,27000),(542,8,10000),(543,8,13000),(544,8,19500),(545,8,21000),(546,8,27500),(547,8,26500),(548,4,110000),(549,4,475000),(550,8,8000),(551,3,8000),(552,8,15500),(552,12,3000),(553,8,90000),(554,3,3000),(555,8,17500),(556,3,6500),(557,8,70000),(558,8,26500),(559,12,6000),(560,3,11000),(561,3,12500),(562,3,10000),(563,3,7200),(564,8,20000),(565,3,3500),(566,8,18500),(567,4,2000),(568,8,37000),(569,3,3500),(570,8,12500),(571,8,28500),(572,8,0),(573,3,10000),(574,12,750),(575,5,6500),(576,5,10300),(577,12,250),(578,16,2500),(579,12,11500),(580,15,3500),(581,3,28000),(582,3,27500),(583,3,33000),(584,3,24000),(585,3,33000),(586,3,20000),(587,8,75000),(588,3,33000),(589,15,40000),(590,8,33000),(591,7,1000),(592,8,23000),(593,4,5000),(594,3,35000),(595,3,11000),(596,8,11000),(597,3,11500),(598,4,5000),(599,8,13000),(600,8,18000),(601,3,3300),(602,8,28000),(603,8,35000),(604,3,3550),(605,3,3500),(606,4,500),(607,3,9200),(608,5,4700),(609,4,7500),(610,12,9000),(611,3,3350),(612,4,7500),(613,3,5000),(614,3,4500),(615,12,3500),(616,15,3500),(617,8,11000),(618,3,3500),(619,8,38000),(620,8,33000),(621,8,10000),(622,3,26000),(623,3,3000),(624,8,16500),(625,3,18000),(626,3,10500),(627,3,14000),(628,12,3000),(629,8,33000),(630,8,22500),(631,8,66000),(632,3,10000),(633,12,1000),(634,12,1750),(635,3,17000),(636,15,3000),(637,8,13500),(638,8,11000),(639,12,5000),(640,4,4500),(641,3,3000),(642,3,43900),(643,15,25500),(644,12,1375),(645,4,4000),(646,12,1750),(647,3,3850),(648,15,4500),(649,8,15500),(650,8,32000),(651,8,28000),(652,8,17500),(653,3,3500),(654,12,1500),(655,8,66000),(656,3,5000),(657,4,10000),(658,8,23000),(659,4,5000),(660,8,23000),(661,3,35000),(662,8,63500),(663,4,3000),(664,3,55000),(665,10,29000),(666,8,134000),(667,3,7000),(668,14,6000),(669,12,75300),(670,14,4000),(671,3,35500),(672,14,2000),(673,4,7500),(674,15,12200),(675,3,5000),(676,3,8600),(677,3,6000),(678,4,3700),(679,3,8000),(680,4,1200),(681,3,16500),(682,3,13000),(683,8,135000),(684,12,30000),(685,9,14500),(686,3,36000),(687,15,14000),(688,3,3000),(689,12,2000),(690,3,6500),(691,4,600),(692,3,1500),(693,4,700),(694,14,100),(695,14,3000),(696,8,79000),(697,3,4500),(698,3,4500),(699,8,3500),(700,3,3300),(701,15,7800),(702,3,7900),(703,15,39000),(704,3,2300),(705,15,6500),(706,16,32000),(707,16,6500),(708,16,3250),(709,16,1500),(710,8,3700),(711,8,2500),(712,16,16000),(713,8,4000),(714,11,600),(715,3,4900),(716,15,6500),(717,8,80000),(718,4,6000),(719,4,4000),(720,8,17500),(721,4,2000),(722,16,95000),(723,4,3000),(724,8,24000),(725,15,6500),(726,4,7500),(727,8,22000),(728,16,5000),(729,12,4750),(730,4,7647),(731,4,0),(732,7,5000),(733,12,2500),(734,8,21500),(735,8,14500),(736,8,22500),(737,3,21500),(738,8,24000),(739,8,35000),(740,8,20000),(741,10,30000),(742,8,17000),(743,8,51000),(744,8,27500),(745,3,4000),(746,3,6500),(747,3,10200),(748,3,4000),(749,3,7500),(750,3,3400),(751,11,4500),(752,12,5500),(753,3,17400),(754,3,7500),(755,3,7000),(756,3,7000),(757,3,3000),(758,5,5000),(759,5,5000),(760,3,36500),(761,8,3600),(762,7,2600),(763,7,529500),(764,14,1500),(765,8,45000),(766,8,20000),(767,8,25000),(768,14,200),(769,3,3800),(770,3,7000),(771,8,19000),(772,8,20000),(773,8,39000),(774,8,29500),(775,3,4100),(776,12,5000),(777,8,9500),(778,9,1500),(779,8,32500),(780,8,19000),(781,3,3650),(782,8,14000),(783,8,115000),(784,16,9000),(785,16,15000),(786,8,32500),(787,16,30000),(788,4,117000),(789,12,5000),(790,8,23000),(791,16,42500),(792,12,7500),(793,15,4500),(794,3,7200),(795,3,6000),(796,8,18000),(797,8,15000),(798,4,22000),(799,4,21500),(800,15,26500),(801,3,21000),(802,3,4000),(803,3,7500),(804,3,14000),(805,3,24000),(806,3,4500),(807,3,8000),(808,3,21000),(809,3,13000),(810,3,7500),(811,12,4000),(812,8,45000),(813,8,15500),(814,8,14500),(815,8,10000),(816,8,10000),(817,15,17000),(818,15,4500),(819,8,16500),(820,3,3400),(821,7,800),(822,3,4100),(823,3,7500),(824,3,12500),(825,3,19000),(826,3,11000),(827,3,11000),(828,3,13000),(829,3,12500),(830,3,28000),(831,3,16500),(832,12,1300),(833,12,1500),(834,12,1500),(835,12,5000),(836,12,5000),(837,4,1750),(838,8,26000),(839,8,194772),(839,14,4500),(840,8,130000),(840,12,14000),(841,12,10000),(842,8,42000),(843,3,3100),(844,3,30600),(845,3,10000),(846,15,28000),(847,3,2100),(848,15,17000),(849,15,9500),(850,4,1000),(851,4,1000),(852,8,36000),(853,14,4500),(854,11,1500),(855,15,11000),(856,15,10500),(857,15,4000),(858,8,33500),(859,8,29000),(860,8,34000),(861,15,3750),(862,8,18000),(863,12,6000),(864,8,55000),(865,8,54000),(866,8,35000),(867,3,3300),(868,3,26000),(869,3,37000),(870,15,19000),(871,15,34000),(872,15,6000),(873,8,130000),(873,12,14000),(874,4,287500),(875,3,6500),(876,3,18000),(877,4,12000),(878,4,6500),(879,4,3500),(880,4,12500),(881,4,7000),(882,4,3500),(883,12,2000),(884,3,55000),(885,4,10000),(886,8,19500),(887,12,4000),(888,3,15000),(889,3,18000),(890,3,35000),(891,3,3500),(892,8,194772),(892,14,4500),(893,3,27000),(894,7,1600),(895,12,13000),(896,12,5000),(897,15,0),(898,12,0),(899,9,5000),(900,8,35000),(901,14,500),(902,12,7500),(903,8,35000),(904,14,1160),(905,8,30000),(906,3,2800),(907,12,2500),(908,14,115),(909,8,17000),(910,5,26500),(911,12,4500),(912,12,2000),(913,14,10000),(914,11,2000),(915,8,11000),(916,12,2000),(917,12,2000),(918,8,55000),(919,14,500),(920,15,9100),(921,11,11000),(922,14,250),(923,8,95000),(924,14,2200),(925,14,8000),(926,8,60000),(927,15,11200),(928,3,12500),(929,15,13500),(930,4,21000),(931,8,28500),(932,3,2750),(933,3,2500),(934,15,3750),(935,8,29000),(935,12,4000),(936,14,420),(937,8,13000),(938,5,52000),(939,8,18000),(940,8,45000),(941,8,12000),(942,12,4000),(943,8,35000),(944,8,45000),(945,8,35000),(946,8,29000),(946,12,4000),(947,4,9000),(948,8,19000),(949,15,6500),(950,8,78500),(951,8,0),(952,15,5250),(953,9,0),(954,3,0),(955,3,3750),(956,8,31000),(957,8,12000),(958,3,3000),(959,15,3000),(960,3,10000),(961,3,7500),(962,3,10000),(963,8,40000),(964,3,19000),(965,12,1500),(966,12,1500),(967,12,500),(968,8,14000),(969,8,14000),(970,8,12500),(971,8,35000),(971,14,3500),(972,3,14000),(973,3,8000),(974,3,3150),(975,8,22000),(976,8,35000),(976,14,3500),(977,12,5000),(978,8,28500),(979,3,3000),(980,4,8500),(981,9,2500),(982,8,34000),(983,3,9500),(984,3,8000),(985,3,3200),(986,3,3300),(987,3,3300),(988,8,17000),(989,8,37500),(990,8,25000),(991,8,10000),(991,12,2000),(992,15,3000),(993,3,3250),(994,8,16000),(995,8,13000),(996,8,27000),(997,8,16000),(998,7,1000),(999,8,29000),(1000,12,2500),(1001,12,2500),(1002,8,16000),(1003,3,14000),(1004,3,5500),(1005,3,7500),(1006,3,11000),(1007,3,11000),(1008,3,13500),(1009,3,5000),(1010,3,3250),(1011,3,8000),(1012,8,10000),(1012,12,2000),(1013,8,30500),(1014,16,23000),(1015,3,4500),(1016,3,125400),(1017,16,196000),(1018,3,13500),(1019,3,10000),(1020,3,7500),(1021,15,22000),(1022,8,19500),(1023,8,0),(1024,15,3200),(1025,8,50000),(1026,8,20000),(1027,10,87000),(1028,12,6000),(1029,8,12500),(1030,8,30000),(1031,14,600),(1032,14,7000),(1033,14,6000),(1034,14,800),(1035,7,3500),(1036,3,5000),(1037,14,70000),(1038,8,0),(1039,12,2500),(1040,12,7500),(1041,12,2500),(1042,12,1000),(1043,8,47500),(1044,12,1250),(1045,8,32500),(1046,14,5000),(1047,12,1000),(1048,15,25000),(1049,14,2500),(1050,14,900),(1051,14,2500),(1052,3,24500),(1053,3,4700),(1054,3,5900),(1055,3,57000),(1056,3,19000),(1057,3,18000),(1058,8,5000),(1059,15,3000),(1060,15,40000),(1061,11,1000),(1062,4,7000),(1063,8,19000),(1064,10,4000),(1065,3,27800),(1066,3,20000),(1067,3,24500),(1068,3,13000),(1069,12,3250),(1070,4,43000),(1071,15,3500),(1072,5,2500),(1073,3,3000),(1074,11,3000),(1075,12,1800),(1076,11,350),(1077,8,1500),(1078,3,3800),(1079,3,16000),(1080,11,1500),(1081,3,4800),(1082,8,26000),(1083,8,11000),(1084,8,34500),(1085,5,99500),(1086,12,4500),(1087,3,29500),(1088,8,19000),(1089,3,3850),(1090,8,15500),(1091,8,12500),(1092,8,15000),(1093,8,15000),(1094,3,30000),(1095,9,3500),(1096,4,30500),(1097,12,0),(1098,3,8000),(1099,3,3250),(1100,8,57500),(1101,12,2000),(1102,3,18000),(1103,3,32500),(1104,12,7500),(1105,8,11000),(1106,3,0),(1107,1,2500),(1108,3,6500),(1109,8,12000),(1110,3,5500),(1111,9,11500),(1112,3,6500),(1113,4,5000),(1114,3,5500),(1115,3,6500),(1116,3,3650),(1117,8,0),(1118,8,36500),(1119,14,1889),(1120,8,73000),(1121,11,2500),(1122,8,230000),(1123,8,16500),(1124,8,79000),(1125,8,91000),(1126,3,3250),(1127,3,3200),(1128,8,33000),(1129,16,5000),(1130,16,3200),(1131,3,8400),(1132,3,12000),(1133,8,32000),(1134,8,32000),(1135,3,2000),(1136,14,3000),(1137,8,5000),(1138,8,60000),(1139,8,75000),(1140,3,12000),(1141,3,9000),(1142,4,6500),(1143,1,3250),(1144,8,11500),(1145,8,42000),(1146,8,15500),(1147,8,10000),(1148,8,15000),(1149,3,5300),(1150,8,16500),(1151,8,16500),(1152,15,4500),(1153,15,4500),(1154,3,7500),(1155,8,26000),(1156,4,4500),(1157,4,4500),(1158,16,5500),(1159,16,4500),(1160,16,4500),(1161,3,9000),(1162,3,4500),(1163,3,4500),(1164,12,5000),(1165,3,6500),(1166,12,23500),(1167,4,6000),(1168,3,5500),(1169,9,4000),(1170,8,7300),(1171,8,7000),(1172,17,10000),(1173,3,3500),(1174,8,19000),(1175,8,16000),(1176,15,3600),(1177,3,3500),(1178,7,1000),(1179,12,3545),(1180,12,2500),(1181,3,11000),(1182,3,10000),(1183,8,48500),(1184,1,4300),(1185,3,15800),(1186,4,9500),(1187,3,15500),(1188,4,3500),(1189,4,15000),(1190,4,6600),(1191,12,1500),(1192,8,26000),(1193,12,5500),(1194,12,5500),(1195,7,2500),(1196,3,11000),(1197,8,16000),(1198,4,5500),(1199,3,10500),(1200,3,10000),(1201,3,10000),(1202,15,5500),(1203,15,7500),(1204,4,7500),(1205,8,57000),(1206,16,13000),(1207,3,12000),(1208,3,16000),(1209,3,24500),(1210,3,20000),(1211,3,22000),(1212,15,2750),(1213,3,17500),(1214,10,4200),(1215,4,4000),(1216,3,6600),(1217,8,105000),(1218,7,1100),(1219,15,19000),(1220,4,20500),(1221,4,69000),(1222,3,8500),(1223,4,850),(1224,8,34000),(1225,8,22000),(1226,8,31000),(1227,15,4500),(1228,8,48000),(1229,3,11000),(1230,4,1600),(1231,12,1500),(1232,12,6000),(1233,8,12000),(1234,8,10500),(1235,15,3000),(1236,8,45000),(1237,3,3500),(1238,4,2500),(1239,8,67000),(1240,8,11500),(1241,8,42500),(1242,3,26000),(1243,8,16000),(1244,15,12500),(1245,4,11000),(1246,8,23000),(1247,3,62500),(1248,3,11000),(1249,12,3500),(1250,8,50000),(1251,3,5600),(1252,14,1000),(1253,3,7000),(1254,4,22500),(1255,4,18000),(1256,4,18000),(1257,4,18000),(1258,3,6500),(1259,3,6500),(1260,3,24000),(1261,8,14000),(1262,8,16000),(1263,16,0),(1264,16,6500),(1265,8,26500),(1266,4,0),(1267,4,0),(1268,3,35500),(1269,4,30000),(1270,8,29000),(1271,4,3650),(1272,4,18500),(1273,8,28500),(1274,8,138000),(1275,15,2850),(1276,3,33700),(1277,3,35600),(1278,3,35000),(1279,3,7000),(1280,3,11000),(1281,3,7500),(1282,3,11000),(1283,3,13500),(1284,4,212000),(1285,15,0),(1286,15,0),(1287,8,15500),(1288,12,25000),(1289,3,4500),(1290,4,69000),(1291,8,2500),(1292,4,800),(1293,4,10000),(1294,4,24000),(1295,3,37000),(1296,3,33700),(1297,11,1200),(1298,11,1700),(1299,15,50000),(1300,15,10000),(1301,16,55000),(1302,15,19000),(1303,4,78000),(1304,12,3500),(1305,8,18500),(1306,11,1500),(1307,14,6600),(1308,8,142000),(1309,3,3300),(1310,3,3350),(1311,8,16000),(1312,8,18500),(1313,8,18000),(1314,8,21000),(1315,8,14500),(1316,4,11500),(1317,8,145000),(1318,3,5600),(1319,10,48500),(1320,4,2500),(1321,4,9000),(1322,8,9000),(1323,4,1000),(1324,8,8000),(1325,8,7000),(1326,12,5000),(1327,12,3000),(1328,8,16500),(1329,3,25500),(1330,8,30000),(1331,8,17500),(1332,8,19000),(1333,8,105600),(1334,4,6500),(1335,3,12000),(1336,3,9000),(1337,3,10300),(1338,4,7000),(1339,12,2500),(1340,4,7000),(1341,8,103500),(1342,12,4000),(1343,3,11500),(1344,5,26000),(1345,9,3200),(1346,8,505000),(1347,5,19000),(1348,15,11000),(1349,10,6500),(1350,3,10000),(1351,3,3500),(1352,3,3500),(1353,3,3500),(1354,12,1500),(1355,15,19000),(1356,8,37500),(1357,12,4500),(1358,12,2000),(1359,12,1500),(1360,12,7000),(1361,7,99000),(1362,3,15000),(1363,3,3500),(1364,3,29500),(1365,8,16500),(1366,3,12000),(1367,3,6500),(1368,3,5500),(1369,3,6500),(1370,15,14500),(1371,15,49000),(1372,15,32000),(1373,15,33000),(1374,16,2200),(1375,16,1000),(1376,16,1500),(1377,16,1200),(1378,16,500),(1379,10,37000),(1380,8,13500),(1381,3,11500),(1382,10,11700),(1383,15,58000),(1384,3,3400),(1385,3,3500),(1386,8,21000),(1387,12,4000),(1388,4,10000),(1389,5,22000),(1390,10,23000),(1391,2,200),(1392,11,1000),(1393,3,5500),(1394,3,11000),(1395,5,24500),(1396,3,6000),(1397,3,3500),(1398,8,55000),(1399,15,6500),(1400,16,149500),(1401,4,14500),(1402,4,13000),(1403,3,15000),(1404,10,22000),(1405,10,46000),(1406,8,19000),(1407,8,69000),(1407,12,10000),(1408,3,15000),(1409,3,15000),(1410,3,3500),(1411,8,25000),(1412,3,8750),(1413,3,14500),(1414,8,69000),(1414,12,10000),(1415,4,6500),(1416,3,25000),(1417,9,3000),(1418,12,1200),(1419,8,39000),(1420,8,32000),(1421,3,31000),(1422,3,5000),(1423,3,3600),(1424,8,33000),(1425,3,4950),(1426,3,3800),(1427,8,11000),(1428,3,3650),(1429,7,1350),(1430,3,5300),(1431,8,29000),(1432,8,17500),(1433,3,3600),(1434,8,54500),(1435,3,3000),(1436,8,21000),(1437,11,1000),(1439,7,2000),(1440,12,3000);
/*!40000 ALTER TABLE `ref_harga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_satuan`
--

DROP TABLE IF EXISTS `ref_satuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_satuan` (
  `kode_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_satuan` varchar(45) DEFAULT NULL,
  `jumlah_satuan` int(11) NOT NULL DEFAULT '1',
  `kode_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`kode_satuan`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_satuan`
--

LOCK TABLES `ref_satuan` WRITE;
/*!40000 ALTER TABLE `ref_satuan` DISABLE KEYS */;
INSERT INTO `ref_satuan` VALUES (1,'Ampul',1,1),(2,'Biji',1,1),(3,'Botol',1,1),(4,'Item',1,1),(5,'Kaleng',1,1),(6,'Kantong',1,1),(7,'Kapsul',1,1),(8,'Kotak',1,1),(9,'Pail',1,1),(10,'Pot',1,1),(11,'Sachet',1,1),(12,'Strip',1,1),(13,'Supp',1,1),(14,'Tablet',1,1),(15,'Tube',1,1),(16,'Unit',1,1),(17,'Vial',1,1);
/*!40000 ALTER TABLE `ref_satuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `rinci_stok`
--

DROP TABLE IF EXISTS `rinci_stok`;
/*!50001 DROP VIEW IF EXISTS `rinci_stok`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `rinci_stok` (
  `Kode PO` bigint(20),
  `Perolehan` date,
  `Penjualan` date,
  `Kode Barang` mediumtext,
  `Nama Barang` varchar(45),
  `Jenis` enum('Obat Dewasa','Obat Anak','Obat Dalam','Obat Luar'),
  `Satuan` varchar(45),
  `Jumlah Perolehan` int(11),
  `Jumlah Penjualan` int(11),
  `Stok` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tabel_aplikasi`
--

DROP TABLE IF EXISTS `tabel_aplikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_aplikasi` (
  `appl_kode` char(6) NOT NULL,
  `ga_kode` char(6) DEFAULT NULL,
  `appl_seq` tinyint(3) unsigned DEFAULT NULL,
  `appl_file` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `appl_proc` varchar(50) DEFAULT NULL,
  `appl_nama` varchar(50) DEFAULT NULL,
  `appl_deskripsi` varchar(50) DEFAULT NULL,
  `appl_sts` tinyint(4) DEFAULT NULL,
  `appl_ref` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`appl_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_aplikasi`
--

LOCK TABLES `tabel_aplikasi` WRITE;
/*!40000 ALTER TABLE `tabel_aplikasi` DISABLE KEYS */;
INSERT INTO `tabel_aplikasi` VALUES ('010000','000000',1,'0','0','File','0',0,0),('010101','010000',5,'stream.php','0','Data Stream','0',0,0),('010201','010000',1,'ganti_password.php','ganti_password.php','Ganti Password','0',0,0),('020000','000000',2,'0','0','Opname','0',0,0),('020101','020000',1,'modul/020101_view.php','modul/020101_proc.php','Validasi Stok','0',0,0),('020300','010000',3,'0','0','Referensi','0',0,0),('020301','020300',1,'modul/020301_view.php','modul/020301_proc.php','Referensi Barang','0',0,0),('020302','020300',2,'modul/020302_view.php','modul/020302_proc.php','Referensi Biaya','0',0,0),('030000','000000',3,'0','0','Transaksi','0',0,0),('030100','030000',5,'0','0','Pembelian','0',0,0),('030101','030100',1,'modul/030101_view.php','modul/030101_proc.php','Pembelian Umum','0',0,0),('030200','030000',2,'0','0','Penjualan','0',0,0),('030201','030200',1,'modul/030201_view.php','modul/030201_proc.php','Penjualan Umum','0',0,0),('030202','030200',2,'modul/030201_view.php','modul/030201_proc.php','Penjualan Pelanggan','0',0,0),('030300','030000',3,'0','0','Pembayaran','0',0,0),('030301','030300',1,'modul/030301_view.php','modul/030301_proc.php','Drop Order','0',0,0),('030302','030300',2,'modul/030302_view.php','modul/030302_proc.php','Purchase Order','0',0,0),('030303','030300',3,'modul/030303_view.php','modul/030303_proc.php','Biaya Operasional','0',0,0),('050000','000000',5,'0','0','Laporan','0',0,0),('050100','050000',1,'0','0','Pembelian','0',0,0),('050101','050100',1,'modul/050101_view.php','0','Harian','Laporan Pembelian Harian',0,0),('050102','050100',2,'modul/050102_view.php','0','Bulanan','Laporan Pembelian Bulanan',0,0),('050200','050000',2,'0','0','Penjualan','0',0,0),('050201','050200',1,'modul/050201_view.php','0','Harian','Laporan Penjualan Harian',0,0),('050300','050000',3,'0','0','Biaya Operasional','0',0,0),('050400','050000',4,'0','0','Laba','0',0,0);
/*!40000 ALTER TABLE `tabel_aplikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabel_biaya`
--

DROP TABLE IF EXISTS `tabel_biaya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_biaya` (
  `kode_transaksi` bigint(20) NOT NULL DEFAULT '3000',
  `kode_biaya` char(5) NOT NULL DEFAULT '00000',
  `jumlah_biaya` int(11) NOT NULL DEFAULT '0',
  `kode_status` tinyint(4) NOT NULL DEFAULT '1',
  `kar_id` varchar(20) NOT NULL DEFAULT '''auto''',
  `keterangan` varchar(45) NOT NULL,
  PRIMARY KEY (`kode_transaksi`),
  KEY `kode_biaya` (`kode_biaya`),
  CONSTRAINT `kode_biaya` FOREIGN KEY (`kode_biaya`) REFERENCES `ref_biaya` (`kode_biaya`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_biaya`
--

LOCK TABLES `tabel_biaya` WRITE;
/*!40000 ALTER TABLE `tabel_biaya` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabel_biaya` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_kode_biaya` BEFORE INSERT ON `tabel_biaya` FOR EACH ROW SET New.kode_transaksi=CONCAT(UNIX_TIMESTAMP(NOW()),New.kode_transaksi) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_cabang`
--

DROP TABLE IF EXISTS `tabel_cabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_cabang` (
  `cab_kode` char(2) NOT NULL,
  `cab_ket` varchar(50) NOT NULL,
  `cab_ip` int(11) NOT NULL,
  PRIMARY KEY (`cab_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_cabang`
--

LOCK TABLES `tabel_cabang` WRITE;
/*!40000 ALTER TABLE `tabel_cabang` DISABLE KEYS */;
/*!40000 ALTER TABLE `tabel_cabang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabel_grup`
--

DROP TABLE IF EXISTS `tabel_grup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_grup` (
  `grup_id` varchar(10) NOT NULL,
  `grup_ket` varchar(70) DEFAULT NULL,
  `grup_nama` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`grup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_grup`
--

LOCK TABLES `tabel_grup` WRITE;
/*!40000 ALTER TABLE `tabel_grup` DISABLE KEYS */;
INSERT INTO `tabel_grup` VALUES ('000','Administrator','Administrator'),('002','Development','Development'),('003','Pembelian','Pembelian'),('004','Penjualan','Penjualan');
/*!40000 ALTER TABLE `tabel_grup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabel_grup_appl`
--

DROP TABLE IF EXISTS `tabel_grup_appl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_grup_appl` (
  `appl_kode` char(6) NOT NULL,
  `grup_id` varchar(10) NOT NULL,
  `sts` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`appl_kode`,`grup_id`),
  KEY `grup_appl` (`grup_id`),
  CONSTRAINT `grup_appl` FOREIGN KEY (`grup_id`) REFERENCES `tabel_grup` (`grup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_grup_appl`
--

LOCK TABLES `tabel_grup_appl` WRITE;
/*!40000 ALTER TABLE `tabel_grup_appl` DISABLE KEYS */;
INSERT INTO `tabel_grup_appl` VALUES ('010000','003',1),('010000','004',1),('010101','003',1),('010101','004',1),('010201','003',1),('010201','004',1),('020000','003',1),('020101','003',1),('020300','003',1),('020301','003',1),('030000','003',1),('030000','004',1),('030100','003',1),('030100','004',1),('030101','003',1),('030101','004',1),('030200','004',1),('030201','004',1),('030202','004',1);
/*!40000 ALTER TABLE `tabel_grup_appl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `tabel_pelanggan`
--

DROP TABLE IF EXISTS `tabel_pelanggan`;
/*!50001 DROP VIEW IF EXISTS `tabel_pelanggan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tabel_pelanggan` (
  `kode_pelanggan` char(6),
  `contact_person` varchar(45),
  `nama` varchar(45),
  `alamat` varchar(90),
  `kota` varchar(45),
  `email` varchar(45),
  `phone_number` varchar(45),
  `fax_number` varchar(45),
  `kode_pos` char(5),
  `kode_status` tinyint(4)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tabel_pembayaran`
--

DROP TABLE IF EXISTS `tabel_pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_pembayaran` (
  `byr_no` bigint(20) unsigned NOT NULL DEFAULT '0',
  `byr_tgl` datetime NOT NULL,
  `byr_serial` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_do` bigint(20) NOT NULL DEFAULT '0',
  `kar_id` varchar(20) NOT NULL DEFAULT '0',
  `lok_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `byr_loket` char(1) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0',
  `byr_total` bigint(10) unsigned NOT NULL DEFAULT '0',
  `byr_kembali` int(11) NOT NULL,
  `byr_cetak` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `byr_upd_sts` datetime NOT NULL,
  `byr_sts` tinyint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`byr_serial`),
  KEY `TANGGAL` (`byr_tgl`),
  KEY `bayar_kasir` (`kar_id`),
  KEY `bayar_kode_do` (`kode_do`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_pembayaran`
--

LOCK TABLES `tabel_pembayaran` WRITE;
/*!40000 ALTER TABLE `tabel_pembayaran` DISABLE KEYS */;
INSERT INTO `tabel_pembayaran` VALUES (13881951505470,'2013-12-28 08:46:16',1,13881948941000,'admin','10.8.0.5','B',100000,0,1,'2013-12-28 08:46:16',1),(13881952232000,'2013-12-28 08:47:31',2,13881952232000,'admin','10.8.0.5','P',20000,0,1,'2013-12-28 08:47:31',1),(13881952652000,'2013-12-28 08:48:55',3,13881952512000,'admin','10.8.0.5','P',10000,0,1,'2013-12-28 08:48:55',1),(13882020840570,'2013-12-28 10:44:19',4,13881952512000,'admin','10.8.0.5','A',5000,0,2,'2013-12-28 10:44:19',1),(13882020840570,'2013-12-28 10:45:21',5,13881952512000,'admin','10.8.0.5','A',5000,5000,3,'2013-12-28 10:45:21',1),(13882026652000,'2013-12-28 10:51:24',6,13882018652000,'admin','10.8.0.5','P',100000,0,1,'2013-12-28 10:51:24',1),(14087568712000,'2014-08-23 08:21:24',7,13882026842000,'admin','10.8.0.5','P',10000,90000,1,'2014-08-23 08:21:24',1);
/*!40000 ALTER TABLE `tabel_pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabel_pembelian`
--

DROP TABLE IF EXISTS `tabel_pembelian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_pembelian` (
  `kode_pembelian` bigint(20) NOT NULL DEFAULT '1000',
  `kode_po` bigint(20) NOT NULL,
  `kode_barang` int(11) NOT NULL DEFAULT '0',
  `jumlah_pembelian` int(11) NOT NULL DEFAULT '0',
  `kode_satuan` int(11) NOT NULL DEFAULT '0',
  `harga` int(11) NOT NULL DEFAULT '0',
  `jumlah_penjualan` int(11) NOT NULL DEFAULT '0',
  `jumlah_stok` int(11) NOT NULL DEFAULT '0',
  `kar_id` varchar(20) NOT NULL DEFAULT 'auto',
  `kode_status` tinyint(4) NOT NULL DEFAULT '3',
  `keterangan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kode_pembelian`),
  KEY `kode_barang_beli` (`kode_barang`),
  KEY `kode_satuan_beli` (`kode_satuan`),
  KEY `kode_po` (`kode_po`),
  CONSTRAINT `kode_barang_beli` FOREIGN KEY (`kode_barang`) REFERENCES `ref_barang` (`kode_barang`) ON UPDATE CASCADE,
  CONSTRAINT `kode_po` FOREIGN KEY (`kode_po`) REFERENCES `tabel_po` (`kode_po`) ON UPDATE CASCADE,
  CONSTRAINT `kode_satuan_beli` FOREIGN KEY (`kode_satuan`) REFERENCES `ref_satuan` (`kode_satuan`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_pembelian`
--

LOCK TABLES `tabel_pembelian` WRITE;
/*!40000 ALTER TABLE `tabel_pembelian` DISABLE KEYS */;
INSERT INTO `tabel_pembelian` VALUES (13881949431000,13881948941000,1439,100,7,1000,100,0,'admin',1,'AUTO'),(14085459381000,14085458821000,1042,50,12,750,10,40,'admin',1,'AUTO');
/*!40000 ALTER TABLE `tabel_pembelian` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_stok_order` BEFORE INSERT ON `tabel_pembelian` FOR EACH ROW SET New.kode_pembelian=CONCAT(UNIX_TIMESTAMP(NOW()),New.kode_pembelian),New.jumlah_stok=New.jumlah_pembelian */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_pengiriman`
--

DROP TABLE IF EXISTS `tabel_pengiriman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_pengiriman` (
  `kode_do` bigint(20) NOT NULL DEFAULT '1000',
  `nomer_do` varchar(45) NOT NULL,
  `nomer_kontrak` varchar(45) NOT NULL,
  `kode_pelanggan` char(6) NOT NULL,
  `tanggal_do` date DEFAULT NULL,
  `tanggal_drop` datetime DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `jatuh_tempo` tinyint(4) NOT NULL DEFAULT '0',
  `kar_id` varchar(20) NOT NULL DEFAULT 'auto',
  `kode_status` tinyint(4) NOT NULL DEFAULT '3',
  `kode_bayar` tinyint(4) NOT NULL DEFAULT '0',
  `keterangan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kode_do`),
  KEY `kode_pelanggan_do` (`kode_pelanggan`),
  CONSTRAINT `kode_pelanggan_do` FOREIGN KEY (`kode_pelanggan`) REFERENCES `tabel_vendor` (`kode_vendor`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_pengiriman`
--

LOCK TABLES `tabel_pengiriman` WRITE;
/*!40000 ALTER TABLE `tabel_pengiriman` DISABLE KEYS */;
INSERT INTO `tabel_pengiriman` VALUES (13881952232000,'000000','-','000000','2013-12-28','2013-12-28 08:47:31','2013-12-28 08:47:31',0,'admin',1,2,'AUTO'),(13881952512000,'000000','-','000000','2013-12-28','2013-12-28 08:48:55','2013-12-28 10:45:21',7,'admin',1,2,'AUTO'),(13881953352000,'000000','-','000000','2013-12-28','2013-12-28 09:11:05',NULL,7,'admin',1,0,'AUTO'),(13882017672000,'000000','-','000000','2013-12-28','2013-12-28 10:36:29',NULL,7,'admin',1,0,'AUTO'),(13882018442000,'000000','-','000000','2013-12-28','2013-12-28 10:37:45',NULL,7,'admin',1,0,'AUTO'),(13882018652000,'000000','-','000000','2013-12-28','2013-12-28 10:51:24','2013-12-28 10:51:24',0,'admin',1,2,'AUTO'),(13882026842000,'000000','-','000000','2013-12-28','2014-08-23 08:21:24','2014-08-23 08:21:24',0,'admin',1,2,'AUTO'),(14087568852000,'000000','-','000000','2014-08-23',NULL,NULL,0,'admin',3,0,'AUTO'),(14222557462000,'000000','-','000031','2015-01-26',NULL,NULL,0,'admin',3,0,'AUTO');
/*!40000 ALTER TABLE `tabel_pengiriman` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_update_do` BEFORE UPDATE ON `tabel_pengiriman` FOR EACH ROW UPDATE tabel_penjualan SET kode_status=New.kode_status WHERE kode_do=New.kode_do */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_penjualan`
--

DROP TABLE IF EXISTS `tabel_penjualan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_penjualan` (
  `kode_penjualan` bigint(20) NOT NULL DEFAULT '1000',
  `kode_do` bigint(20) NOT NULL,
  `kode_barang` int(11) NOT NULL DEFAULT '0',
  `jumlah_penjualan` int(11) NOT NULL DEFAULT '0',
  `harga_penjualan` int(11) NOT NULL DEFAULT '0',
  `harga_diskon` int(11) NOT NULL,
  `diskon` tinyint(4) NOT NULL,
  `kode_satuan` int(11) NOT NULL,
  `kar_id` varchar(20) NOT NULL DEFAULT 'auto',
  `kode_status` tinyint(4) NOT NULL DEFAULT '3',
  `keterangan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kode_penjualan`),
  KEY `kode_barang_jual` (`kode_barang`),
  KEY `kode_satuan_jual` (`kode_satuan`),
  KEY `kode_do` (`kode_do`),
  CONSTRAINT `kode_barang_jual` FOREIGN KEY (`kode_barang`) REFERENCES `ref_barang` (`kode_barang`) ON UPDATE CASCADE,
  CONSTRAINT `kode_do` FOREIGN KEY (`kode_do`) REFERENCES `tabel_pengiriman` (`kode_do`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `kode_satuan_jual` FOREIGN KEY (`kode_satuan`) REFERENCES `ref_satuan` (`kode_satuan`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_penjualan`
--

LOCK TABLES `tabel_penjualan` WRITE;
/*!40000 ALTER TABLE `tabel_penjualan` DISABLE KEYS */;
INSERT INTO `tabel_penjualan` VALUES (13881952361000,13881952232000,1439,10,2000,2000,0,7,'admin',1,'AUTO'),(13881953201000,13881952512000,1439,10,2000,2000,0,7,'admin',1,'AUTO'),(13881966451000,13881953352000,1439,10,2000,2000,0,7,'admin',1,'AUTO'),(13882017781000,13882017672000,1439,10,2000,2000,0,7,'admin',1,'AUTO'),(13882018551000,13882018442000,1439,10,2000,2000,0,7,'admin',1,'AUTO'),(13882026751000,13882018652000,1439,50,2000,2000,0,7,'admin',1,'AUTO'),(14085459591000,13882026842000,1042,10,1000,1000,0,12,'admin',1,'AUTO');
/*!40000 ALTER TABLE `tabel_penjualan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_draft_penjualan` BEFORE INSERT ON `tabel_penjualan` FOR EACH ROW BEGIN
DECLARE e_stok,n_stok,n_jual INT;
DECLARE done INT DEFAULT FALSE;
DECLARE e_kode_pembelian BIGINT;
DECLARE cur1 CURSOR FOR SELECT kode_pembelian,jumlah_stok FROM tabel_pembelian WHERE kode_barang=New.kode_barang AND jumlah_stok>0 AND kode_status=1 AND kode_satuan=New.kode_satuan ORDER BY kode_pembelian ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
SET New.kode_penjualan=CONCAT(UNIX_TIMESTAMP(NOW()),New.kode_penjualan);
SELECT SUM(jumlah_stok) INTO e_stok FROM tabel_pembelian WHERE kode_barang=New.kode_barang AND kode_status=1 AND kode_satuan=New.kode_satuan GROUP BY kode_barang;
IF e_stok<New.jumlah_penjualan THEN
  SET New.kode_status=2;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_valid_penjualan` BEFORE UPDATE ON `tabel_penjualan` FOR EACH ROW BEGIN
DECLARE e_stok,n_stok,n_jual INT;
DECLARE done INT DEFAULT FALSE;
DECLARE e_kode_pembelian BIGINT;
DECLARE cur1 CURSOR FOR SELECT kode_pembelian,jumlah_stok FROM tabel_pembelian WHERE kode_barang=New.kode_barang AND jumlah_stok>0 AND kode_status=1 AND kode_satuan=New.kode_satuan ORDER BY kode_pembelian ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
IF Old.kode_status=3 AND New.kode_status=1 THEN
	SELECT SUM(jumlah_stok) INTO e_stok FROM tabel_pembelian WHERE kode_barang=New.kode_barang AND kode_status=1 AND kode_satuan=New.kode_satuan GROUP BY kode_barang;
	IF e_stok>=New.jumlah_penjualan THEN
	  SET New.kode_status=1;
	  SET n_jual=New.jumlah_penjualan;
	  OPEN cur1;
	  read_loop: LOOP
		FETCH cur1 INTO e_kode_pembelian,n_stok;
		IF done THEN
		  LEAVE read_loop;
		END IF;
		IF n_stok>=n_jual THEN
		  INSERT INTO tabel_transaksi(kode_penjualan,kode_pembelian,jumlah_penjualan) VALUES(New.kode_penjualan,e_kode_pembelian,n_jual);
		  SET n_jual=0;
		ELSE
		  INSERT INTO tabel_transaksi(kode_penjualan,kode_pembelian,jumlah_penjualan) VALUES(New.kode_penjualan,e_kode_pembelian,n_stok);
		  SET n_jual=n_jual-n_stok;
		END IF;
		IF n_jual=0 THEN
		  SET done=TRUE;
		END IF;
	  END LOOP;
	  CLOSE cur1;
	ELSE
	  SET New.kode_status=2;
	END IF;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_po`
--

DROP TABLE IF EXISTS `tabel_po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_po` (
  `kode_po` bigint(20) NOT NULL DEFAULT '1000',
  `nomer_po` varchar(45) NOT NULL,
  `nomer_kontrak` varchar(45) NOT NULL,
  `kode_vendor` char(6) NOT NULL,
  `tanggal_po` date DEFAULT NULL,
  `tanggal_drop` datetime DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `kar_id` varchar(20) NOT NULL DEFAULT 'auto',
  `kode_status` tinyint(4) NOT NULL DEFAULT '3',
  `kode_bayar` tinyint(4) NOT NULL DEFAULT '0',
  `keterangan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kode_po`),
  KEY `kode_vendor_po` (`kode_vendor`),
  CONSTRAINT `kode_vendor_po` FOREIGN KEY (`kode_vendor`) REFERENCES `tabel_vendor` (`kode_vendor`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_po`
--

LOCK TABLES `tabel_po` WRITE;
/*!40000 ALTER TABLE `tabel_po` DISABLE KEYS */;
INSERT INTO `tabel_po` VALUES (13881948941000,'000000','-','000031','2013-12-28','2013-12-28 08:42:38','2013-12-28 08:46:16','admin',1,2,'AUTO'),(14085458821000,'000000','-','000031','2014-08-20','2014-08-20 21:45:44','2014-08-20 21:45:44','admin',1,0,'AUTO');
/*!40000 ALTER TABLE `tabel_po` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_kode_po` BEFORE INSERT ON `tabel_po` FOR EACH ROW IF LENGTH(New.kode_po)=4 THEN
  SET New.kode_po=CONCAT(UNIX_TIMESTAMP(NOW()),New.kode_po);
END IF */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_update_po` BEFORE UPDATE ON `tabel_po` FOR EACH ROW UPDATE tabel_pembelian SET kode_status=New.kode_status WHERE kode_po=New.kode_po */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_trans_log`
--

DROP TABLE IF EXISTS `tabel_trans_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_trans_log` (
  `tr_id` bigint(20) NOT NULL,
  `tr_sts` tinyint(4) NOT NULL DEFAULT '0',
  `tr_ip` bigint(11) NOT NULL DEFAULT '0',
  `kp_kode` char(2) NOT NULL DEFAULT '00',
  `kar_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'auto',
  PRIMARY KEY (`tr_id`),
  KEY `KASIR` (`kar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_trans_log`
--

LOCK TABLES `tabel_trans_log` WRITE;
/*!40000 ALTER TABLE `tabel_trans_log` DISABLE KEYS */;
INSERT INTO `tabel_trans_log` VALUES (13881946691125,1,168296453,'00','admin'),(13884019895120,1,168297225,'00','admin'),(14085425214893,1,168296453,'00','admin'),(14085458226267,1,168296453,'00','admin'),(14087568537606,1,168296453,'00','admin'),(14091476251483,1,168296453,'00','admin'),(14222556987932,1,168296453,'00','admin');
/*!40000 ALTER TABLE `tabel_trans_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_resetResi` BEFORE INSERT ON `tabel_trans_log` FOR EACH ROW BEGIN
  IF New.tr_sts=3 THEN
    INSERT INTO system_parameter(sys_param,sys_value,sys_value1,sys_value2) VALUES('RESI',New.kar_id,1,'-') ON DUPLICATE KEY UPDATE sys_value1=1;
  END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_transaksi`
--

DROP TABLE IF EXISTS `tabel_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_transaksi` (
  `kode_penjualan` bigint(20) NOT NULL,
  `kode_pembelian` bigint(20) NOT NULL,
  `jumlah_penjualan` int(11) NOT NULL DEFAULT '0',
  `kode_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`kode_penjualan`,`kode_pembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_transaksi`
--

LOCK TABLES `tabel_transaksi` WRITE;
/*!40000 ALTER TABLE `tabel_transaksi` DISABLE KEYS */;
INSERT INTO `tabel_transaksi` VALUES (13881952361000,13881949431000,10,1),(13881953201000,13881949431000,10,1),(13881966451000,13881949431000,10,1),(13882017781000,13881949431000,10,1),(13882018551000,13881949431000,10,1),(13882026751000,13881949431000,50,1),(14085459591000,14085459381000,10,1);
/*!40000 ALTER TABLE `tabel_transaksi` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_udpate_stok` BEFORE INSERT ON `tabel_transaksi` FOR EACH ROW UPDATE tabel_pembelian SET jumlah_penjualan=(jumlah_penjualan+New.jumlah_penjualan),jumlah_stok=(jumlah_stok-New.jumlah_penjualan) WHERE kode_pembelian=New.kode_pembelian */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_user`
--

DROP TABLE IF EXISTS `tabel_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_user` (
  `kar_id` varchar(20) NOT NULL,
  `grup_id` varchar(10) DEFAULT NULL,
  `kar_pass` varchar(50) DEFAULT NULL,
  `kar_nip` varchar(10) DEFAULT NULL,
  `kar_nik` varchar(50) DEFAULT NULL,
  `kar_nama` varchar(50) DEFAULT NULL,
  `kar_jabatan` varchar(30) DEFAULT NULL,
  `kar_pangkat` varchar(20) DEFAULT NULL,
  `kar_email` varchar(50) DEFAULT NULL,
  `kar_note` varchar(30) DEFAULT NULL,
  `kp_kode` varchar(10) DEFAULT NULL,
  `ip_print` int(11) DEFAULT NULL,
  `kar_sts` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`kar_id`),
  KEY `grup_user` (`grup_id`),
  CONSTRAINT `grup_user` FOREIGN KEY (`grup_id`) REFERENCES `tabel_grup` (`grup_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_user`
--

LOCK TABLES `tabel_user` WRITE;
/*!40000 ALTER TABLE `tabel_user` DISABLE KEYS */;
INSERT INTO `tabel_user` VALUES ('admin','000','fe01ce2a7fbac8fafaed7c982a04e229','-','-','Administrator','-','-','-','-','00',1,'1'),('pembelian','003','fe01ce2a7fbac8fafaed7c982a04e229','-','-','Pembelian','-','-','-','-','00',1,'1'),('penjualan','004','fe01ce2a7fbac8fafaed7c982a04e229','-','-','Penjualan','-','-','-','-','00',1,'1');
/*!40000 ALTER TABLE `tabel_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabel_vendor`
--

DROP TABLE IF EXISTS `tabel_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_vendor` (
  `kode_vendor` char(6) NOT NULL DEFAULT '000000',
  `contact_person` varchar(45) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `alamat` varchar(90) NOT NULL,
  `kota` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  `fax_number` varchar(45) DEFAULT NULL,
  `kode_pos` char(5) NOT NULL,
  `kode_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`kode_vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_vendor`
--

LOCK TABLES `tabel_vendor` WRITE;
/*!40000 ALTER TABLE `tabel_vendor` DISABLE KEYS */;
INSERT INTO `tabel_vendor` VALUES ('000000','-','-','-','-','-','-','-','-',1),('000001','','PT. Tri Sapta Jaya','JL. Singgasana Raya No. 41 Cibaduyut Bandung','Bandung','-','-',NULL,'40236',1),('000002','','PT. Bina San Prima','Jl. Petireman No. 11 b Cirebon','Cirebon','-','0231-204866','0231-201853','45113',1),('000003','','PT Enseval Kanvas','Cirebon','Cirebon','-','-',NULL,'-',1),('000004','','PT. SAPTA SARI TAMA','Cirebon','Cirebon','-','-',NULL,'-',1),('000005','','Apotek Ria','Cirebon','Cirebon','-','-',NULL,'-',1),('000006','','PT. INTI CENTRAL PHARMA','Jl. Pinang Raya No.234 B Cirebon','Cirebon','-','-',NULL,'-',1),('000007','','Pasar','Kuningan','Kuningan','-','-',NULL,'-',1),('000008','','PT.Molex','Bandung','Bandung','-','-',NULL,'-',1),('000009','','PT.APL','Cirebon','Cirebon','-','-',NULL,'-',1),('000010','Yona','PT. HASIL KARYA SEJAHTERA','jl. golf raya no 39, cisaranten bina harapan bdg','Bandung','-','022-7834298','022-7834298','40294',1),('000011','','PT.ARTA BOGA CEMERLANG','JL.PALMERAH BARAT NO.82 JAKARTA','Cirebon','-','0231-510154',NULL,'-',1),('000012','','PT SUPRA USADHATAMA','JL.H.R.RASUNA SAID KAV. 3-4','Cirebon','-','-',NULL,'-',1),('000013','','APOTEK BUANA','JL.PRAMUKA NO.75 KUNINGAN','Kuningan','-','-',NULL,'-',1),('000014','','PT LENKO SURYA PERKASA','JL. KUSNAN NO 11','Cirebon','-','-',NULL,'-',1),('000015','','PT.ALAMI CIPTA ABADI','JL.GUNUNG PERENG NO.36 TASIK MALAYA','Tasikmalaya','-','-',NULL,'-',1),('000016','','PROCOLD','RUKO','Cirebon','-','-',NULL,'-',1),('000017','','PT INDUSTRI JAMU BOROBUDUR','JL.MADUKORO BLOK A NO 26','Semarang','-','-',NULL,'-',1),('000018','','PT LARAS WIRA FARMA ','JL PASAK BUMI 0011 NO 19 HARAPAN CIBIRU BANDUNG','Bandung','-','-',NULL,'-',1),('000019','','APT. DELLA FARMA','JL. BIMA DESA JEMARAS LOR','Kuningan','-','-',NULL,'-',1),('000020','','PT INTERCALLIN','JL DAAN MOGOT KM 11 CENGKARENG','Jakarta Barat','-','-',NULL,'-',1),('000021','','PT. Parit Padang Global','Wiratama No.25 Tuparev Kel.Kedung Jaya Kec.Kedawung Cirebon','Cirebon','-','-',NULL,'-',1),('000022','','Agen herbal Zahira','jl.Raya Karangsuwung-Karangsembung Cirebon','Cirebon','-','-',NULL,'-',1),('000023','','PT. Anugrah Pharmindo Lestari','Jl. Pulolentut','Jakarta','-','-',NULL,'-',1),('000024','','CV.EKA GEMILANG PRATAMA','JL. CAKRA BUANA PERUM','Cirebon','-','-',NULL,'-',1),('000025','','PT. Intra Asma Mulia','JL. Kapten Damsur No.60','Cirebon','-','-',NULL,'-',1),('000026','','Sumbar Makmur','cirebon','Cirebon','-','-',NULL,'-',1),('000027','','Mitra periangan persada cirebon','cirebon','Cirebon','-','-',NULL,'-',1),('000028','-','Kimia Farma','Jl Kesambi No. 9','Cirebon','-','-','-','-',1),('000029','Andi-CRB','PT. Inti Pharma Persada','Jl. Myr. Sastraatmaja No. 1','Cirebon','-','0231-91400300','0231-205993','-',1),('000030','YAN','PT. Nurosadi Farma','Jl. Caringin No. 254','Bandung','-','022-6044131','022-6026307','-',1),('000031','-','AMAN FARMA','Jl. Lawanggada No. 54','Cirebon','-','0231-208788','-','-',1),('000032','Moko','PT.Bina Putra Libra','Kopo Permai I 55A No.14 Bandung ','Bandung','-','022-5404700','022-5417277','-',1),('000033','Jojo','PT.Milenium Pharmacon int','Jl.Kesambi No.78 Cirebon','Cirebon','-','0231-231615','0231-231614','-',1),('000034','','PT.Tritunggal Mulia Wisesa','Kuningan','Kuningan','-','-',NULL,'-',1),('000035','','Cv.Anugerah Jaya','Jl.Jagasatru No.75 Cirebon','Cirebon','-','0231-205126',NULL,'-',1),('000036','','PT.Almedika Alkesindo','Ruko Duta Bahagia No.3 Pekalongan','Pekalongan','-','0285-412533','0285-412534','-',1),('000037','','PT.Kebayoran Pharma','Komp.Pegambiran Estate \r\nJl.Palem Raya Blok D.7-9 Cirebon','Cirebon','-','-',NULL,'-',1),('000038','','PT.Citra Jaya Putra Utama','JL.Karang Jalak GG. Siliwangi No.5 Cirebon','Cirebon','-','0231-201661','0231-201661','-',1),('000039','','PT.Combi Putra','JL.Terusan Holis No. 472','Bandung','-','022-5407365','022-5407364','-',1),('000040','','PT.Mensa Binasukses','JL.Brigjen Darsono No. 9 Kedawung','Cirebon','-','-',NULL,'-',1),('000041','','PT.Enseval Putera Megatrading Tbk','JL.Raya Klayan No. 57 Cirebon','Cirebon','-','0231-201121',NULL,'-',1),('000042','','PT.Kalista Prima','JL.Dr.Sutomo No. 35','Cirebon','-','-',NULL,'-',1),('000043','','PT.Dos Ni Roha','JL.Lapangan Udara No. 17 ','Cirebon','-','-',NULL,'-',1),('000044','','PT.Indofarma GM','JL.Pilang Raya No. 147','Cirebon','-','-',NULL,'-',1),('000045','','PT.Merapi Utama Pharma','JL.Cilosari 23','Jakarta','-','-',NULL,'-',1),('000046','','PT.Antar Mitra Sembada','Jl.Sultan Ageng Tirtayasa No.15','Cirebon','-','0231-8496796','0231-483986','-',1),('000047','','PT.Marga Nusantara Jaya','Jl.Katiasa No.7A','Cirebon','-','-',NULL,'-',1),('000048','','Cv.Carmela','Jl.Lawanggada No.39','Cirebon','-','0231-238551','0231-238551','-',1),('000049','','UD.Aman Jaya','Jl.Lawanggada ','Cirebon','-','-',NULL,'-',1),('000050','','Tk.As Sunnah','Jl.Kalitanjung','Cirebon','-','-',NULL,'-',1),('000051','','PT.Sawah Besar Farma','Jl.Kedrunan no.5 Kesenden Cirebon.','Cirebon','-','-',NULL,'-',1),('000052','','PT. Penta Valent','Jl. Angkasa No. 3 Kel. Harjamukti ','Cirebon','-','0231-484419,490252',NULL,'45143',1),('000053','','PT Kinarya Satria Farma','Jl. Prabu Gajah Agung No. 48 Sumedang','Sumedang','-','0261-205236',NULL,'-',1),('000054','','PT. Conmed','Cirebon','Cirebon','-','-',NULL,'-',1),('000055','','PT. Kembang Christapharma','Jl. Sarimas Utara No. 1','Bandung','-','022-7203258',NULL,'-',1),('000056','','PT. Cipta Karya Mandiri','Jl. Raya Lembang Km. 15 No. 134 Batureok','Bandung','-','022-2787092',NULL,'-',1),('000057','081324590546','Simon Bapak','Jl, Kadugede, Kuningan','-','Kuningan','-',NULL,'-',1),('000058','-','Oman Bapak','Jl. Cibingbin, Kuningan','-','Kuningan','-',NULL,'-',1),('000059','-','Yus bapak','Cileuleuy','-','Kuningan','-',NULL,'-',1),('000060','-','Triwani Dr. Hj.','Cipondok, Kadugede','-','Kuningan','-',NULL,'-',1),('000061','-','Asikin Dr. ','Garawangi','-','Kuningan','-',NULL,'-',1),('000062','-','Ena ibu','Bojong','-','Kuningan','-',NULL,'-',1),('000063','-','Oneng Bidan','Cibingbin','-','Kuningan','-',NULL,'-',1),('000064','-','Lina Bidan','Pkm Kadugede','-','Kuningan','-',NULL,'-',1),('000065','-','Darno Bapak','Pamulihan','-','Kuningan','-',NULL,'-',1),('000066','-','Ade Dr','Ciherang','-','Kuningan','-',NULL,'-',1),('000067','-','Tanti Dr','Cikaso','-','Kuningan','-',NULL,'-',1),('000068','-','Teti Babatan','Babatan','-','Kuningan','-',NULL,'-',1),('000069','-','Deasy Dr','Bayuning','-','Kuningan','-',NULL,'-',1),('000070','-','Albert Bapak','Cigugur','-','Kuningan','-',NULL,'-',1),('000071','-','Mamah Endang Ibu','Sindang Laut','-','Cirebon','-',NULL,'-',1),('000072','-','Nia bidan Karangmangu','Karangmangu','-','Kuningan','-',NULL,'-',1),('000073','-','Cholid Dr. H.','Bendungan','-','Kuningan','-',NULL,'-',1),('000074','-','Arumi Bidan Hj','Citangtu','-','Kuningan','-',NULL,'-',1),('000075','-','Agus Dr','Cibungkang','-','Kuningan','-',NULL,'-',1),('000076','-','SArman Dr.','Maleber','-','Kuningan','-',NULL,'-',1),('000077','-','Edi Bapak H.','Cibingbin','-','Kuningan','-',NULL,'-',1),('000078','-','Memed Bapak','Maleber','-','Kuningan','-',NULL,'-',1),('000079','-','Eka Dr Hj','Citangtu','-','Kuningan','-',NULL,'-',1),('000080','-','Handris Dr','Kuningan','-','Kuningan','-',NULL,'-',1),('000081','-','RS KMC','KUNINGAN','-','Kuningan','-',NULL,'-',1),('000082','-','Eddie Syarif Dr','Jl. Sadamantra','-','Kuningan','-',NULL,'-',1),('000083','-','Maman bapak H.','Ciniru','-','Kuningan','-',NULL,'-',1),('000084','-','Toni Dr','Kramat Mulya','-','Kuningan','-',NULL,'-',1),('000085','-','Eem bidan','Cimaranten','-','Kuningan','-',NULL,'-',1),('000086','-','Mustakim, Bp','Cihaur, Ciawi','-','Kuningan','-',NULL,'-',1),('000087','-','Yanti, dr','Ciawi','-','kuningan','081802430269',NULL,'-',1),('000088','-','Cecep Hj. Bapak','Ciherang','-','Kuningan','-',NULL,'-',1),('000089','-','Oneng Hj Bidan','Cibingbin','-','Kuningan','-',NULL,'-',1),('000090','-','Mamat Bapak','Cibingbin','-','Kuningan','-',NULL,'-',1),('000091','-','Andi Bpk','Kramat Mulya','-','Kuningan','-',NULL,'-',1),('000092','-','Etih ibu','Cibingbin','-','Kuningan','-',NULL,'-',1),('000093','-','Eha Dr. Hj.','Kuningan','-','Kuningan','-',NULL,'-',1),('000094','-','Aa Bapak','Cibingbin','-','Kuningan','-',NULL,'-',1),('000095','-','Erwin Dr','Sindang Laut','-','Cirebon','-',NULL,'-',1),('000096','-','Iin Bidan, Randubawa','Randubawa','-','Kuningan','-',NULL,'-',1),('000097','-','Siti Hasanah Bdn','Ciawi','-','Kuningan','-',NULL,'-',1),('000098','-','Rizal Bapak','Kadugede','-','Kuningan','-',NULL,'-',1),('000099','-','Yani bidan','Karoya','-','Kuningan','-',NULL,'-',1),('000100','-','Udin H. Bapak','Setianegara','-','Kuningan','-',NULL,'-',1),('000101','-','Yetty Bd. Hj. ','Windusengkahan','-','Kuningan','-',NULL,'-',1),('000102','-','Kusdi H. Bapak','Cidahu','-','Kuningan','-',NULL,'-',1),('000103','-','Apotek Winduhaji','winduhaji','-','Kuningan','-',NULL,'-',1),('000104','-','Fadil bapak','Cibingbin','-','Kuningan','-',NULL,'-',1),('000105','-','Rahmat Dr.','Panawuan','-','Kuningan','-',NULL,'-',1),('000106','-','Nono, Bapak','Jalaksana','-','kuningan','087723225660',NULL,'-',1),('000107','-','Titin, Ibu','Cibingbin','-','Kuningan','-',NULL,'-',1),('000108','-','Tedi, bapak','Jl. Ciniru','-','Kuningan','-',NULL,'-',1),('000109','-','Yuyu, dr','Sindangagung','-','Kuningan','-',NULL,'-',1),('000110','081223185454','dr Devi','Panumbangan Ciamis','-','Ciamis','-',NULL,'-',1),('000111','-','Rian, bapa','Kramat Mulya','-','Kuningan','-',NULL,'-',1),('000112','-','Oom,bidan','Cibentang','-','Kuningan','-',NULL,'-',1),('000113','-','Deni,bapak','Gandasoli','-','Kuningan','-',NULL,'-',1),('000114','-','Wan, drg','Kuningan','-','Kuningan','-',NULL,'-',1),('000115','-','ARI, BIDAN','KALIMANGGIS WETAN','-','Kuningan','-',NULL,'-',1),('000116','-','NUR, BDN','KADUGEDE','-','Kuningan','-',NULL,'-',1),('000117','-','UNAH, HJ, BDN','CIDAHU','-','Kuningan','-',NULL,'-',1),('000118','-','TATI R, BDN','WALAHAR','-','Kuningan','-',NULL,'-',1),('000119','-','EMI, HJ, BDN','DESA PESING','-','Kuningan','-',NULL,'-',1),('000120','-','Stok Opname','-','-','-','-','-','-',1);
/*!40000 ALTER TABLE `tabel_vendor` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `_get_kode_vendor` BEFORE INSERT ON `tabel_vendor` FOR EACH ROW BEGIN
  DECLARE i_kode INT DEFAULT 0;
  SELECT IFNULL(ABS(MAX(kode_vendor)),0) INTO i_kode FROM tabel_vendor;
  SET i_kode = i_kode + 1;
  SET New.kode_vendor = CONCAT(REPEAT(0,6-LENGTH(i_kode)),i_kode);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tabel_wilayah`
--

DROP TABLE IF EXISTS `tabel_wilayah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabel_wilayah` (
  `kp_kode` char(2) NOT NULL,
  `cab_kode` char(2) NOT NULL,
  `kp_nama` varchar(4) NOT NULL,
  `kp_ket` varchar(50) NOT NULL,
  `kp_ip` int(11) NOT NULL,
  `kp_ftp_user` varchar(50) NOT NULL,
  `kp_ftp_pass` varchar(50) NOT NULL,
  `kp_ftp_folder` varchar(100) NOT NULL,
  PRIMARY KEY (`kp_kode`),
  UNIQUE KEY `kp_kode` (`kp_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabel_wilayah`
--

LOCK TABLES `tabel_wilayah` WRITE;
/*!40000 ALTER TABLE `tabel_wilayah` DISABLE KEYS */;
INSERT INTO `tabel_wilayah` VALUES ('00','00','PUST','Pusat',0,'0','0','0');
/*!40000 ALTER TABLE `tabel_wilayah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_daftar_harga`
--

DROP TABLE IF EXISTS `v_daftar_harga`;
/*!50001 DROP VIEW IF EXISTS `v_daftar_harga`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_daftar_harga` (
  `tanggal_perolehan` date,
  `kode_barang` int(11),
  `kode_satuan` int(11),
  `nama_barang` varchar(45),
  `nama_satuan` varchar(45),
  `jumlah_stok` int(11),
  `harga_pokok` int(11),
  `harga_dasar` int(11)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_menu_item`
--

DROP TABLE IF EXISTS `v_menu_item`;
/*!50001 DROP VIEW IF EXISTS `v_menu_item`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_menu_item` (
  `appl_kode` char(6),
  `appl_seq` tinyint(3) unsigned,
  `l1` varchar(2),
  `l2` varchar(2),
  `l3` varchar(2),
  `parent_id` char(6),
  `ga_kode` char(6),
  `ga_nama` varchar(50),
  `appl_file` varchar(50),
  `appl_proc` varchar(50),
  `appl_name` varchar(50),
  `appl_nama` varchar(50),
  `appl_desc` varchar(50),
  `appl_sts` tinyint(4),
  `status` varchar(7)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_pembayaran`
--

DROP TABLE IF EXISTS `v_pembayaran`;
/*!50001 DROP VIEW IF EXISTS `v_pembayaran`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_pembayaran` (
  `kode_do` bigint(20),
  `byr_tgl` date,
  `byr_total` decimal(42,0),
  `byr_sts` decimal(26,0)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_pembelian`
--

DROP TABLE IF EXISTS `v_pembelian`;
/*!50001 DROP VIEW IF EXISTS `v_pembelian`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_pembelian` (
  `Tanggal` date,
  `Nama Barang` varchar(45),
  `Satuan` varchar(45),
  `Harga Satuan` varchar(47),
  `Total Item` varchar(47),
  `Sisa Stok` varchar(47),
  `Keterangan` varchar(45)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_pengguna`
--

DROP TABLE IF EXISTS `v_pengguna`;
/*!50001 DROP VIEW IF EXISTS `v_pengguna`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_pengguna` (
  `usr_id` varchar(20),
  `usr_nama` varchar(50),
  `grup_id` varchar(10),
  `grup_nama` varchar(70),
  `pdam_kode` varchar(10),
  `pdam_nama` varchar(50)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_stok`
--

DROP TABLE IF EXISTS `v_stok`;
/*!50001 DROP VIEW IF EXISTS `v_stok`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_stok` (
  `Jumlah` decimal(32,0),
  `Barang` varchar(91),
  `Transaksi` varchar(4)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_penjualan`
--

DROP TABLE IF EXISTS `view_penjualan`;
/*!50001 DROP VIEW IF EXISTS `view_penjualan`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_penjualan` (
  `kode_resi` char(9),
  `tanggal_drop` datetime,
  `byr_cetak` decimal(26,0),
  `total_transaksi` decimal(42,0),
  `total_dibayar` decimal(42,0),
  `kode_bayar` tinyint(4),
  `remark_tgl` varchar(10)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_stok`
--

DROP TABLE IF EXISTS `view_stok`;
/*!50001 DROP VIEW IF EXISTS `view_stok`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_stok` (
  `Kode` mediumtext,
  `Nama Barang` varchar(45),
  `Jenis` enum('Obat Dewasa','Obat Anak','Obat Dalam','Obat Luar'),
  `Satuan` varchar(45),
  `Pokok` int(11),
  `Harga Jual` int(11),
  `Stok` decimal(32,0),
  `Total Pokok` bigint(21),
  `Total Jual` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'tran_qwey'
--

--
-- Dumping routines for database 'tran_qwey'
--
/*!50003 DROP FUNCTION IF EXISTS `decodeResi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `decodeResi`(e_kode_do CHAR(9)) RETURNS bigint(20)
BEGIN
	RETURN CONV(e_kode_do,36,10);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `encodeResi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `encodeResi`(e_kode_do BIGINT) RETURNS char(9) CHARSET utf8
BEGIN
	RETURN CONV(e_kode_do,10,36);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getMenu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `getMenu`(e_grup_id VARCHAR(7),e_appl_kode VARCHAR(7)) RETURNS tinyint(4)
BEGIN
DECLARE e_sts TINYINT DEFAULT 1;
IF e_grup_id = '000' THEN
RETURN e_sts;
ELSE
SELECT COUNT(a.appl_kode) INTO e_sts FROM tabel_grup_appl a WHERE a.grup_id=e_grup_id AND a.appl_kode=e_appl_kode;
RETURN e_sts;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getML` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `getML`(e_ga_kode CHAR(6),e_sub TINYINT) RETURNS char(6) CHARSET utf8
BEGIN
	DECLARE e_l0 TINYINT;
	CASE
		WHEN e_ga_kode='000000' THEN
			SELECT (MAX(ABS(SUBSTR(appl_kode,1,2)))+1) INTO e_l0 FROM tabel_aplikasi WHERE ga_kode=e_ga_kode;
			RETURN CONCAT(REPEAT(0,2-LENGTH(e_l0)),e_l0,'000',e_sub);
		WHEN SUBSTR(e_ga_kode,3,4)='0000' THEN
			SELECT IFNULL((MAX(ABS(SUBSTR(appl_kode,3,2)))+1),1) INTO e_l0 FROM tabel_aplikasi WHERE ga_kode=e_ga_kode;
			IF e_sub=1 THEN
				RETURN CONCAT(SUBSTR(e_ga_kode,1,2),REPEAT(0,2-LENGTH(e_l0)),e_l0,'01');
			ELSE
				RETURN CONCAT(SUBSTR(e_ga_kode,1,2),REPEAT(0,2-LENGTH(e_l0)),e_l0,'00');
			END IF;
		ELSE
			SELECT IFNULL((MAX(ABS(SUBSTR(appl_kode,5,2)))+1),1) INTO e_l0 FROM tabel_aplikasi WHERE ga_kode=e_ga_kode;
			RETURN CONCAT(SUBSTR(e_ga_kode,1,4),REPEAT(0,2-LENGTH(e_l0)),e_l0);
	END CASE;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getTanggal` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `getTanggal`(e_kode BIGINT) RETURNS datetime
BEGIN
	DECLARE e_count,f_count TINYINT;
	SET e_count=LENGTH(e_kode)-4;
	RETURN FROM_UNIXTIME(SUBSTR(e_kode,1,e_count));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proses_harian` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `proses_harian`()
BEGIN
DELETE FROM `persediaan_harian` WHERE DATE(`Tanggal Proses`)=CURDATE();
INSERT INTO `persediaan_harian` SELECT NOW(),CURDATE(),`b`.`nama_barang`,format(SUM(`a`.`jumlah_stok`),0),`c`.`nama_satuan`,max(a.harga),a.kode_barang,a.kode_satuan from ((`tabel_pembelian` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) group by a.kode_barang,a.kode_satuan  order by `b`.`nama_barang`,a.kode_satuan;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `tran_qwey`
--

USE `tran_qwey`;

--
-- Final view structure for view `050101_view`
--

/*!50001 DROP TABLE IF EXISTS `050101_view`*/;
/*!50001 DROP VIEW IF EXISTS `050101_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `050101_view` AS select date_format(cast(`getTanggal`(`a`.`kode_pembelian`) as date),'%d-%m-%Y') AS `tgl_transaksi`,(to_days(curdate()) - to_days(`getTanggal`(`a`.`kode_pembelian`))) AS `hari`,period_diff(date_format(now(),'%Y%m'),date_format(`getTanggal`(`a`.`kode_pembelian`),'%Y%m')) AS `bulan`,`d`.`kode_vendor` AS `kode_vendor`,`d`.`nama` AS `nama_vendor`,`b`.`kode_barang` AS `kode_barang`,`b`.`nama_barang` AS `nama_barang`,`c`.`kode_satuan` AS `kode_satuan`,`c`.`nama_satuan` AS `nama_satuan`,`a`.`harga` AS `harga_perolehan`,`a`.`jumlah_pembelian` AS `jumlah_pembelian`,`a`.`jumlah_penjualan` AS `jumlah_penjualan`,`a`.`jumlah_stok` AS `jumlah_persediaan`,if((length(`a`.`keterangan`) > 0),`a`.`keterangan`,_utf8'-') AS `keterangan` from ((((`tabel_pembelian` `a` join `tabel_po` `e` on((`e`.`kode_po` = `a`.`kode_po`))) join `tabel_vendor` `d` on((`d`.`kode_vendor` = `e`.`kode_vendor`))) join `ref_barang` `b` on(((`b`.`kode_barang` = `a`.`kode_barang`) and (`a`.`kode_status` = 1)))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) order by `a`.`kode_pembelian`,`b`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `050102_view`
--

/*!50001 DROP TABLE IF EXISTS `050102_view`*/;
/*!50001 DROP VIEW IF EXISTS `050102_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `050102_view` AS select date_format(cast(`getTanggal`(`a`.`kode_pembelian`) as date),'%m %Y') AS `tgl_transaksi`,(to_days(curdate()) - to_days(`getTanggal`(`a`.`kode_pembelian`))) AS `halaman`,period_diff(date_format(now(),'%Y%m'),date_format(`getTanggal`(`a`.`kode_pembelian`),'%Y%m')) AS `bulan`,`b`.`kode_barang` AS `kode_barang`,`b`.`nama_barang` AS `nama_barang`,`c`.`kode_satuan` AS `kode_satuan`,`c`.`nama_satuan` AS `nama_satuan`,`a`.`harga` AS `harga_perolehan`,`a`.`jumlah_pembelian` AS `jumlah_pembelian`,`a`.`jumlah_penjualan` AS `jumlah_penjualan`,`a`.`jumlah_stok` AS `jumlah_persediaan`,if((length(`a`.`keterangan`) > 0),`a`.`keterangan`,_utf8'-') AS `keterangan` from ((`tabel_pembelian` `a` join `ref_barang` `b` on(((`b`.`kode_barang` = `a`.`kode_barang`) and (`a`.`kode_status` = 1)))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) order by `a`.`kode_pembelian`,`b`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `050201_view`
--

/*!50001 DROP TABLE IF EXISTS `050201_view`*/;
/*!50001 DROP VIEW IF EXISTS `050201_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`jerbee_view`@`10.8.0.5` SQL SECURITY DEFINER */
/*!50001 VIEW `050201_view` AS select cast(`getTanggal`(`c`.`kode_do`) as date) AS `tgl_transaksi`,(to_days(curdate()) - to_days(`getTanggal`(`a`.`kode_do`))) AS `hari`,period_diff(date_format(now(),'%Y%m'),date_format(`getTanggal`(`a`.`kode_do`),'%Y%m')) AS `bulan`,`b`.`kode_pelanggan` AS `kode_vendor`,`b`.`nama` AS `nama_pelanggan`,`d`.`kode_barang` AS `kode_barang`,`d`.`nama_barang` AS `nama_barang`,`e`.`nama_satuan` AS `nama_satuan`,`c`.`harga_penjualan` AS `harga_penjualan`,`c`.`jumlah_penjualan` AS `jumlah_penjualan`,`c`.`keterangan` AS `keterangan` from ((((`tabel_pengiriman` `a` join `tabel_pelanggan` `b` on((`b`.`kode_pelanggan` = `a`.`kode_pelanggan`))) join `tabel_penjualan` `c` on((`c`.`kode_do` = `a`.`kode_do`))) join `ref_barang` `d` on((`d`.`kode_barang` = `c`.`kode_barang`))) join `ref_satuan` `e` on((`e`.`kode_satuan` = `c`.`kode_satuan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cek_persediaan`
--

/*!50001 DROP TABLE IF EXISTS `cek_persediaan`*/;
/*!50001 DROP VIEW IF EXISTS `cek_persediaan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cek_persediaan` AS select concat(repeat(_utf8'0',(6 - length(`b`.`kode_barang`))),`b`.`kode_barang`) AS `Kode`,`b`.`nama_barang` AS `Nama Barang`,format(sum(`a`.`jumlah_stok`),0) AS `Posisi Stok`,`c`.`nama_satuan` AS `Satuan` from ((`tabel_pembelian` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) group by `a`.`kode_barang`,`a`.`kode_satuan` order by `b`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_barang`
--

/*!50001 DROP TABLE IF EXISTS `form_barang`*/;
/*!50001 DROP VIEW IF EXISTS `form_barang`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_barang` AS select `a`.`nama_barang` AS `nama_barang`,`a`.`jenis_barang` AS `jenis_barang` from `ref_barang` `a` order by `a`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_harga`
--

/*!50001 DROP TABLE IF EXISTS `form_harga`*/;
/*!50001 DROP VIEW IF EXISTS `form_harga`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_harga` AS select `a`.`nama_barang` AS `nama_barang`,`b`.`nama_satuan` AS `nama_satuan`,`c`.`harga_dasar` AS `harga_jual` from ((`ref_harga` `c` join `ref_barang` `a` on((`a`.`kode_barang` = `c`.`kode_barang`))) join `ref_satuan` `b` on((`b`.`kode_satuan` = `c`.`kode_satuan`))) order by `a`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_opname`
--

/*!50001 DROP TABLE IF EXISTS `form_opname`*/;
/*!50001 DROP VIEW IF EXISTS `form_opname`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_opname` AS select `frm_opname`.`kode_status` AS `kode_status` from `frm_opname` where (cast(`frm_opname`.`opname_tgl` as date) = curdate()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_pelanggan`
--

/*!50001 DROP TABLE IF EXISTS `form_pelanggan`*/;
/*!50001 DROP VIEW IF EXISTS `form_pelanggan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_pelanggan` AS select `a`.`contact_person` AS `contact_person`,`a`.`nama` AS `nama`,`a`.`alamat` AS `alamat`,`a`.`kota` AS `kota`,`a`.`email` AS `email`,`a`.`phone_number` AS `phone_number`,`a`.`kode_pos` AS `kode_pos` from `tabel_pelanggan` `a` order by `a`.`nama` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_satuan`
--

/*!50001 DROP TABLE IF EXISTS `form_satuan`*/;
/*!50001 DROP VIEW IF EXISTS `form_satuan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_satuan` AS select `a`.`nama_satuan` AS `nama_satuan`,`a`.`jumlah_satuan` AS `jumlah_satuan` from `ref_satuan` `a` order by `a`.`nama_satuan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `form_vendor`
--

/*!50001 DROP TABLE IF EXISTS `form_vendor`*/;
/*!50001 DROP VIEW IF EXISTS `form_vendor`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `form_vendor` AS select `a`.`contact_person` AS `contact_person`,`a`.`nama` AS `nama`,`a`.`alamat` AS `alamat`,`a`.`kota` AS `kota`,`a`.`email` AS `email`,`a`.`phone_number` AS `phone_number`,`a`.`fax_number` AS `fax_number`,`a`.`kode_pos` AS `kode_pos` from `tabel_vendor` `a` order by `a`.`nama` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `kode_biaya`
--

/*!50001 DROP TABLE IF EXISTS `kode_biaya`*/;
/*!50001 DROP VIEW IF EXISTS `kode_biaya`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `kode_biaya` AS select `a`.`kode_biaya` AS `Kode`,`a`.`keterangan_biaya` AS `Keterangan` from `ref_biaya` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `kode_satuan`
--

/*!50001 DROP TABLE IF EXISTS `kode_satuan`*/;
/*!50001 DROP VIEW IF EXISTS `kode_satuan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `kode_satuan` AS select `a`.`nama_satuan` AS `Nama`,`a`.`jumlah_satuan` AS `Jumlah Item` from `ref_satuan` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_biaya`
--

/*!50001 DROP TABLE IF EXISTS `laporan_biaya`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_biaya`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_biaya` AS select month(`getTanggal`(`a`.`kode_transaksi`)) AS `Bulan`,year(`getTanggal`(`a`.`kode_transaksi`)) AS `Tahun`,`b`.`keterangan_biaya` AS `Jenis Biaya`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 1),`a`.`jumlah_biaya`,0)) AS `01`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 2),`a`.`jumlah_biaya`,0)) AS `02`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 3),`a`.`jumlah_biaya`,0)) AS `03`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 4),`a`.`jumlah_biaya`,0)) AS `04`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 5),`a`.`jumlah_biaya`,0)) AS `05`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 6),`a`.`jumlah_biaya`,0)) AS `06`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 7),`a`.`jumlah_biaya`,0)) AS `07`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 8),`a`.`jumlah_biaya`,0)) AS `08`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 9),`a`.`jumlah_biaya`,0)) AS `09`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 10),`a`.`jumlah_biaya`,0)) AS `10`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 11),`a`.`jumlah_biaya`,0)) AS `11`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 12),`a`.`jumlah_biaya`,0)) AS `12`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 13),`a`.`jumlah_biaya`,0)) AS `13`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 14),`a`.`jumlah_biaya`,0)) AS `14`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 15),`a`.`jumlah_biaya`,0)) AS `15`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 16),`a`.`jumlah_biaya`,0)) AS `16`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 17),`a`.`jumlah_biaya`,0)) AS `17`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 18),`a`.`jumlah_biaya`,0)) AS `18`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 19),`a`.`jumlah_biaya`,0)) AS `19`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 20),`a`.`jumlah_biaya`,0)) AS `20`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 21),`a`.`jumlah_biaya`,0)) AS `21`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 22),`a`.`jumlah_biaya`,0)) AS `22`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 23),`a`.`jumlah_biaya`,0)) AS `23`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 24),`a`.`jumlah_biaya`,0)) AS `24`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 25),`a`.`jumlah_biaya`,0)) AS `25`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 26),`a`.`jumlah_biaya`,0)) AS `26`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 27),`a`.`jumlah_biaya`,0)) AS `27`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 28),`a`.`jumlah_biaya`,0)) AS `28`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 29),`a`.`jumlah_biaya`,0)) AS `29`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 30),`a`.`jumlah_biaya`,0)) AS `30`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_transaksi`)) = 31),`a`.`jumlah_biaya`,0)) AS `31` from (`tabel_biaya` `a` join `ref_biaya` `b` on((`b`.`kode_biaya` = `a`.`kode_biaya`))) where (`a`.`kode_status` = 1) group by date_format(`getTanggal`(`a`.`kode_transaksi`),_utf8'%Y%m'),`a`.`kode_biaya` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_laba`
--

/*!50001 DROP TABLE IF EXISTS `laporan_laba`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_laba`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_laba` AS select month(`getTanggal`(`a`.`kode_penjualan`)) AS `Bulan`,year(`getTanggal`(`a`.`kode_penjualan`)) AS `Tahun`,`b`.`nama_barang` AS `Jenis Barang`,`c`.`nama_satuan` AS `Satuan`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 1),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `01`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 2),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `02`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 3),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `03`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 4),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `04`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 5),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `05`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 6),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `06`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 7),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `07`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 8),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `08`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 9),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `09`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 10),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `10`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 11),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `11`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 12),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `12`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 13),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `13`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 14),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `14`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 15),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `15`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 16),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `16`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 17),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `17`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 18),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `18`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 19),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `19`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 20),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `20`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 21),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `21`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 22),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `22`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 23),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `23`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 24),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `24`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 25),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `25`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 26),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `26`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 27),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `27`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 28),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `28`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 29),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `29`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 30),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `30`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 31),(`d`.`jumlah_penjualan` * (`a`.`harga_penjualan` - `e`.`harga`)),0)) AS `31` from ((((`tabel_penjualan` `a` join `tabel_transaksi` `d` on((`d`.`kode_penjualan` = `a`.`kode_penjualan`))) join `tabel_pembelian` `e` on((`e`.`kode_pembelian` = `d`.`kode_pembelian`))) join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) where (`a`.`kode_status` = 1) group by date_format(`getTanggal`(`a`.`kode_penjualan`),_utf8'%Y%m'),`a`.`kode_barang`,`a`.`kode_satuan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_opname`
--

/*!50001 DROP TABLE IF EXISTS `laporan_opname`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_opname`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_opname` AS select `b`.`nama_barang` AS `Nama Barang`,format(`a`.`jumlah_stok`,0) AS `Persediaan`,`c`.`nama_satuan` AS `Satuan` from ((`tabel_pembelian` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) order by `b`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_pembelian_barang`
--

/*!50001 DROP TABLE IF EXISTS `laporan_pembelian_barang`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_pembelian_barang`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_pembelian_barang` AS select month(`getTanggal`(`a`.`kode_pembelian`)) AS `Bulan`,year(`getTanggal`(`a`.`kode_pembelian`)) AS `Tahun`,`b`.`nama_barang` AS `Jenis Barang`,`c`.`nama_satuan` AS `Satuan`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 1),`a`.`jumlah_penjualan`,0)) AS `01`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 2),`a`.`jumlah_penjualan`,0)) AS `02`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 3),`a`.`jumlah_penjualan`,0)) AS `03`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 4),`a`.`jumlah_penjualan`,0)) AS `04`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 5),`a`.`jumlah_penjualan`,0)) AS `05`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 6),`a`.`jumlah_penjualan`,0)) AS `06`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 7),`a`.`jumlah_penjualan`,0)) AS `07`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 8),`a`.`jumlah_penjualan`,0)) AS `08`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 9),`a`.`jumlah_penjualan`,0)) AS `09`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 10),`a`.`jumlah_penjualan`,0)) AS `10`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 11),`a`.`jumlah_penjualan`,0)) AS `11`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 12),`a`.`jumlah_penjualan`,0)) AS `12`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 13),`a`.`jumlah_penjualan`,0)) AS `13`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 14),`a`.`jumlah_penjualan`,0)) AS `14`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 15),`a`.`jumlah_penjualan`,0)) AS `15`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 16),`a`.`jumlah_penjualan`,0)) AS `16`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 17),`a`.`jumlah_penjualan`,0)) AS `17`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 18),`a`.`jumlah_penjualan`,0)) AS `18`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 19),`a`.`jumlah_penjualan`,0)) AS `19`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 20),`a`.`jumlah_penjualan`,0)) AS `20`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 21),`a`.`jumlah_penjualan`,0)) AS `21`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 22),`a`.`jumlah_penjualan`,0)) AS `22`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 23),`a`.`jumlah_penjualan`,0)) AS `23`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 24),`a`.`jumlah_penjualan`,0)) AS `24`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 25),`a`.`jumlah_penjualan`,0)) AS `25`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 26),`a`.`jumlah_penjualan`,0)) AS `26`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 27),`a`.`jumlah_penjualan`,0)) AS `27`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 28),`a`.`jumlah_penjualan`,0)) AS `28`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 29),`a`.`jumlah_penjualan`,0)) AS `29`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 30),`a`.`jumlah_penjualan`,0)) AS `30`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_pembelian`)) = 31),`a`.`jumlah_penjualan`,0)) AS `31` from ((`tabel_pembelian` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) where (`a`.`kode_status` = 1) group by date_format(`getTanggal`(`a`.`kode_pembelian`),_utf8'%Y%m'),`a`.`kode_barang`,`a`.`kode_satuan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_penjualan_barang`
--

/*!50001 DROP TABLE IF EXISTS `laporan_penjualan_barang`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_penjualan_barang`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_penjualan_barang` AS select month(`getTanggal`(`a`.`kode_penjualan`)) AS `Bulan`,year(`getTanggal`(`a`.`kode_penjualan`)) AS `Tahun`,`b`.`nama_barang` AS `Jenis Barang`,`c`.`nama_satuan` AS `Satuan`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 1),`a`.`jumlah_penjualan`,0)) AS `01`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 2),`a`.`jumlah_penjualan`,0)) AS `02`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 3),`a`.`jumlah_penjualan`,0)) AS `03`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 4),`a`.`jumlah_penjualan`,0)) AS `04`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 5),`a`.`jumlah_penjualan`,0)) AS `05`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 6),`a`.`jumlah_penjualan`,0)) AS `06`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 7),`a`.`jumlah_penjualan`,0)) AS `07`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 8),`a`.`jumlah_penjualan`,0)) AS `08`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 9),`a`.`jumlah_penjualan`,0)) AS `09`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 10),`a`.`jumlah_penjualan`,0)) AS `10`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 11),`a`.`jumlah_penjualan`,0)) AS `11`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 12),`a`.`jumlah_penjualan`,0)) AS `12`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 13),`a`.`jumlah_penjualan`,0)) AS `13`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 14),`a`.`jumlah_penjualan`,0)) AS `14`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 15),`a`.`jumlah_penjualan`,0)) AS `15`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 16),`a`.`jumlah_penjualan`,0)) AS `16`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 17),`a`.`jumlah_penjualan`,0)) AS `17`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 18),`a`.`jumlah_penjualan`,0)) AS `18`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 19),`a`.`jumlah_penjualan`,0)) AS `19`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 20),`a`.`jumlah_penjualan`,0)) AS `20`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 21),`a`.`jumlah_penjualan`,0)) AS `21`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 22),`a`.`jumlah_penjualan`,0)) AS `22`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 23),`a`.`jumlah_penjualan`,0)) AS `23`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 24),`a`.`jumlah_penjualan`,0)) AS `24`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 25),`a`.`jumlah_penjualan`,0)) AS `25`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 26),`a`.`jumlah_penjualan`,0)) AS `26`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 27),`a`.`jumlah_penjualan`,0)) AS `27`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 28),`a`.`jumlah_penjualan`,0)) AS `28`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 29),`a`.`jumlah_penjualan`,0)) AS `29`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 30),`a`.`jumlah_penjualan`,0)) AS `30`,sum(if((dayofmonth(`getTanggal`(`a`.`kode_penjualan`)) = 31),`a`.`jumlah_penjualan`,0)) AS `31` from ((`tabel_penjualan` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) where (`a`.`kode_status` = 1) group by date_format(`getTanggal`(`a`.`kode_penjualan`),_utf8'%Y%m'),`a`.`kode_barang`,`a`.`kode_satuan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_rekap_biaya_perjenis`
--

/*!50001 DROP TABLE IF EXISTS `laporan_rekap_biaya_perjenis`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_rekap_biaya_perjenis`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_rekap_biaya_perjenis` AS select month(`getTanggal`(`a`.`kode_transaksi`)) AS `Bulan`,year(`getTanggal`(`a`.`kode_transaksi`)) AS `Tahun`,`b`.`keterangan_biaya` AS `Jenis Biaya`,sum(`a`.`jumlah_biaya`) AS `Jumlah Biaya` from (`tabel_biaya` `a` join `ref_biaya` `b` on((`b`.`kode_biaya` = `a`.`kode_biaya`))) where (`a`.`kode_status` = 1) group by date_format(`getTanggal`(`a`.`kode_transaksi`),_utf8'%Y%m'),`a`.`kode_biaya` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_rinci_biaya`
--

/*!50001 DROP TABLE IF EXISTS `laporan_rinci_biaya`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_rinci_biaya`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_rinci_biaya` AS select `getTanggal`(`a`.`kode_transaksi`) AS `Tanggal`,`b`.`keterangan_biaya` AS `Jenis Biaya`,`a`.`jumlah_biaya` AS `Jumlah Biaya`,`a`.`keterangan` AS `Keterangan`,`a`.`kode_status` AS `Status` from (`tabel_biaya` `a` join `ref_biaya` `b` on((`b`.`kode_biaya` = `a`.`kode_biaya`))) order by `a`.`kode_transaksi`,`a`.`kode_biaya` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_rinci_pembelian`
--

/*!50001 DROP TABLE IF EXISTS `laporan_rinci_pembelian`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_rinci_pembelian`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_rinci_pembelian` AS select cast(`getTanggal`(`a`.`kode_po`) as date) AS `tanggal`,`a`.`nomer_po` AS `nomer_po`,`a`.`nomer_kontrak` AS `nomer_kontrak`,`b`.`nama` AS `vendor`,`d`.`nama_barang` AS `nama_barang`,`e`.`nama_satuan` AS `nama_satuan`,`c`.`harga` AS `harga`,`c`.`jumlah_pembelian` AS `jumlah_pembelian`,`c`.`jumlah_penjualan` AS `jumlah_penjualan`,`c`.`jumlah_stok` AS `jumlah_stok`,`c`.`keterangan` AS `keterangan` from ((((`tabel_po` `a` join `tabel_vendor` `b` on((`b`.`kode_vendor` = `a`.`kode_vendor`))) join `tabel_pembelian` `c` on((`c`.`kode_po` = `a`.`kode_po`))) join `ref_barang` `d` on((`d`.`kode_barang` = `c`.`kode_barang`))) join `ref_satuan` `e` on((`e`.`kode_satuan` = `c`.`kode_satuan`))) order by `a`.`kode_po` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_rinci_penjualan`
--

/*!50001 DROP TABLE IF EXISTS `laporan_rinci_penjualan`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_rinci_penjualan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_rinci_penjualan` AS select cast(`getTanggal`(`a`.`kode_do`) as date) AS `tanggal`,`a`.`nomer_do` AS `nomer_do`,`a`.`nomer_kontrak` AS `nomer_kontrak`,`b`.`nama` AS `nama_pelanggan`,`d`.`nama_barang` AS `nama_barang`,`e`.`nama_satuan` AS `nama_satuan`,`c`.`harga_penjualan` AS `harga_penjualan`,`c`.`jumlah_penjualan` AS `jumlah_penjualan`,`c`.`keterangan` AS `keterangan` from ((((`tabel_pengiriman` `a` join `tabel_pelanggan` `b` on((`b`.`kode_pelanggan` = `a`.`kode_pelanggan`))) join `tabel_penjualan` `c` on((`c`.`kode_do` = `a`.`kode_do`))) join `ref_barang` `d` on((`d`.`kode_barang` = `c`.`kode_barang`))) join `ref_satuan` `e` on((`e`.`kode_satuan` = `c`.`kode_satuan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `laporan_stok`
--

/*!50001 DROP TABLE IF EXISTS `laporan_stok`*/;
/*!50001 DROP VIEW IF EXISTS `laporan_stok`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `laporan_stok` AS select month(`a`.`Tanggal Stok`) AS `Bulan`,year(`a`.`Tanggal Stok`) AS `Tahun`,`a`.`Nama Barang` AS `Nama Barang`,`a`.`Satuan` AS `Satuan`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 1),`a`.`Persediaan`,0)) AS `01`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 2),`a`.`Persediaan`,0)) AS `02`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 3),`a`.`Persediaan`,0)) AS `03`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 4),`a`.`Persediaan`,0)) AS `04`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 5),`a`.`Persediaan`,0)) AS `05`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 6),`a`.`Persediaan`,0)) AS `06`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 7),`a`.`Persediaan`,0)) AS `07`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 8),`a`.`Persediaan`,0)) AS `08`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 9),`a`.`Persediaan`,0)) AS `09`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 10),`a`.`Persediaan`,0)) AS `10`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 11),`a`.`Persediaan`,0)) AS `11`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 12),`a`.`Persediaan`,0)) AS `12`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 13),`a`.`Persediaan`,0)) AS `13`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 14),`a`.`Persediaan`,0)) AS `14`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 15),`a`.`Persediaan`,0)) AS `15`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 16),`a`.`Persediaan`,0)) AS `16`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 17),`a`.`Persediaan`,0)) AS `17`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 18),`a`.`Persediaan`,0)) AS `18`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 19),`a`.`Persediaan`,0)) AS `19`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 20),`a`.`Persediaan`,0)) AS `20`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 21),`a`.`Persediaan`,0)) AS `21`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 22),`a`.`Persediaan`,0)) AS `22`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 23),`a`.`Persediaan`,0)) AS `23`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 24),`a`.`Persediaan`,0)) AS `24`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 25),`a`.`Persediaan`,0)) AS `25`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 26),`a`.`Persediaan`,0)) AS `26`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 27),`a`.`Persediaan`,0)) AS `27`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 28),`a`.`Persediaan`,0)) AS `28`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 29),`a`.`Persediaan`,0)) AS `29`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 30),`a`.`Persediaan`,0)) AS `30`,sum(if((dayofmonth(`a`.`Tanggal Stok`) = 31),`a`.`Persediaan`,0)) AS `31` from `persediaan_harian` `a` group by date_format(`a`.`Tanggal Stok`,_utf8'%Y%m'),`a`.`Kode Barang`,`a`.`Kode Satuan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `nama_barang`
--

/*!50001 DROP TABLE IF EXISTS `nama_barang`*/;
/*!50001 DROP VIEW IF EXISTS `nama_barang`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `nama_barang` AS select `a`.`nama_barang` AS `Nama` from `ref_barang` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `rinci_stok`
--

/*!50001 DROP TABLE IF EXISTS `rinci_stok`*/;
/*!50001 DROP VIEW IF EXISTS `rinci_stok`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `rinci_stok` AS select `a`.`kode_po` AS `Kode PO`,cast(`getTanggal`(`a`.`kode_pembelian`) as date) AS `Perolehan`,cast(`getTanggal`(`e`.`kode_penjualan`) as date) AS `Penjualan`,concat(repeat(_utf8'0',(6 - length(`b`.`kode_barang`))),`b`.`kode_barang`) AS `Kode Barang`,`b`.`nama_barang` AS `Nama Barang`,`b`.`jenis_barang` AS `Jenis`,`c`.`nama_satuan` AS `Satuan`,`a`.`jumlah_pembelian` AS `Jumlah Perolehan`,`d`.`jumlah_penjualan` AS `Jumlah Penjualan`,`a`.`jumlah_stok` AS `Stok` from ((((`tabel_pembelian` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) left join `tabel_transaksi` `d` on((`d`.`kode_pembelian` = `a`.`kode_pembelian`))) left join `tabel_penjualan` `e` on((`e`.`kode_penjualan` = `d`.`kode_penjualan`))) where (`a`.`kode_barang` = 137) order by `b`.`nama_barang`,`a`.`kode_po`,`d`.`kode_penjualan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tabel_pelanggan`
--

/*!50001 DROP TABLE IF EXISTS `tabel_pelanggan`*/;
/*!50001 DROP VIEW IF EXISTS `tabel_pelanggan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tabel_pelanggan` AS select `a`.`kode_vendor` AS `kode_pelanggan`,`a`.`contact_person` AS `contact_person`,`a`.`nama` AS `nama`,`a`.`alamat` AS `alamat`,`a`.`kota` AS `kota`,`a`.`email` AS `email`,`a`.`phone_number` AS `phone_number`,`a`.`fax_number` AS `fax_number`,`a`.`kode_pos` AS `kode_pos`,`a`.`kode_status` AS `kode_status` from `tabel_vendor` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_daftar_harga`
--

/*!50001 DROP TABLE IF EXISTS `v_daftar_harga`*/;
/*!50001 DROP VIEW IF EXISTS `v_daftar_harga`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_daftar_harga` AS select `e`.`tanggal_po` AS `tanggal_perolehan`,`b`.`kode_barang` AS `kode_barang`,`c`.`kode_satuan` AS `kode_satuan`,`b`.`nama_barang` AS `nama_barang`,`c`.`nama_satuan` AS `nama_satuan`,`d`.`jumlah_stok` AS `jumlah_stok`,`d`.`harga` AS `harga_pokok`,`a`.`harga_dasar` AS `harga_dasar` from ((((`tabel_pembelian` `d` join `ref_harga` `a` on(((`a`.`kode_barang` = `d`.`kode_barang`) and (`a`.`kode_satuan` = `d`.`kode_satuan`)))) join `tabel_po` `e` on((`e`.`kode_po` = `d`.`kode_po`))) join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) order by `b`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_menu_item`
--

/*!50001 DROP TABLE IF EXISTS `v_menu_item`*/;
/*!50001 DROP VIEW IF EXISTS `v_menu_item`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_menu_item` AS select `a`.`appl_kode` AS `appl_kode`,`a`.`appl_seq` AS `appl_seq`,substr(`a`.`appl_kode`,1,2) AS `l1`,substr(`a`.`appl_kode`,3,2) AS `l2`,substr(`a`.`appl_kode`,5,2) AS `l3`,`a`.`ga_kode` AS `parent_id`,`a`.`ga_kode` AS `ga_kode`,ifnull(`b`.`appl_nama`,_utf8'-') AS `ga_nama`,`a`.`appl_file` AS `appl_file`,`a`.`appl_proc` AS `appl_proc`,`a`.`appl_nama` AS `appl_name`,`a`.`appl_nama` AS `appl_nama`,`a`.`appl_deskripsi` AS `appl_desc`,`a`.`appl_sts` AS `appl_sts`,if((`a`.`appl_sts` = 1),_utf8'Disable',if((substr(`a`.`appl_kode`,5,2) = _utf8'00'),_utf8'Submenu',_utf8'Enable')) AS `status` from (`tabel_aplikasi` `a` left join `tabel_aplikasi` `b` on((`b`.`appl_kode` = `a`.`ga_kode`))) order by `a`.`ga_kode`,`a`.`appl_seq` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pembayaran`
--

/*!50001 DROP TABLE IF EXISTS `v_pembayaran`*/;
/*!50001 DROP VIEW IF EXISTS `v_pembayaran`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pembayaran` AS select `tabel_pembayaran`.`kode_do` AS `kode_do`,cast(max(`tabel_pembayaran`.`byr_tgl`) as date) AS `byr_tgl`,sum(`tabel_pembayaran`.`byr_total`) AS `byr_total`,sum(`tabel_pembayaran`.`byr_sts`) AS `byr_sts` from `tabel_pembayaran` group by `tabel_pembayaran`.`kode_do` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pembelian`
--

/*!50001 DROP TABLE IF EXISTS `v_pembelian`*/;
/*!50001 DROP VIEW IF EXISTS `v_pembelian`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pembelian` AS select cast(`getTanggal`(`a`.`kode_pembelian`) as date) AS `Tanggal`,`b`.`nama_barang` AS `Nama Barang`,`c`.`nama_satuan` AS `Satuan`,format(`a`.`harga`,0) AS `Harga Satuan`,format(`a`.`jumlah_pembelian`,0) AS `Total Item`,format(`a`.`jumlah_stok`,0) AS `Sisa Stok`,if((length(`a`.`keterangan`) > 0),`a`.`keterangan`,_utf8'-') AS `Keterangan` from ((`tabel_pembelian` `a` join `ref_barang` `b` on(((`b`.`kode_barang` = `a`.`kode_barang`) and (`a`.`kode_status` = 1)))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) order by `a`.`kode_pembelian` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pengguna`
--

/*!50001 DROP TABLE IF EXISTS `v_pengguna`*/;
/*!50001 DROP VIEW IF EXISTS `v_pengguna`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pengguna` AS select `a`.`kar_id` AS `usr_id`,`a`.`kar_nama` AS `usr_nama`,ifnull(`b`.`grup_id`,_utf8'-') AS `grup_id`,ifnull(`b`.`grup_nama`,_utf8'-') AS `grup_nama`,ifnull(`c`.`kp_kode`,`a`.`kp_kode`) AS `pdam_kode`,ifnull(`c`.`kp_ket`,ifnull(`d`.`cab_ket`,_utf8'Kantor Pusat')) AS `pdam_nama` from (((`tabel_user` `a` left join `tabel_grup` `b` on((`a`.`grup_id` = `b`.`grup_id`))) left join `tabel_wilayah` `c` on((`a`.`kp_kode` = `c`.`kp_kode`))) left join `tabel_cabang` `d` on((`c`.`cab_kode` = `d`.`cab_kode`))) order by `a`.`kar_nama` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_stok`
--

/*!50001 DROP TABLE IF EXISTS `v_stok`*/;
/*!50001 DROP VIEW IF EXISTS `v_stok`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_stok` AS select sum(`a`.`jumlah_pembelian`) AS `Jumlah`,concat(`b`.`nama_barang`,_utf8' ',`c`.`nama_satuan`) AS `Barang`,_utf8'Beli' AS `Transaksi` from ((`tabel_pembelian` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) group by `a`.`kode_barang`,`a`.`kode_satuan` union select sum(`a`.`jumlah_penjualan`) AS `sum( a.jumlah_penjualan )`,concat(`b`.`nama_barang`,_utf8' ',`c`.`nama_satuan`) AS `concat( b.nama_barang,' ', c.nama_satuan )`,_utf8'Jual' AS `Jual` from ((`tabel_penjualan` `a` join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) group by `a`.`kode_barang`,`a`.`kode_satuan` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_penjualan`
--

/*!50001 DROP TABLE IF EXISTS `view_penjualan`*/;
/*!50001 DROP VIEW IF EXISTS `view_penjualan`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_penjualan` AS select `encodeResi`(`a`.`kode_do`) AS `kode_resi`,`a`.`tanggal_drop` AS `tanggal_drop`,ifnull(`c`.`byr_sts`,0) AS `byr_cetak`,sum((`b`.`harga_diskon` * `b`.`jumlah_penjualan`)) AS `total_transaksi`,ifnull(`c`.`byr_total`,0) AS `total_dibayar`,`a`.`kode_bayar` AS `kode_bayar`,ifnull(`c`.`byr_tgl`,_utf8'-') AS `remark_tgl` from ((`tabel_pengiriman` `a` join `tabel_penjualan` `b` on(((`b`.`kode_do` = `a`.`kode_do`) and (`a`.`kode_status` < 3)))) left join `v_pembayaran` `c` on((`c`.`kode_do` = `a`.`kode_do`))) group by `a`.`kode_do` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_stok`
--

/*!50001 DROP TABLE IF EXISTS `view_stok`*/;
/*!50001 DROP VIEW IF EXISTS `view_stok`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_stok` AS select concat(repeat(_utf8'0',(6 - length(`b`.`kode_barang`))),`b`.`kode_barang`) AS `Kode`,`b`.`nama_barang` AS `Nama Barang`,`b`.`jenis_barang` AS `Jenis`,`c`.`nama_satuan` AS `Satuan`,max(`a`.`harga`) AS `Pokok`,`d`.`harga_dasar` AS `Harga Jual`,sum(`a`.`jumlah_stok`) AS `Stok`,(`a`.`harga` * `a`.`jumlah_stok`) AS `Total Pokok`,(`d`.`harga_dasar` * `a`.`jumlah_stok`) AS `Total Jual` from (((`tabel_pembelian` `a` join `ref_harga` `d` on(((`d`.`kode_barang` = `a`.`kode_barang`) and (`d`.`kode_satuan` = `a`.`kode_satuan`)))) join `ref_barang` `b` on((`b`.`kode_barang` = `a`.`kode_barang`))) join `ref_satuan` `c` on((`c`.`kode_satuan` = `a`.`kode_satuan`))) group by `a`.`kode_barang`,`a`.`kode_satuan` order by `b`.`nama_barang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-16 22:26:55
