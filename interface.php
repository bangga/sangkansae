<?php
	require "model/setDB.php";
	require "_data/logging.php";
	require "fungsi.php";
	require "lib.php";
	$log 	= new errorLog();
	
	/** getParam 
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST;
	$konci	= array_keys($nilai);
	for($i=0;$i<count($konci);$i++){
		$$konci[$i]	= $nilai[$konci[$i]];
	}
	/* getParam **/

	if(isset($login)){
		session_start();
		$randId 	= mt_rand(1,9999);
		$appl_tokn	= getToken(str_repeat('0',4-strlen($randId)).$randId);
		define("_USER",'000000');
		define("_KODE",'000000');
		define("_TOKN",$appl_tokn);
	}
	else{
		if(!cek_pass()) die();
	}
	
	/** define parameter */
	if($ipClient == '::1'){
		$ipClient = '127.0.0.1';
	}
	
	if(isset($appl_kode)){
		$_SESSION['appl_kode'] = $appl_kode;
	}
	
	if(isset($appl_name)){
		$_SESSION['appl_name'] = $appl_name;
	}
	if(isset($appl_file)){
		$_SESSION['appl_file'] = $appl_file;
	}
	if(isset($appl_proc)){
		$_SESSION['appl_proc'] = $appl_proc;
	}
	if(isset($appl_desc)){
		$_SESSION['appl_desc'] = $appl_desc;
	}

	define("_KODE",$_SESSION['appl_kode']);
	define("_NAME",$_SESSION['appl_name']);
	define("_FILE",$_SESSION['appl_file']);
	define("_PROC",$_SESSION['appl_proc']);
	define("_DESC",$_SESSION['appl_desc']);
	define("_HOST",$ipClient);
	
	// inisiasi kode_do untuk proses transaksi penjualan
	if(isset($from_menu) and substr(_KODE,0,4)=='0302'){
		$kode_do				= getToken('2000');
		$nomer_do				= '000000';
		$_SESSION['appl_tokn']	= $kode_do;
		$_SESSION['kode_do']	= $kode_do;
		$_SESSION['nomer_do']	= $nomer_do;
		define("_TOKN",$kode_do);
	}
	// inisiasi kode_po untuk proses transaksi pembelian
	else if(isset($from_menu) and substr(_KODE,0,4)=='0301'){
		$kode_po				= getToken('1000');
		$nomer_po				= '000000';
		$_SESSION['appl_tokn']	= $kode_po;
		$_SESSION['kode_po']	= $kode_po;
		$_SESSION['nomer_po']	= $nomer_po;
		define("_TOKN",$kode_po);
	}
	else if(isset($from_menu) and substr(_KODE,0,4)!='0302'){
		$randId 	= mt_rand(1,9999);
		$appl_tokn	= getToken(str_repeat('0',4-strlen($randId)).$randId);
		define("_TOKN",$appl_tokn);
		$_SESSION['appl_tokn'] = $appl_tokn;
		if(isset($_SESSION['kode_do'])){
			unset($_SESSION['kode_do']);
		}
		if(isset($_SESSION['nomer_do'])){
			unset($_SESSION['nomer_do']);
		}
	}
	else{
		define("_TOKN",$_SESSION['appl_tokn']);
	}
		
	/** database */
	try{
		$link 	= new PDO("mysql:host=".$DHOST.";dbname=".$DNAME, $DUSER, $DPASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		$erno	= true;
	}
	catch(Exception $e){
		$log->errorDB($e->getMessage());
		$log->logMess('Gagal terhubung ke database server');
		$erno 	= false;
	}
	/* database **/
	/* define parameter **/
	
	/* Pagination */
	if(isset($pg) and $pg>1){
		$next_page 	= $pg + 1;
		$pref_page 	= $pg - 1;
		$pref_mess	= "<button class=\"btn\" onClick=\"buka('pref_page')\">Pref</button>";
		$frst_mess	= "<button class=\"btn\" onClick=\"buka('frst_page')\">First</button>";
	}
	else{
		$pg 		= 1;
		$next_page 	= 2;
		$pref_page 	= 1;
	}
	if(!isset($jml_perpage)){
		$jml_perpage 	= 7;
		if(isset($proses)){
			$jml_perpage 	= 7;
		}
	}
	$limit_awal	= ($pg - 1) * $jml_perpage;
	
	$tambah		= "<button class=\"btn\" onClick=\"nonghol('tambah')\">Tambah</button>";
	/* pagination **/
	
	if(is_readable($targetUrl)){
		$erno	= true;
		require $targetUrl;
	}
	else{
		$pesan 	= "File : $targetUrl tidak ditemukan";
		$erno	= false;
		$log->logMess($pesan);
	}
?>