<?php
	if(!$erno) die();
	switch($proses){
		case "editGrup":
			$saveID = getToken(0);
			$procID = getToken(0);
?>
<input type="hidden" class="<?php echo $saveID; ?>" name="targetUrl" 	value="<?php echo _PROC; 		?>" />
<input type="hidden" class="<?php echo $saveID; ?>" name="targetId" 	value="<?php echo $procID; 		?>" />
<input type="hidden" class="<?php echo $saveID; ?>" name="tutupId" 		value="<?php echo $targetId;	?>" />
<input type="hidden" class="<?php echo $saveID; ?>" name="proses" 		value="<?php echo $proses; 		?>" />
<input type="hidden" class="<?php echo $saveID; ?>" name="old_nama"		value="<?php echo $grup_nama; 	?>" />
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" aria-hidden="true" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4>Form edit akses grup <?php echo $grup_nama; ?></h4>
	</div>
	<span id="<?php echo $procID; ?>">
		<div class="modal-body">
			<div class="form-horizontal">
				<div class="control-group">
					<label class="control-label">Kode Grup</label>
					<div class="controls">
						<input type="text" class="span2" placeholder="<?php echo $grup_id; ?>" disabled />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Nama Grup</label>
					<div class="controls">
						<input type="text" class="span2 <?php echo $saveID; ?>" name="grup_nama" placeholder="<?php echo $grup_nama; ?>" onmouseover="$(this).select()" />
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<div class="btn-group">
				<button class="btn" onclick="tutup('<?php echo $targetId; ?>')">Kembali</button>
				<button class="btn" onclick="buka('<?php echo $saveID; ?>')">Simpan</button>
			</div>
		</div>
	</span>
</div>
<?php
			break;
		case "formMenu":
			try{
				$que 	= "SELECT a.*,IFNULL(b.sts,0) AS status FROM v_menu_item a LEFT JOIN tabel_grup_appl b ON(b.appl_kode=a.appl_kode AND b.grup_id='$grup_id') ORDER BY appl_kode";
				$data	= $link->query($que)->fetchAll();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data tagihan");
				$log->logDB($que);
			}
?>
<div class="modal">
	<div class="modal-header">
		<button type="button" class="close" aria-hidden="true" onclick="tutup('<?php echo $targetId; ?>')">&times;</button>
		<h4>Form edit akses grup <?php echo $grup_nama; ?></h4>
	</div>
	<div class="modal-body">
		<ul class="nav nav-list">
<?php
			for($j=0;$j<count($data);$j++){
				/** getParam 
					memindahkan semua nilai dalam array POST ke dalam
					variabel yang bersesuaian dengan masih kunci array
				*/
				$konci	= array_keys($data[$j]);
				for($i=0;$i<count($konci);$i++){
					$$konci[$i]	= $data[$j][$konci[$i]];
				}
				/* getParam **/
				$badge = "";
				if($status==1){
					$badge = "badge-success";
				}
				$setID = getToken(0);
				
				if($l3=='00' and $parent_id!='000000'){
					?><li class="nav-header"><?php echo $ga_nama." - ".$appl_nama; ?></li><?php
				}
				else if($parent_id!='000000'){
?>
				<input type="hidden" class="<?php echo $setID; ?>" name="targetId" 	value="<?php echo $setID; 		?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="targetUrl" value="<?php echo _PROC; 		?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="grup_id"	value="<?php echo $grup_id; 	?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="grup_nama" value="<?php echo $grup_nama; 	?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="appl_kode"	value="<?php echo $appl_kode; 	?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="appl_nama"	value="<?php echo $appl_nama; 	?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="l1"		value="<?php echo $l1; 			?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="l2"		value="<?php echo $l2; 			?>" />
				<input type="hidden" class="<?php echo $setID; ?>" name="proses" 	value="setMenu" 					/>
				<li id="<?php echo $setID; ?>">
					<input type="hidden" class="<?php echo $setID; ?>" name="status" value="<?php echo $status; 	?>" />
					<a><?php echo $appl_nama; ?><span class="badge <?php echo $badge; ?> pull-right" onclick="buka('<?php echo $setID; ?>')" style="cursor:pointer">Pilih</span></a>
				</li>
<?php
				}
			}
?>
		</ul>
	</div>
	<div class="modal-footer"></div>
</div>
<?php
			break;
		default:
			try{
				$que 	= "SELECT grup_id,grup_nama FROM tabel_grup WHERE grup_id!='000' ORDER BY grup_nama LIMIT $limit_awal,$jml_perpage";
				$data	= $link->query($que)->fetchAll();
			}
			catch(Exception $e){
				$log->errorDB($e->getMessage());
				$log->logMess("Gagal melakukan inquiri data tagihan");
				$log->logDB($que);
			}
?>
<h4 class="muted"><?php echo _NAME; ?></h4>
<table class="table table-striped">
	<tr>
		<th>No.</th>
		<th>Grup ID</th>
		<th>Grup Nama</th>
		<th>Pengaturan</th>
	</tr>
<?php
	for($j=0;$j<count($data);$j++){
		/** getParam 
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		$konci	= array_keys($data[$j]);
		for($i=0;$i<count($konci);$i++){
			$$konci[$i]	= $data[$j][$konci[$i]];
		}
		/* getParam **/
		
		$nomer = $j + 1;
		if($pg > 1){
			$nomer = $nomer + ($pg - 1) * $jml_perpage;
		}
		
		$menuID = getToken(0);
		$editID = getToken(0);
?>
	<tr>
		<td><?php echo $nomer; 		?></td>
		<td><?php echo $grup_id; 	?></td>
		<td><?php echo $grup_nama; 	?></td>
		<td>
			<div class="btn-group">
				<input type="hidden" class="<?php echo $menuID; ?>" name="targetUrl" 	value="<?php echo _FILE; 		?>" />
				<input type="hidden" class="<?php echo $menuID; ?>" name="grup_id" 		value="<?php echo $grup_id; 	?>" />
				<input type="hidden" class="<?php echo $menuID; ?>" name="grup_nama"	value="<?php echo $grup_nama; 	?>" />
				<input type="hidden" class="<?php echo $menuID; ?>" name="proses" 		value="formMenu"					/>
				<button class="btn" onclick="nonghol('<?php echo $menuID; ?>')">Edit Akses</button>
				<input type="hidden" class="<?php echo $editID; ?>" name="targetUrl" 	value="<?php echo _FILE; 		?>" />
				<input type="hidden" class="<?php echo $editID; ?>" name="grup_id" 		value="<?php echo $grup_id; 	?>" />
				<input type="hidden" class="<?php echo $editID; ?>" name="grup_nama"	value="<?php echo $grup_nama; 	?>" />
				<input type="hidden" class="<?php echo $editID; ?>" name="proses" 		value="editGrup"					/>
				<button class="btn" onclick="nonghol('<?php echo $editID; ?>')">Edit Grup</button>
				<button class="btn">Hapus Grup</button>
			</div>
		</td>
	</tr>
<?php
	}
?>
	<tr>
		<td colspan="4">
			<div class="btn-group">
				<?php echo $pref_mess.$next_mess; ?>
			</div>
		</td>
	</tr>
</table>
<?php
	}
?>