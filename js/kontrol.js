function getToken(){
	var randId = new Date();
	return randId.getTime();
}

var targetId = getToken();
jQuery('body').prepend('<div id=\"'+targetId+'\" class=\"hide\"></div>');
jQuery(document).ajaxStart(function() {
	$('#'+targetId).modal('show');
});
jQuery(document).ajaxComplete(function() {
	$('#'+targetId).modal('hide');
});

function getParams(opt){
	var stringData	= '{';
	var	kunci		= '';
	var nilai		= '';
	var targetId	= false;
	var errorId		= '';
	var	params		= document.getElementsByClassName(opt);
	for(i=0;i<params.length;i++){
		kunci = params[i].name;
		nilai = params[i].value;
		if(kunci=='targetId'){
			targetId = nilai;
		}
		if(kunci=='errorId'){
			errorId = nilai;
		}
		stringData = stringData+'\"'+kunci+'\"'+':\"'+nilai+'\"';
		if((i+1)<params.length){
			stringData = stringData+',';
		}
	}
	if(i>0 && targetId==false){
		targetId 	= getToken();
		stringData 	= stringData+',"targetId\":"'+targetId+'\"}';
	}
	else{
		stringData 	= stringData+'}';
	}
	return new Array(targetId,stringData,errorId);
}

function buka(opt){
	var targetUrl	= 'interface.php';
	var param		= getParams(opt);
	var targetId	= param[0];
	var errorId		= false;
	var errMess		= false;
	var data 		= jQuery.parseJSON(param[1]);
	jQuery.post(targetUrl,data, function(data) {
		jQuery('#'+targetId).html(data);
	});
	console.log(param[1]);
}

function nonghol(opt){
	var targetUrl	= 'interface.php';
	var param		= getParams(opt);
	var targetId	= param[0];
	var errorId		= false;
	var errMess		= false;
	var data 		= jQuery.parseJSON(param[1]);
	jQuery('body').prepend('<div id=\"'+targetId+'\" class=\"hide\"></div>');
	jQuery('#'+targetId).modal('show');
	jQuery.post(targetUrl,data, function(data) {
		jQuery('#'+targetId).html(data);
	});
	console.log(param[1]);
}

function tutup(opt){
	jQuery('#'+opt).modal('hide');
	jQuery('#'+opt).remove();
}